---
title: "Decisions are progress. Ask \"What's the next decision?\""
date: 2021-10-12T22:04:21-04:00
author: "Aaron Bell"
draft: false
---

Decisions cause actions which cause results. Decisions are progress. Every moment I'm not deciding is less progress, so I want to make sure that I'm deciding as much as I possibly can. I don't have an Elon Musk-like intellect, so a lot of the decisions won't be perfect. But I must decide. I must try. 