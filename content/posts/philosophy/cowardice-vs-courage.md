---
title: "Cowardice vs Courage"
date: 2021-04-23T18:10:44-04:00
author: "Aaron Bell"
draft: false
---

I choose courage sustained by faith over cowardice sustained by doubt. Cowardice means:

 - avoiding a doable job.
 - dopamine rushes over long-term-supporting disfort. 
 - switching tasks unnecessarily.
 - YouTube, or Discord, or a phone call, instead of putting your eyes on the blinking cursor, hands on the keyboard, ready to type or learn. 

Cowardice is the attempted murder of your ideals, goals, time, and energy. Courage from faith solves this. Courage is:

 - the next step, and the next.
 - paying sanity for your result. 

I think this is also why you need faith, too. Faith restores sanity, so that you may pay more each step. 