---
title: "My future VR company"
date: 2021-06-03T21:42:07-04:00
author: "Aaron Bell"
draft: true
---

Here's a brief glimpse into a VR company I want to build. A pair of glasses sits on the table before you. You reach for them, grab them, and pull them to your face. You watch as the glasses grow in size until they totally surround your eyes and you no longer notice they're there, except for the feeling of them on your ears and around your eyes. Donning these glasses, the kitchen table disappears, and in its place is a white room with a giant, slowly spinning, blue-tinted, ridged-edged circle of metal: a quarter rotating slowly. This shows one example of how the world can change with a new set of lenses and how we aim to reveal new perspectives to people. That is, we aim to show people the money, in terms of immersion-based learning. 