---
title: "RegEx consumption"
date: 2021-05-05T19:22:37-04:00
author: "Aaron Bell"
draft: false
---

I suspect that RegEx "consumption" means incrementing the position of the RegEx pointer while putting it into a bucket with a pending possible match. Then I suspect that discarding is maintaining the position while getting rid of the match because the string failed the regex pattern.
