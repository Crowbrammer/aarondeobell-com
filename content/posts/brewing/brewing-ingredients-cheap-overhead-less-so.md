---
title: "Brewing requires cheap ingredients but has overhead at ~4x the ingredients."
date: 2022-04-21T02:37:51-04:00
author: "Aaron Bell"
draft: false
---

For [a brew I'm thinking of](https://www.homebrewersassociation.org/the-easy-guide-to-making-beer/#video), before the equipment, there's four different malts, three different hops, yeast, and dextrose (<$50). Malt must be milled before steeping it like tea for the wort that will eventually ferment into beer, and this requires I have a mill or access to someone who has a mill (hopefully $0). If I don't buy it pre-milled, I need to hope that Bell's Brewery or One Well can help me figure out how to mill it. With the ingredients considered, I need the equipment to work the ingredients. I need large containers, bottles, a big pot, a bottle wand, plastic tubes, and sanitizer (i.e. Star San) (~$150). $200 total for a first-time brew and $50 per subsequent brew :) 