---
title: "What I understand about Kubernetes (K8)"
date: 2021-03-21T20:38:29-04:00
author: "Aaron Bell"
draft: false
---

I think K8 turns several computers into one highly available computer by using clusters. Clusters have nodes and a control plane. K8 (typically) uses Docker in its nodes(!). Docker spawns all the necessary services that the app'll use. I think each node starts its own app instance on top of this when activated, probably always on in case it needs to be used. Each node has a copy of all the services via Docker, and it's not like "Node A uses services from Node B and some from Node C". Though what refutes this is storage. Maybe one node's the database for all the nodes? I don't know yet. But I do think that from many perspectives a node is a near identitical to the other nodes, used when the other nodes are deleted, down, or at capacity. 

...We'll see how right or wrong I am over time 🙂