---
title: "Aesthetic guidance from the book, Structures"
date: 2022-03-25T17:27:07-04:00
author: "Aaron Bell"
draft: false
---

I'm at a point where I need an eye for aesthetics as much as I need technical ability as my useful website (not yet available) could use some underlying theory to sustain it. What are the principles of aesthetics? What makes someone go "wow"? Why are some things intrinsically worth looking at? I seek answers to my questions in books like [Structures: Or Why Things Don't Fall Down](https://www.amazon.com/Structures-Things-Dont-Fall-Down-ebook/dp/B009G1PHP2) and Wikipedia pages on [Wonders of the World](https://en.wikipedia.org/wiki/Wonders_of_the_World).