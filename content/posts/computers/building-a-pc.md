---
title: "Making twelve-plus PC components work together"
date: 2021-07-07T20:11:32-04:00
author: "Aaron Bell"
draft: false
---

I'm looking to build a PC, and I need to buy twenty or so components that work together to form a great PC. For example, CPUs have something called a chipset. AMD uses one chipset, Intel uses another. The motherboard supports one or the other, not both.  Then there's RAM, the GPU, the storage, power supply, cooling components, the case. I thought this'd be hard, but PC Part Picker's making this easy. (Not sponsored.)