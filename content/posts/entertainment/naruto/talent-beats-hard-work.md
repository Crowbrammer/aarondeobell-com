---
title: "Talent beats hard work (Naruto Season Two spoilers)"
date: 2021-07-19T04:39:54-04:00
author: "Aaron Bell"
draft: false
---

The prelim Chunin exam after the second exam show that hard work doesn't beat talent. It pulls on your heart strings, getting you to root for the underdog who puts more effort than others, but I conclude that despite it's honorable notion (i.e. something that should be done), it only ends one way. Neji nearly breaks Hinata's heart through chakra attacks(?) (with the "Gentle Fist"). Rock Lee fights Gara in one of the final matches. Gara got hurt, but hardly did anything the whole match while Rock Lee went nuts, expending incredible physical energy. Gara has this ability to encase people in sand, the "sand coffin"(?). In the Rock Lee fight, he doesn't have the energy to coffin him entirely, but he catches the left arm and leg. It doesn't kill him, but the sand exerts high pressure and [comminutes](https://www.medicinenet.com/comminuted_fracture/definition.htm) his left arm and leg, rendering his shinobi days over (at least from what we see). 