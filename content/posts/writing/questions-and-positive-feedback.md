---
title: "Questions and positive feedback: springboards for writing."
date: 2023-03-12T09:28:39-04:00
author: "Aaron Bell"
draft: false
---

Before I knew this, my urge to write wasn't constantly at max and veered toward empty, so I often felt a sense of futility which'd cause me to back out after a few minutes of a writing attempt. I started to think that I lacked the biological hardware necessary to get the ideas needed to write. 

Frustrated with my "mental drip" and with a heavy self-image infraction coming up, my subconscious apparently saw my pain and gave me a bolt from the blue idea to get me out of it. I reflected back to my self-help-heavy days where I read a bunch of books like that of Tony Robbins' Awaken the Giant Within. In that book, Tony Robbins (1992) states that thinking is nothing more than asking and answering questions. While that's likely an oversimplification, and thinking is probably more generally a stimulus-response thing, I thought about it and wondered if the lack of ideas may have stemmed from a lack of said questions. I looked at the blank screen and started asking questions to it, and sure enough the screen started flowing with black text. 

In my head, I wondered about this, because I figured there's not _really_ a difference between a question and an idea, but it appears that my brain can generate questions wayyy more easily than just any specific idea. I suppose you have to have the "admission of the unknown" before the brain feels motivated to fill the working memory with things related to it. That said, I know people whose brains "don't turn off", so this is likely a me thing. 

Once I have the ideas down with the help of excessive questioning, I cull and improve them through editing. I've adopted Jordan Peterson's method of editing from his essay writing template, so read that for a better understanding of how I edit. My old process was to have a general intention of improving the sentences, reading each sentence, closing my eyes and then just starting to type new ones, but this would cause tension in my body, and I couldn't sustain it. With this way, I didn't know if my efforts were making things better or not, and I dislike the idea of wasting time: I want the better sentence _now_, or at least know what I did actually caused improvemente. Well today recalled ideas on "positive deviance" from minds like [Hawkins &  Blakeslee (April 1, 2007)](https://www.amazon.com/Awaken-Giant-Within-Immediate-Emotional/dp/0671791540), [Fogg (December 31, 2019)](https://www.amazon.com/Tiny-Habits-Changes-Change-Everything-ebook/dp/B07LC9KDP5), [Maltz & Fury (November, 2015)](https://www.amazon.com/Psycho-Cybernetics-Updated-Expanded-Maxwell-Maltz-ebook/dp/B00SI02BW4), and [HealthyGamerGG (Kanojia, Feb 24, 2023)](https://www.youtube.com/shorts/RuSqQR1xlRA), and I incorporate positive feedback into this editing process instead of just focusing on "better". The idea is to read the sentence and _focus on what it does right_ and then rewrite it with the aim to do more of that. It turns out that I can fly through edits, leaving a trail of higher quality sentences in my wake through emphasis and expansion of the positives. 

To right is to write :) 



##  Bibliography.                      

Robbins, Anthony. (Fireside first edition 1992.) Awaken the giant within. https://www.amazon.com/Awaken-Giant-Within-Immediate-Emotional/dp/0671791540

Peterson, Jordan. (No date.) \[Essay template\]. https://lobster-writer.co.uk/Jordan-Peterson-Writing-Template.docx

Hawkins, Jeff & Blakeslee, Sandra. (April 1, 2007.) On intelligence: how a new understanding of the brain will lead to the creation of truly intelligent machines Adapted edition, kindle edition. https://www.amazon.com/Intelligence-Understanding-Creation-Intelligent-Machines-ebook/dp/B003J4VE5Y

Fogg, BJ. (December 31, 2019.) Tiny habits: the small changes that change everything Kindle edition. https://www.amazon.com/Tiny-Habits-Changes-Change-Everything-ebook/dp/B07LC9KDP5

Maltz, Maxwell & Fury, Matt. (November 2015.) Psycho-cybernetics updated and expanded.  https://www.amazon.com/Psycho-Cybernetics-Updated-Expanded-Maxwell-Maltz-ebook/dp/B00SI02BW4

@HealthyGamerGG. (Feb 24, 2023.) What is positive deviance? https://www.youtube.com/shorts/RuSqQR1xlRA

(As of March 12th, 2023, I need to make my bibliographies prettier.)