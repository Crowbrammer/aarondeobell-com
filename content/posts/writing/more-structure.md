---
title: "For structure and ease of consumption, I want to \"purpose-ify\" and outline my communication"
date: 2021-08-19T15:22:34-04:00
author: "Aaron Bell"
draft: false
---

I want to structure my communication better. [Adler in Chapter 7 of How to Read a Book](https://www.amazon.com/How-Read-Book-Classic-Intelligent-dp-0671212095/dp/0671212095) recommends IDing the unity and then the parts of the unity when reading. Explaining this, he says the writer should ideally consider this when forming his material. If he did, then the unity and structure'd be obvious. His comment on this motivates me to consider doing this. Going forward, I'll start purposing and outlining my communications before responding instead of just "going with the flow". 