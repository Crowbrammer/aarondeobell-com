---
title: "How I'm handling my outlining issue"
date: 2021-05-03T10:44:55-04:00
author: "Aaron Bell"
draft: false
---

[Jordan Peterson's traditional essay writing method](https://lobster-writer.co.uk/Jordan-Peterson-Writing-Template.docx), which I try to model,  has you select a topic (a question) and break it down into parts with an outline. You then read, draft, edit, and organize (i.e. "write") to these specific points. Because I didn't understand the topic enough to know what I didn't know though, my outlines were weak and not worth writing to. To counter this, after making the topic I should read and make 300 "note words" on the topic so that I can outline from an informed place. I intend to then draft the outline like I would a paragraph, editing and refining it through a similar process as I do with my paragraphs before using them as the basis of an entire essay. Then... I'll use it write an essay! 