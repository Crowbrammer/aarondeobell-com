---
title: "All <svg> attributes are optional, not required"
date: 2022-04-13T22:36:40-04:00
author: "Aaron Bell"
draft: false
---

The otherwise extremely helpful [MDN SVG docs](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Positions) overcomplicate the `<svg>` element for beginners. They throw in viewBox, xmlns, width, and height attributes for the svg canvas when not a single one is required, and I think this will frustrate the beginner who'll think these attributes are required. It's only with serious study and experimentation did I learn they were not required. One will almost never use the viewBox (how often do you simply zoom in on an subsection of an image instead of just show the whol image), refer to xmlns (the browser's default works fine), nor use the width or height attributes (which'll crop your image). The svg tag will adjust the canvas to the size of your svg elements, and that's fine to start. 

Looking at MDN, it appears I can propose changes to the docs...