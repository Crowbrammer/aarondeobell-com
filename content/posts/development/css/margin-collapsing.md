---
title: "Margin collapsing: Adjacent elements share the biggest vertical margin between them, ignoring the lesser of the margins"
date: 2022-04-12T08:38:45-04:00
author: "Aaron Bell"
draft: false
---

In CSS, [the browser "collapses" adjacent element margins](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing). Two elements share the same direct parent. One's bottom margin of 30px touches another's top margin of 10px. The browser sees the 10px margin exists but doesn't use it (collapses it), and the two elements share the top element's 30px bottom margin. If you highlight the two elements in Devtools, the two margins will still show, but the top margin will overlap with the bottom here, rendering it unused. 

![Shared margin between h1 and p tag](https://dl.dropboxusercontent.com/s/o2m56m7kro0z2sg/msedge_2022-04-12_08-32-49.gif)

Wrapping the two elements in divs to give them different parents didn't affect the margin collapse. I expected the two elements that have a different parent but appear to touch--the same bottom and top margin--to increase to keep both margins for 40px of margin, but I see it still shares the 30px. 