---
title: "A browser takes html, makes a DOM tree, then makes a render tree, organizes it, then paints it."
date: 2022-04-11T18:33:37-04:00
author: "Aaron Bell"
draft: false
---

A browser's rendering engine always uses [the same five-step process](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#The_main_flow), potentially with extra steps. To create a webpage from html/css data, the browser takes the HTML, makes a DOM tree, constructs a render tree from the DOM tree, organizes it (lays it out), then paints it. Different browsers have [different rendering engines](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#Rendering_engines) to accomplish this: Trident (IE), Webkit (Safari), Gecko (Firefox), and Blink (fork of Webkit; used by Chrome, Edge(?), and Opera). [W3C maintains CSS specs](https://www.w3.org/TR/CSS/), explaining how each of these engines should implement their rendering engine to conform to CSS standards.