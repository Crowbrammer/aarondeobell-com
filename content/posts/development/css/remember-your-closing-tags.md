---
title: "Remember your closing tags"
date: 2021-06-23T23:50:21-04:00
author: "Aaron Bell"
draft: false
---

To exercise my CSS skills, I tried placing two 100px by 100px divs beside each other. After putting two div tags in the html and setting the height and width of divs to 100px, my Codepen preview showed one box, not two. 

![One box, not two, shows](https://dl.dropboxusercontent.com/s/5y0w705s1rx230x/2021-06-24_00-01-05.png)

Did Codepen not render multiple divs? Did it have overriding, wonky CSS? To find out, I spawn a fresh index.html and styles.css file with the same div ruleset. Two divs in the html later, I see two blue boxes in the live preview. 

It has to be Codepen. I want to see the CSS for the divs, so I right click the Codepen box/div and select "inspect". Before I get to the CSS properties of the box, I see that one div contains another div. I find this weird, so I look back at the html. I see the obvious missing slash in the closing tag and change it. A second later, the blue box divides into two blue boxes. It wasn't Codepen.

![Forgot the closing tags](https://dl.dropboxusercontent.com/s/r6cxgfnuhdtkd4f/2021-06-24_00-03-07.gif)