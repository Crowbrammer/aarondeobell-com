---
title: "CSS document flow"
date: 2021-08-02T10:06:26-04:00
author: "Aaron Bell"
draft: false
---

If I understand document flow and three properties' effects on it, I can determine where elements will land. The `display`, `float`, and `position` properties manipulate elements from their default position in the "document flow".  `display` affects an elements response to box model properties: `margin`, `border`, and `padding`. It also decides whether elements can sit horizontal to it: "inline" yes, "block" no. `float` pulls things out of the normal flow and makes the normal things flow around it. The `position` property determines how something treats its default position in the flow. [One can classify `position` values as "in-flow" and "out-of-flow"](https://soulandwolf.com.au/blog/what-is-document-flow/) (Rosato, 21 December, 2018).  There are two in-flow positions: "static" and "relative". In-flow elements keep their spot unless offset with `top`, `right`, `down`, or `left` or a transform. Out-of-flow positions not discussed here. 