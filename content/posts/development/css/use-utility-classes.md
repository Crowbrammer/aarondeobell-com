---
title: "Reduce the amount of CSS you need to write with utility classes"
date: 2022-04-12T10:20:50-04:00
author: "Aaron Bell"
draft: false
---

Utility CSS trumps semantic CSS! What changed me CSS-wise was when I analyzed [Coub.com](https://coub.com/)'s CSS and saw the "-always-visible" and "-scrolled" classes and wondered "why do that"? I understood that these were called utility classes and DuckDuckGo-ed "utility classes". I stumbled [on an article](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/) that described the switch from "semantic" to "utility" describing it as a "functional" approach to CSS. This reminded me of Tailwind, and sure enough this was written by the creator of [Tailwind](https://tailwindcss.com/) in 2017. His word "functional" made me immediately understand the role of utility classes. 

Despite my functional mindset since I switched to Clojure, I've been writing the equivalent of OOP in CSS*. OOP-style things encapsulates unnecessary data into things and prevents other things from easily using the data in these "object" boxes.  Functional programming makes data more freely accessible, usable by anything that needs it. Correcting my errors, now instead of having a menu that's centered and then a card that's centered, I simply have a "center" class and give any element that needs centering this class. I write the centering code once instead of many times for different elements. These benefits cumulate for every CSS action you do, so I think it's nuts to code CSS any other way than functionally with utility classes. 