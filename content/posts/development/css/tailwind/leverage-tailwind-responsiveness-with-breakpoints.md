---
title: "Design for every size from mobile to massive displays with Tailwind's breakpoints"
date: 2022-04-20T17:49:02-04:00
author: "Aaron Bell"
draft: false
---

[Tailwind's responsive nature](https://tailwindcss.com/docs/responsive-design) works via breakpoints. Every single class in Tailwind works with the breakpoints: none, `sm`, `md`, `lg`, `xl`, and `2xl`. Breakpoints specify when a style should start, meaning the styles persist through all larger screen sizes unless they're "undone" with another style at a larger breakpoint. That is `md:<your style>` will go through the `lg` and `xl` breakpoints unless you say `xl:<your overriding style>`. Start with none to cover mobile and bigger, then imagine the screens get larger, see how they should look and add breakpointed styles at the bigger sizes, maybe [increasing the text size](https://tailwindcss.com/docs/font-size) with: `lg:text-lg` (you don't set specific numbers in the style, you customize it in the Tailwind config). 