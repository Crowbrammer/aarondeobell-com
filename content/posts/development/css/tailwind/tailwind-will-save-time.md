---
title: "Tailwind will likely save me CSS-coding time"
date: 2022-04-20T17:12:01-04:00
author: "Aaron Bell"
draft: false
---

I subscribe to [the functional/utility mindset now for CSS](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/), so I know now that if I do CSS I'd make utility classes of my own, so why go through that work when [Tailwind](https://tailwindcss.com/) already likely has everything I'd probably make myself. I'll save so much time looking through their library and class names instead of trying to invent everything myself. I'll likely spend five minutes of documentation-hunting for every ten minutes of personal CSS coding I'd do myself. Perhaps CSS should only be used in intense animation cases and where I need minute differences to the Tailwind library. 

There's also the disfort of having to use a ton of classnames where I'm used to writing just one or two, but I believe the payoff will be so intense that I'll grow use to the long class list. Go long utility class lists!

```html
<figure class="md:flex bg-slate-100 rounded-xl p-8 md:p-0 dark:bg-slate-800">
```