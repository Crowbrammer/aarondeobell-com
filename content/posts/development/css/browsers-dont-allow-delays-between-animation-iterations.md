---
title: "Browsers don't allow delays between animation iterations"
date: 2023-01-15T11:31:32-05:00
author: "Aaron Bell"
draft: false
---

CSS animations can be repeated with delays between each iteration, albeit with difficulty in both the all-CSS and JS approach. 

There's no built-in CSS property for "delay between each cycle" for CSS. One CSS-only workaround is the [cubic-bezier timing function to create the delay] (https://stackoverflow.com/questions/13887889/css-animation-delay-in-repeating/13927380#13927380).

For using JS, I tried the obvious approach of setInterval with removeClass and addClass, but this doesn't work with animations. 

The browser's renderer (?) - in animation cases at least - doesn't reanimate on property writes but only on property reads of something on the animated element's branch (the parent, the animated element itself, or its children). 

You can't read properties of elements outside the branch and cause an animation. I think this prevents animations for untouched elements. 

(Updated at 03/17/2023 21:47:28)