---
title: "In the standard CSS box model, width and height affect only the content box"
date: 2022-04-11T20:18:12-04:00
author: "Aaron Bell"
draft: false
---

CSS offers two box models: [standard](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model#the_standard_css_box_model) and [alternate](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model#the_alternative_css_box_model). These two models dictate which parts of the box `width` and `height` affect. In the standard, they only affect the content box--the padding and border are added after. In the alternate, the padding and border are considered with the width. To use the alternate box model, set `box-sizing` to `border-box`. To make the entire page use the alternate model, modify the html element to use `box-sizing: border-box` and make all the elements beneath it inherit it:

```css
  html {
    box-sizing: border-box;
  }
  *, *::before, *::after {
    box-sizing: inherit;
  }
```