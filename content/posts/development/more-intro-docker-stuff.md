---
title: "More intro Docker stuff"
date: 2021-06-04T22:25:44-04:00
author: "Aaron Bell"
draft: false
---

Over the past half hour, I learned to write a recipe for building an image called the Dockerfile. I then told Docker to make a cake called an image with the `docker build` command. I then told it to throw a party around this cake, to run a container. I made some changes to some files (to something the recipe uses) and rebaked the cake. The old party was using a room that I wanted, so I stopped the old party (stopped running the container) to free up that room (the 3000 port), and I threw a new party (ran the new container) with that cake in the center. 