---
title: "Using classes in ClojureScript"
date: 2021-07-01T13:15:21-04:00
author: "Aaron Bell"
draft: false
---

Whenever I see a class in js, just do `(js/className. arg1 ... argN)`. Treat the dot at the end as the `new` keyword in js. Pass an audio/visual stream to a MediaRecorder with `(js/MediaRecorder. %)`.
