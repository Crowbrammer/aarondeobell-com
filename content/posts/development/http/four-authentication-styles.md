---
title: "Authorization: Bearer TOKEN"
date: 2022-01-17T22:54:24-05:00
author: "Aaron Bell"
draft: false
---



You can authorize http requests in four main ways: Two involve the Authentication header, two involve request parameters. You can use a 1) Basic or 2) Bearer Authentication header, or you can pass a token/un-pass combo to a 3) query paramater or 4) form parameter. The value of the Authorization header is a space-delimited tuple. As an http newbie, I wondered how to pass authorization tokens in. Do I pass another token in? Do I paste it into a new line? (I think headers are newline delimited in http/1.1, but I didn't know that at first). I feel uncomfortable about this syntax, and I wish it were separated into two headers: Authentication-Type and Authentication-Credentials. 