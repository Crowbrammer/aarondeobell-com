---
title: "Understanding HTTP"
date: 2021-08-06T21:58:49-04:00
author: "Aaron Bell"
draft: false
---

I should learn the tools to manipulate HTTP payloads using the given control information. I see a lot of http libraries that'll let me use the browser to interact with a server, but I didn't understand HTTP itself. Now that I get that it's "stuff" and "how to send and use the stuff", I need to find robots that process the stuff and directions for using the stuff. I.e. I have some work to do learning Ring/Jetty for Clojure. Also need to learn to send blobs. 