---
title: "Sending the client's audio data to a server with HTTP"
date: 2021-07-08T21:12:12-04:00
author: "Aaron Bell"
draft: false
---

I have a personal development app idea ("the imagestreaming app"), where the server should accept, store, and organize the client's audio data (their description of internal imagery). In this post, let's forget the storage (do I store on a db or a filesystem?), and focus on getting the data from the user's machine to the server. I believe this should be done with an HTTP POST request which a Ring or Compojure server should listen for. I don't know http well enough to know, but I guess thath there's a size limitation with http requests. I'm used to sending small data, mostly text, to the server, so I never had to consider the size of the http requests. But how is text any different from audio? They're both binary to the computer. I just need to learn how to send bigger blocks of binary and store it in audio format. I'm guessing with the size, it'll need to be done with multiple HTTP requests, and the server code'll need to stitch it together, likely with a library. 