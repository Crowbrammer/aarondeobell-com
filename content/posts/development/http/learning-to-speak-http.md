---
title: "Learning to speak http"
date: 2021-07-22T08:56:44-04:00
author: "Aaron Bell"
draft: false
---

I think to speak with http servers and get their info, I need to know the anatomy of http requests. This means knowing how to populate a request with params, a body, and headers. Something that surprised me a while back is that an http request has multiple headers. Instead of anthropomorphizing the request, hydraize it. It's a hydra, or a Cerberus: there are many headers. One header is "Authorization" while another is "Accept-Encoding". 

Also to speak with http servers, after knowing the anatomy, I need to know about "http clients" and how to select and use one. Before I knew the term "http client", I had difficulty finding ways to create these requests. After I learned the term though, I found a whole arsenal of tools to speak with web servers. You probably use Python or JavaScript. I use Clojure. The language doesn't matter as long as you know the term "http client", because you can use this term to find libraries for your language to create http requests.

I guess that once you understand the anatomy, and once you understand your http client, you can start to speak with web servers, probably while keeping the API documentation tab on your second monitor. 