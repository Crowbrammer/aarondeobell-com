---
title: "Dogecoin Twitter bot"
date: 2021-06-14T10:13:31-04:00
author: "Aaron Bell"
draft: false
---

To make a hopefully-useful Twitter bot, I learned to tweet from a program! Here's what I did:

 1. Sign up for [a Twitter dev account](https://developer.twitter.com/) (expect lotsa writing)
 2. Wait until accepted
 3. Have a Twitter API library (like [twitter-api for Clojure](https://github.com/adamwynne/twitter-api)) for your language imported into your project
 4. Log out of the current account and log into what you want the app to control
 5. DL and install [Ruby](https://www.ruby-lang.org/en/)
 6. Run `gem install [twurl](https://github.com/twitter/twurl)` on the CLI
 7. *From your OS's native CLI* (this didn't work for Bash for Windows, only on Powershell) run `twurl authorize --consumer_key=[key] --consumer_secret=[secret]`. [There are literally six different terms describing what a consumer key](https://dl.dropboxusercontent.com/s/mmtcqjs5uf0tbiv/2021-06-14_10-16-03.png) is [here](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens). It's your API key for your project.
 8. Go to your home folder and open the .twurlrc file.
 9. Pull the user access token and secret (I forget what Twurl calls it)
 10. Paste it wherever your program's API asks for it
 11. Use your library's functions to send requests to the API