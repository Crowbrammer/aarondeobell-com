---
title: "Parking vs. blocking makes no difference to me atm"
date: 2021-06-29T06:28:34-04:00
author: "Aaron Bell"
draft: false
---

When using asynchronously running processes, i.e. with [go](https://clojuredocs.org/clojure.core.async/go) [macros](https://www.braveclojure.com/writing-macros) or [futures](https://clojuredocs.org/clojure.core/future), [blocking or parking](https://www.braveclojure.com/core-async/#Blocking_and_Parking_) make no difference to me atm. Atm I see no reason not to simplify things, using [<!!](https://clojuredocs.org/clojure.core.async/%3C!!) exclusively instead of [<!](https://clojuredocs.org/clojure.core.async/%3C!) sometimes in a `go` block. 

-------

```
;; Control, can use multiple expressions in the go body
(def my-chan (chan))
(println 1)
(go (println 2) (println 3))
(println 4)

;; (def my-chan (chan))
(println 1)
(go (>! my-chan "Hi") (println 2))
(println 3)

;; (def my-chan (chan))
(println 1)
(go (>!! my-chan "Hi") (println 2))
(println 3)
```
--------------

Neither #2 and #3 never print `2`. What's the difference? 

----------

```
;; (def my-chan (chan))
(println 1)
(go (>!! my-chan "Hi") (println 2))
(go (>!! my-chan "Hi") (println 2))
(go (println 2))
(println 3)

;; (def my-chan (chan))
(println 1)
(go (>! my-chan "Hi") (println 2))
(go (>! my-chan "Hi") (println 2))
(go (println 2))
(println 3)
```
---------------

Both these sections do the same thing. From my perspective, >! performs exactly like >!!. Why complicate the asynchronous headspace with "parking" and "blocking", when both block each's current process? 