---
title: "I timeboxed my coding tasks to gain ugly-but-working code"
date: 2021-08-15T12:41:40-04:00
author: "Aaron Bell"
draft: false
---

Nasty-looking code can work (well), esp. for indy one-off projects. What I did is have a list of things that needed to be done and set time limits for the first four things. I timeboxed these four things. Before the deadlines, I wondered how I'd get these things done. I used excuses like, "The pictures in my head are virtually non-existent" (I'm pretty much aphantasiac, thus my propensity for words and writing), but with the specific tasks and timelines, hardish seeming code started filling out before my eyes. Where I felt discomfort, I started seeing progress, until over a period of a couple hours I made more progress than I did in months. The trade-off for this productivity? My code looks like ass and works. But what does this ass-looking code compare to? No code at all, which doesn't give me the app that I'm writing this on. 