---
title: "Dave Martin's a Re-Frame master"
date: 2021-11-28T19:59:28-05:00
author: "Aaron Bell"
draft: false
---

How did [Dave](https://github.com/DaveWM) use [Re-Frame](https://day8.github.io/re-frame/re-frame/) to make [Lobster Writer](https://github.com/DaveWM)? He has the classic `pages` function which takes the subcription to the `:active-page` key of the db. This gets passed into a `case` which returns another component function, which gives whatever page. Clicking buttons changes this `:active-page` key, which updates the subscription, and gives a different page. Wielding `reg-event-db`, `reg-event-fx`, `reg-fx`, `reg-cofx`, and interceptors;  using routing libraries like [Bidi](https://github.com/juxt/bidi) and [Pushy](https://github.com/kibu-australia/pushy); and working with misc libraries like [React Quill](https://zenoamaro.github.io/react-quill/), he makes a fluid and navigable SPA that works locally to streamline one's writing of beautiful essays. 
