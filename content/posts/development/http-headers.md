---
title: "An http header is not a collection of key-value pairs: it is a key-value pair"
date: 2021-05-15T03:58:09-04:00
author: "Aaron Bell"
draft: false
---

I misunderstood http headers. There are typically many headers on an http request, and the server uses this to decide what to do with the request, kind of like a passport for a person arriving at an airport. I used to think an http header, this passport, was a map of key-value pairs, but an http header _is_ a key-value pair. When you add a new key-value pair to the "headers" key of the http request, you're adding a new header.