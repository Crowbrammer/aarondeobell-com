---
title: "Getting microphone permission in JS"
date: 2021-07-01T06:52:06-04:00
author: "Aaron Bell"
draft: false
---

Use `js/navigator` (ClojureScript's way of accessing [JS's Navigator](https://developer.mozilla.org/en-US/docs/Web/API/Navigator)) to pull the user's microphone stream. I haven't recorded the stream data yet, but I've finished the first step: Getting the user's permission. You can't ask for mic permission straight away; you need the JS tools need to load, which means creating an `onload` handler (function) for the js/window. In [ClojureScript](https://clojurescript.org/guides/quick-start):

```
(set! (.-onload js/window)
      (fn [] (js/navigator.getUserMedia #js {:audio true} #(println "Success: " %) #(println "Error message: " %))))

;; #object[MediaStream [object MediaStream]]
```
I tested switching between js/document and js/window. Only js/window did the trick. 