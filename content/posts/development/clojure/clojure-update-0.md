---
title: "Learning Clojure"
date: 2021-01-06T11:18:37-05:00
author: "Aaron Bell"
draft: false
---

I started learning [Clojure](https://clojure.org/guides/getting_started) to help update a site written in [ClojureScript](https://clojurescript.org/guides/quick-start) called [Lobster Writer](https://lobster-writer.co.uk/), which I'm using now to write this. Before all of this, I didn't know what a "lisp dialect" nor what Clojure(Script) or was. To rectify this, I've taken a lot of unpublished actions until now. I've read Ch. 1, 2, and 10 of Clojure for the Brave and True (Higginbotham, 2015), read several papers, John McCarthy's (June 1960) [original Lisp essay](http://www-formal.stanford.edu/jmc/recursive.pdf), Paul Graham's [essays](http://www.paulgraham.com/lisp.html) (where I learned about McCarthy), and then Richard Hickey (2020), the creator of Clojure's, ["The History of Clojure"](https://download.clojure.org/papers/clojure-hopl-iv-final.pdf). 

I have some great study habits, so I set some goals related to these habits. I want to complete all of Clojure for the Brave and True (Higginbotham, 2015)... and all [156 4Clojure exercises/konas](https://dl.dropboxusercontent.com/s/h1v34vs1kurn6is/2021-01-05_21-19-22.png) this month. Higginbotham (2015) wrote [thirteen chapters and two appendices for Clojure for the Brave and True](https://dl.dropboxusercontent.com/s/7r2xpusx1lwe6nb/2021-01-05_21-18-52.png). I intend to finish it way sooner, but worst case scenario, I need to read and grok one chapter every two days. Given my study habits, [sixteen "study sessions" today alone](https://dl.dropboxusercontent.com/s/c2hdnjif00n0dy4/2021-01-05_23-30-12.png) when I didn't have to, lets me think that I can do this. (The times on the right are "durations between study sessions", so the shorter the time the better).

## I've Learned Lisp, Basic Clojure Principles, and emacs

In all this, I learned about Lisp, S-expressions, cons cells and their CAR and CDR slots (and Clojure's abstraction of it, the `seq` with the `first` and `rest` slots), Lisp's basis in math and how that leads to extreme practicality and faster development, and more. I learned that Lisp is powerful, but not omnipresent, so for many reasons (like automated garbage collection) Hickey deployed the Lisp called Clojure in 2007 and has been improving it ever since (see the timeline in Hickey's paper). I'd later learn more practical things, like prefix notation (`(+ 1 2) => 3`), starting the REPL (Clojure works with Java, i.e. a jar file's needed to work with Clojure), using Leiningen, and now emacs. 

I think I can measure my progress/competency based on my knowledge and skill for these factors:

 - Clojure, general (3/10)
 - Lisp (2/10)
 - Create a Clojure project (4/10)
 - REPL (6/10)
 - DOM work (2/10)
 - Can I output it to Node.js (8/10)
 - Leiningen (4/10)
 - emacs (6/10)


Because I just finished learning a bit of emacs, I wrote a companion post alongside this. [See my emacs post here](https://aarondeobell.com/posts/clojure/intro-to-emacs/). May the gifs help you to grok emacs commands faster. 

## Schedule to Finish C4TBAT and All 4Clojure

I don't know the difficulty, so I may vary from or completely fail this schedule, but I intend to follow this:

|Day | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Lesson  |
|---- |----|---------|
|Today (2021-01-06) || Ch. 3, & 4 of C4TBAT |
|Tom|| 5 & 6 C4TBAT |
|7th|| 7 & 8 |
|8th|| 9 & 10 |
|9th|| 10 & 11 |
|10th|| 12 & 13 |
|11th|| Appendices |
|12th-30th|| 4Clojure problems |
|&nbsp;|&nbsp;|&nbsp;|

To keep the big picture in mind, with insight from [Adler's (1972) How to Read a Book](https://www.amazon.com/How-Read-Book-Touchstone-ebook/dp/B004PYDAPE), I plan on doing a daily "Stage Two: Inspectorial Reading" of C4TBAT. Whenever I find a concept I don't know, like this code snippet for instance--(map #(sock-count % 2) ; what does the % do here?--I intend to get "Stage Four" the concept, starting with collecting a bibliography and Stage Two reading (inspectorially read) each source for a better idea. 

And that's about it. Back to other things.

## References

Adler, Mortimer. (2011 [Preface written in March 26, 1972]). How to read a book. https://www.amazon.com/How-Read-Book-Touchstone-ebook/dp/B004PYDAPE

Graham, Paul. (May 2001). The roots of lisp. http://www.paulgraham.com/rootsoflisp.html

Hickey, Richard. (June 2020). A history of Clojure. https://download.clojure.org/papers/clojure-hopl-iv-final.pdf

Higginbotham, Daniel. (2015). Clojure for the brave and true. Basic emacs. https://www.braveclojure.com/basic-emacs/.

McCarthy, John. (April 1960). Recursive functions of symbolic expressions and their computations by machine [i.e. Lisp]. http://www-formal.stanford.edu/jmc/recursive.pdf
