---
title: "Understanding Clojure, Java, and Leiningen"
date: 2021-03-26T01:32:24-04:00
author: "Aaron Bell"
draft: false
---

I'm trying to understand `lein run`, Clojure, Java, and how they work together. So far, here's how I see it. [`lein run`](https://github.com/technomancy/leiningen/blob/master/src/leiningen/run.clj) automates something... but what? Clojure's built with and uses Java to run. Java needs Java bytecode to work. Once that's available, Java needs a classpath and an entry class with optional arguments. (You need to know this from the docs/original code, because it's going into the 0s and 1s of bytecode). With this it runs the main function of the entry class. Compiled Clojure code makes the namespace a class and the -main function the main function (?). This lets java access the Clojure code--with the clojure.jar and your compiled code in the class path--work with it and do stuff with it. It looks into the namespace-turned-class and invokes its `main` function. Am I looking at this the right way?

*Bibliography*

Higginbotham, Daniel. (12 March, 2013). How Clojure babies are made: ompiling and running a Java program. https://www.flyingmachinestudios.com/programming/how-clojure-babies-are-made-the-java-cycle/

Higginbotham, Daniel. (18 March, 2013). How Clojure babies are made: understanding lein run. https://www.flyingmachinestudios.com/programming/how-clojure-babies-are-made-lein-run/

@technomancy. (Oct 21, 2017). run.clj https://github.com/technomancy/leiningen/blob/master/src/leiningen/run.clj

Miller, Alex. (n.d.) Getting started. https://clojure.org/guides/getting_started#_clojure_installer_and_cli_tools


