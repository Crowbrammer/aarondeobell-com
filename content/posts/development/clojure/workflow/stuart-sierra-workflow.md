---
title: "In Clojure, define the app in a local state, not the global state, to avoid waiting on the JVM to restart"
date: 2022-05-27T13:04:39-04:00
author: "Aaron Bell"
draft: false
---

Faster than restarting the JVM: Make the app constructable from a function and use the function's result. When you want to restart the app, use a `stop` function and a `go` function (or a new function that does both, `reset`) that destroys the old instance and creates a new instance of the app. 

The default approach is to have the default scope of be the initial scope. [The way Stuart Sierra proposes](https://cognitect.com/blog/2013/06/04/clojure-workflow-reloaded) has you use the highest scope as an app instance manager instead, with all your app's work done one local scope down. With the default scope being the initial scope, your only choice of buildup and teardown is to restart the JVM. With everything in the local scope, you can use the initial scope to tear away that local scope and build up a new one, with a fresh state. In the functions used to start up the new app, you'll also use tools to pull in the new source code for both fresh state and up-to-date code. 