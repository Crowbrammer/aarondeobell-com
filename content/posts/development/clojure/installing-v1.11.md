---
title: "Clojure's install .sh script will replace the existing Clojure version (no need to uninstall)"
date: 2022-03-29T14:09:22-04:00
author: "Aaron Bell"
draft: false
---

I wanted to try a new version of Clojure: Clojure v1.11. My first thought was, "How do I uninstall Clojure v1.11", but when I visited the docs, the [Getting Started guide](https://clojure.org/guides/getting_started) on clojure.org mentions only installing. I attempted to download the new Clojure despite not installing, and in the install directions I saw a custom install directory option. I didn't want Clojure to be in my home directory, so I used that and ran `clojure --version`, but I still saw version 1.10.3. No bueno. I leaned back and zoned out at my monitor for a bit until I got the idea to peruse the install shell script. At the bottom of the script, there's a command to remove the Clojure install download, meaning my concern for a messy home directory was unfounded. I thought, "okay, maybe I don't need the custom directory", and I ran the default install then ran `clojure --version` after which gave me v1.11. `ls`ing my home directory showed no straggling install script, meaning the home directory is nice and clean. 