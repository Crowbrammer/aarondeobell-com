---
title: "Using a Docker-Clojure REPL"
date: 2021-06-08T02:52:02-04:00
author: "Aaron Bell"
draft: false
---

I'm trying to use a Docker instance of Clojure instead of a host instance of Clojure. Why? I have computers both at home and at my parents' place, and I don't want to deal with setting up Clojure on multiple environments. I'd like to pull in a repo and spawn a Docker container and boom, I can start developing. That is, I would like to be able to start right away anywhere that I have Docker. To do this, I think I should do something with volumes. Have a repo with instructions to build the Docker image with the director around it, and when I use emacs/CIDER from within a container, it'll modify the files outside of it. This will persist the files after the container's gone and let me push the stuff. I'll probably need to configure Git each time I do this until I come up with a better solution, but so far this'll cover the emacs/CIDER/Clojure/portability problems all in one go. 