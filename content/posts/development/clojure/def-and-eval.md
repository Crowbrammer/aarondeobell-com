---
title: "`def` and `eval`"
date: 2021-04-14T04:06:56-04:00
author: "Aaron Bell"
draft: false
---

While trying to use `def` in a non-macro using symbols, I erred by giving eval non-lists, `(eval def 'hi "Hello")`. `eval` is a list processor, so `eval` needs a list to work. `def`, `'hi`, and `"Hello"` aren't lists, so this errored. I first thought quoting `hi` made it not register the var(?), but no: I passed in a non-list. By changing it to: `(eval '(def hi "Hello"))`, *or* `(eval (list 'def 'hi "Hello"))`, *or* `(eval (read-string "(def hi \"Hello\")"))`, `eval` produces the expected result. 