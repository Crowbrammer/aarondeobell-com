---
title: "What I learned solving all 156 4Clojure problems"
date: 2021-03-19T21:15:46-04:00
author: "Aaron Bell"
draft: false
---

![proof](https://dl.dropboxusercontent.com/s/vl41a0tb2mbtkfw/2021-03-19_23-11-31.gif)

I solved them all. Here's the jist of what I've learned:

 - Psycho-Cybernetics works
 - Diffused state for creative shit
 - Can meditate into it
 - Read Mortimer Adler style
 - TDD's useful

# [Psycho-Cybernetics](https://www.amazon.com/Psycho-Cybernetics-Updated-Expanded-Maxwell-Maltz/dp/0399176136) works

I knew about [Psycho-Cybernetics](https://www.amazon.com/Psycho-Cybernetics-Updated-Expanded-Maxwell-Maltz/dp/0399176136) going through this, but it hit home for me when I spun my wheels on a big problem (I forget which). I took a break and read, [Mortimer Adler style](https://www.amazon.com/How-Read-Book-Touchstone-ebook/dp/B004PYDAPE), all of Psycho-Cybernetics. I did as Adler suggested and outlined the whole book, and I came to understand it as this: If your body were a “gun”, what it points at is your goal, your physical health is the “bullets”, and feeling strongly squeezes the trigger. Some goals are hunted with a single shot. Most goals are big and take many (hundreds of) bullets to bring home the game. I think the reason I succeeded in [solving all 156 4Clojure problems](https://aarondeobell.com/posts/clojure/clojure-update-0/) is because I used this and pointed at the target and “squeezed the trigger” hundreds of times until I brought it down. That is: I kept the goal in mind and felt strongly about it for a period of two and a half months until I solved them all. Psycho-Cybernetics works.

# Get creative with the diffused state

There were many problems that would've overwhelmed me if I didn't: 1) believe in Psycho-Cybernetics, and 2) understand the diffused state where new "ahas" are found. I talked about #1. For #2, Isaac Asimov (1971) wrote about this with the [“The Eureka Phenomena”](https://www.yumpu.com/en/document/read/4657323/the-eureka-phenomenon-by-isaac-asimov) detailing this state with five examples spanning over two thousand years, from the time of Archimedes with the crown problem in Syracuse circa 250 B.C. to himself solving a key problem in his dissertation circa 1960. Barbara Oakley [shows how Edison’d relax with ball bearings](https://youtu.be/O96fE1E-rf8?t=518) in his hand. When he’d nearly fall asleep, it’d drop and wake him up, and he’d apply the insights from the diffused state to his problems. Dr. Kanojia of recent fame, a Harvard-trained psychiatrist who’s worked with many famous personalities like Pokimane, Asmongold, Reckful, [says about studying](https://youtu.be/Lw4dMehQkgs?t=240), “The first thing to understand about studying is the more calm or tranquil your mind, the more receptive it is to information”. I don't have detailed records of my "diffused state" experiences, but I can tell you generally that's the state that gave me the "aha" needed to solve hard problems. 

# Meditate into the diffused state

Because I viewed Dr. K's [meditation videos](https://www.youtube.com/watch?v=dvq48pfJa9w&t=1743s), I understood meditation's ability to put me in the diffused state, so I did it consistently while solving these problems. I opted for the “doing meditation”, and I went on to use it with much success. That is, I’d set a two minute timer every time I needed a break or a “creativity” spurt and just zone in on a single point in my room, letting it crowd out thoughts of the problem and everything else. Not only did this relax my mind and body, but I came back with strength and many, many times I got an aha and I solved it. Without this technique, I don’t think I would’ve solved the problems–soon or ever.

# Read [Mortimer Adler](https://www.amazon.com/How-Read-Book-Touchstone-ebook/dp/B004PYDAPE) style

On the journey to solving the 4Clojure problems, you'll need to read about [K-maps](https://en.wikipedia.org/wiki/Karnaugh_map), [minterms](https://en.wikipedia.org/wiki/Canonical_normal_form), [Gray code](https://en.wikipedia.org/wiki/Gray_code), [binary relations](https://en.wikipedia.org/wiki/Binary_relation), [bijections](https://en.wikipedia.org/wiki/Bijection), [set theory](https://en.wikipedia.org/wiki/Set_theory), and more. It's possible you're educated in these concepts, but if you aren't you'll need read and understand them. Nothing's helped me with reading comprehension like Mortimer Adler's book, [*How to Read a Book*](https://www.amazon.com/How-Read-Book-Touchstone-ebook/dp/B004PYDAPE). There's some tedious processes in there that may strike you as dumb, but they really, really work. For instance, a K-map is a truth table squeezed into a different kind of table where the axis are incremented with Gray code, which looks like binary digits but differs in that one digit changes at a time to get to the next digit (which doesn't happen in traditional binary). This allows you to plot things on a map, make base-2 groups of cells with 1's in it, find what doesn't change, and boom, these groups give you a minimized equivalent Boolean function that would otherwise take eight or sixteen or so minterms/maxterms. Doesn't make sense yet? Use Mortimer's approach to learn these materials, and you'll figure it out.

# TDD saves ~ 75% of the time

I'm not alone in saying this. Don Wells [writes](http://www.extremeprogramming.org/don.html) about the Chrysler Comprehensive Compensation program's switch to Extreme Programming (i.e. using a lot of unit tests): "Don has some rough estimate  and believes the team was going six and a half times faster than before [using unit tests]". He writes about his second experience with Ford's Vehicle Cost and Profitability System (VCAPS), "As production bugs dropped even further the team became more productive eventually reaching a factor of ten times". So I don't think it's extreme (well it is an extreme programming practice) to say that when I started TDD about 120 problems in, at more granular levels than what the 4Clojure test cases offered, that I reduced my time-to-solution to 25% of what it was, saving 75% of my time and energy.

# Clojure secrets

Yes, I've learned a LOT about the Clojure language, hidden functions (`letfn`, `reify`) and features (macros, `loop-recur`, `reduce`), and neat surprises (`peek`, `rseq`, `droplast` are hundreds of times faster operations for vectors than `last`, `reverse`, and `butlast`), and gotchas (a vector is not a seq! Use `coll?` or `vector?` to detect it). My competence with recursion--and the assisting recursive function `reduce`--has shot through the roof. I've improved my ability to manipulate data (`assoc`, `assoc-in`, `update`, `update-in`, `dissoc`) and get data (`nth`, `get`, `get-in`). I suppose these are simple, but it's nice when it becomes second nature to wield data around--and to know that these don't apply to lists (exc. `nth`) but vectors and maps. I've learned about the ways of Clojure's strange implementation of `for` (it could easily be called `cartesian-product`). I've picked up many goodies like `partition-by`, `interleave`, `interpose`, `repeat`. 

# TDD's the big aha

I expected to take a month for these problems, but it took two and a half months. I spent so much time spinning my wheels, trying to cram four functions into one. When there was a tiny chance of it working, I'd run it through the test case hoping it'd work, and feeling dejected when it didn't. Knowing TDD would've prevented that, and I would've solved it faster with more certainty. If I had to wipe my brain of all Clojure knowledge and restart this challenge, but with a TDD approach, I think that I could do it in a month. 

![proof](https://dl.dropboxusercontent.com/s/ijm07l26dar3q1i/msedge_2021-03-19_23-11-52.png)