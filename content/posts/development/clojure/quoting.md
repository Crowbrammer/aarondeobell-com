---
title: "How I see Clojure quoting"
date: 2021-04-13T16:49:47-04:00
author: "Aaron Bell"
draft: false
---

[`quote`](https://clojuredocs.org/clojure.core/quote)-ing leaves most things alone except for what could refer to something else. 

#

#

```
(type (quote ()))                                ;;=> PersistentList$EmptyList
(type (quote []))                                ;;=> PersistentVector
(type (quote {}))                                ;;=> PersistentArrayMap
(type (quote #{}))                              ;;=> PersistentHashSet
(type (quote max))                            ;;=> Symbol
(type (quote 1))                                 ;;=> Long
(type (quote :a))                                ;;=> Keyword
(type (quote nil))                               ;;=> nil
(map type (quote (1 2 3)))                ;;=> (Long Long Long)
(map type (quote (max min whoa)));;=> (Symbol Symbol Symbol)
```
#

#

Ticks called quote [reader macros](https://clojure.org/reference/reader#macrochars) allow you to write concisely:

```
(type '())                                ;;=> PersistentList$EmptyList
(type '[])                                ;;=> PersistentVector
...
(map type '(max min whoa));;=> (Symbol Symbol Symbol)
```

# 

Quoting lets you build lists for [the Clojure evaluator](https://clojure.org/reference/evaluation). Building lists for the evaluator is the whole idea behind [the macro](https://clojure.org/reference/macros) (also see [this](https://www.braveclojure.com/read-and-eval/) and [this](https://clojure.org/reference/macros)), except that with quotes you can turn the form into a listable thing anywhere, not just via the macro's arguments.