---
title: "Unquote splicing (`~@`) evaluates before stripping parentheses"
date: 2021-11-05T19:50:48-04:00
author: "Aaron Bell"
draft: false
---

To understand macros, I needed to understand [unquote splicing](https://www.braveclojure.com/writing-macros/#Refactoring_a_Macro_and_Unquote_Splicing). I used to think it peeled the parentheses off, but no:  

```
(= `(+ ~@(map #(+ 2 %) [1 2 3 4])) '(clojure.core/+ map #(+ 2 % ) [1 2 3 4]))`. 
;;=> false
```

Evident with the `~` unquote symbol, it lets the onion sprout (evaluate) first before peeling its layers 😄:

```
(= `(+ ~@(map #(+ 2 %) [1 2 3 4])) '(clojure.core/+ 3 4 5 6))
;;=> true
```
