---
title: "Faster grokking of emacs commands with gifs"
date: 2021-01-06T19:09:20-05:00
author: "Aaron Bell"
draft: false
---



Here's what I learned from Daniel Higginbotham (2015) from [Ch.2 of Clojure for the Brave and True](https://www.braveclojure.com/basic-emacs/).

Table of Contents:


## Prereqs

 - AppData/Roaming/.emacs.d, not ~/.emacs.d when adding Higginbotham's config files

![emacs directory](https://dl.dropboxusercontent.com/s/a16p2qgsbjyz1la/2021-01-06_13-59-44.gif)

 - Shortcut shorthand! 
 
C-x C-f differs from C-x f. C-x C-f (Open file) vs. C-x f (fill lines, whatever that means). To execute, hold control for both (C-[letter] C-[letter]) vs (C-[letter] [let go of control] [letter]). (Using On-Screen Keyboard to avoid installing a keylogger). 

![Two different commands](https://dl.dropboxusercontent.com/s/jvfbaj6w5wb2tcn/2021-01-06_14-09-27.gif)

 - M- means "alt".

## Basic

 - yank (pasting with C-y),
 - set and work with regions (including region-only regex, and regex-based region selection; all done using C-space),

![yank, use regions](https://dl.dropboxusercontent.com/s/c8b7jm4ij64u3ug/2021-01-06_14-13-22.gif)

 - use the kill ring (ah, the clipboard),
 - kill (kilobytes, by cutting with C-k, or copying with M-k or Alt-k),

![kill, cycle through kill ring](https://dl.dropboxusercontent.com/s/tixner7axstzuzr/2021-01-06_15-08-32.gif)

## Intermediate

 - Moving the cursor after yanking (Ctrl + y-ing or "C-y" for short) removes the ability to cycle through the kill ring

![Moving after yank prevents kill ring cycling](https://dl.dropboxusercontent.com/s/yhfyufh5vk0wsar/2021-01-06_15-12-14.gif)

 - I can split windows (C-x [some number between 0 and 3 inclusive, usually])

 ![Working with windows](https://dl.dropboxusercontent.com/s/uw2xh00r07nn6yj/2021-01-06_15-15-41.gif)

 - M-/ (Bash-tab-like text-expansion).

![hippie expand](https://dl.dropboxusercontent.com/s/by4y7tzvyvouvml/2021-01-06_15-17-45.gif)

 - Clojure commands:
   - start CIDER (`C-x "cider-jack-in" enter` at your Clojure code)
![cider-jack-in command](https://dl.dropboxusercontent.com/s/gj10jowexk7xt19/2021-01-06_16-15-40.gif)

   - `C-c C-d C-d` (help with a symbol, like println or defn. Press q to leave.)

![help for symbol](https://dl.dropboxusercontent.com/s/wrc0nxs415i0poa/2021-01-06_16-24-17.gif)
   - `C-x C-e` (execute the command directly left of the pointer),

![execute command to pointer's left](https://dl.dropboxusercontent.com/s/3t8zcqoqfwn3vi9/2021-01-06_16-26-58.gif)

   - `C-u C-x C-e` (print expression output into the script)

![print that shit right in the script](https://dl.dropboxusercontent.com/s/xiws4oqz88xqwp5/2021-01-06_16-32-51.gif)

   - `C-c C-k y` (compile)

![compile your amazing Clojure code](https://dl.dropboxusercontent.com/s/q2dogt1ss7k914c/2021-01-06_17-15-36.gif)

   - `C-c C-d C-a [text] enter` (apropos search; like C-c C-d C-d but for something not under your pointer. Press q to leave.)

![search docs for something?](https://dl.dropboxusercontent.com/s/5470b5823hxz23t/2021-01-06_17-25-06.gif)

   - `C-c M-n M-n` (set the REPL to the current namespace),

![set the REPL's namespace to your pointer's namespace](https://dl.dropboxusercontent.com/s/60arsyow1v1m3uj/2021-01-06_19-20-07.gif)

   - use `paredit-mode` (`M-x "paredite-mode"`; here's [a high quality post on Paredit](http://danmidwood.com/content/2014/11/21/animated-paredit.html))

![Working with paredit mode](https://dl.dropboxusercontent.com/s/m1hi7xsjng80f9o/2021-01-06_18-29-31.gif)

      - Point to the very left of an object (have the rectangle on the left-most character) before alt-shift-9-ing it (`M-(` ) 

![enclose](https://dl.dropboxusercontent.com/s/m1hi7xsjng80f9o/2021-01-06_18-29-31.gif)

      - `C- ->` to slurp

![forward slurp](https://dl.dropboxusercontent.com/s/92grgmcbsleijjy/2021-01-06_18-33-11.gif)

      - `C-M- <-` to slurp backwards

![backward slurp](https://dl.dropboxusercontent.com/s/hqrwuug5dship76/2021-01-06_19-04-40.gif)

      - `C- <-` to "barf" (yuck)

![forward barf (yuck)](https://dl.dropboxusercontent.com/s/u059spemooaf4zb/2021-01-06_19-05-31.gif)

      - `C-M- ->` to backward barf

![backward barf](https://dl.dropboxusercontent.com/s/9u3h1x1b3uf3zu9/2021-01-06_19-07-25.gif)      

## Advanced

I know the authors based emacs on elisp, but I don't have practical knowledge of elisp yet.

Thank you, Mr. Higginbotham, for the knowledge. 