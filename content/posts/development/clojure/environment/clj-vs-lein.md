---
title: "Clj vs. Lein"
date: 2021-08-12T05:22:33-04:00
author: "Aaron Bell"
draft: false
---

The [`clj` tool](https://clojure.org/guides/deps_and_cli) evaluates expressions; [Leiningen](https://clojure.org/guides/deps_and_cli) (`lein`) does not. Lein enables easy jar building with `lein [uber]jar`; `clj` does not. Lein offers easier builds at the price of a heavier weight build system. Clj offers more control and the ability to run ad hoc expressions. To use `clj` to build jar files, you use the tools.build library in some Clojure code. To evaluate expressions from the command line using Lein... you can't. 