---
title: "\"Getting\" macros"
date: 2021-09-20T14:25:34-04:00
author: "Aaron Bell"
draft: false
---

Macros let you leverage the understanding of the reader and evaluator. I perceive Clojure's process to be: 

"Text -> List of symbols -> Macro list of symbols -> Evaluation".

Macros take the symbols of arguments before they're evaluated so that you can change the content and order of the list before they're evaluated.

Define macros with `defmacro` and pass in the forms.  The arguments given are parts of the list before evaluated.

```
(defmacro mymacro [form]
  (println (type form)) ;; clojure.lang.PersistentList
  form)
  
(type (mymacro (str 1 2))) ;; java.lang.String
```

Bibliography:

Higginbotham, Daniel. (2015?). Clojure for the brave and true. Chapter 7: Clojure alchemy: reading, evaluation, and macros. https://www.braveclojure.com/read-and-eval/