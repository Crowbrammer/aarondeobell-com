---
title: "Clojure namespaces"
date: 2021-08-02T19:50:59-04:00
author: "Aaron Bell"
draft: false
---

In Clojure, you have boxes of code called namespaces, and there are ways of making Clojure communicate between the boxes. There are at least two boxes: clojure.core and your-project.core. If you have external libraries, more boxes will be added to the project. To talk between these boxes, use fully qualified symbols (`your-project.other-namespace/func-name`) or use `refer` to enable just using `func-name`. Now everywhere "boxes" are mentioned replace it with "namespaces". 

Biblio:

Higginbotham, Daniel. (2015(?)). Clojure for the brave and true, chapter 6: organizing your project: a librarian’s tale. https://www.braveclojure.com/organization/