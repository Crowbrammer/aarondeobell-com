---
title: "How I see Clojure's reader, evaluator, and macros"
date: 2021-04-13T14:39:09-04:00
author: "Aaron Bell"
draft: false
---

Clojure is [a "lisp"](http://www.paulgraham.com/lisp.html), a list processor, and it uses [a reader](https://clojure.org/reference/reader) and [an evaluator](https://clojure.org/reference/evaluation) to implement the list processing. More precisely, the evaluator of Clojure is the list processor, and everything else, primarily the reader, support that. To build the lists for the evaluator to process, the reader takes your text and builds lists out of them. The reader parses your text for S-expressions (the parentheses and the various symbols inside them) and then passes it into the evaluator which makes computations from this. A translator called [a macro](https://clojure.org/reference/macros) can intercept lists just before the evaluator, do something with it, then hand it to the evaluator. This is like lifting someone up to catch a basketball just before it goes into the net. 

**Bibliography**

Clojure.org. ([N.d.]). [Evaluation](https://clojure.org/reference/evaluation); [The Reader](https://clojure.org/reference/reader); [Macros](https://clojure.org/reference/macros)

Higginbotham, Daniel. (2015). [Clojure for the brave and true](https://www.amazon.com/Clojure-Brave-True-Ultimate-Programmer/dp/1593275919), chapters [seven](https://www.braveclojure.com/read-and-eval/) and [eight](https://www.braveclojure.com/read-and-eval/). 