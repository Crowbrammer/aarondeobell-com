---
title: "Clojure testing with multiple test files"
date: 2021-03-25T16:08:06-04:00
author: "Aaron Bell"
draft: false
---

Like Java, and probably because it uses Java, Clojure/clojure.test/`lein test` appear to use naming requirements to run tests. I wanted to use multiple files to store and run tests from:

```
.../tests/core_test.clj
.../tests/ch5_test.clj
```

Simply adding a file in the tests directory didn't allow that. I researched this, and I learned pretty much everything but what to do here. I learned that I could

 - nest `testing`; 
 - require clojure.test and then wrap functions at creation using `with-test` and test them later using `test` 
 - use fixtures for build-up/tear-down. 

Exciting! At some point while looking at all this, I thought of `use`, and how that would bring the test forms from another namespace. `use`-ing tests from the other files/namespaces probably isn't the "right" way, but until I learn what to do, this is what I'm going to do. 