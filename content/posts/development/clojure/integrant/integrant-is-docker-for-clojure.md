---
title: "Integrant is like Docker for Clojure"
date: 2022-05-20T11:49:27-04:00
author: "Aaron Bell"
draft: false
---

[Integrant](https://github.com/weavejester/integrant)'s produces decoupled, well-contained parts within the app to prevent one piece of the app from tanking the whole thread--speeding up development time when you need to change one part. Integrant is like a mini Docker for your Clojure program--it containerizes things (database connection, http server) and isolates them to make their core functionality available to other parts of the program while protecting the program from any faults they may have. To use this, you define the parts/containers with key values in a configuration map. You decide what happens in the container at each process with `prep-key`, `init-key`, and `halt-key!` multimethods--or functions that run conditionally if the "dispatching value" matches (Integrant just uses the key of the map as the value to decide if your multimethod should run). Once you've made all these decisions, you then optionally `prep` and then mandatorily `init`--and you'll have all your stuff. Now how do you access this stuff.