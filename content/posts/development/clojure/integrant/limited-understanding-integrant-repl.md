---
title: "Stuart Sierra inspired the Integrant REPL library"
date: 2022-05-27T07:10:30-04:00
author: "Aaron Bell"
draft: false
---

A Clojure developer named Stuart Sierra developed and taught [a Clojure workflow](https://cognitect.com/blog/2013/06/04/clojure-workflow-reloaded) that inspired James Reeves to build [a library that implements it using Integrant](https://github.com/weavejester/integrant-repl): the Integrant REPL library. In my limited understanding, I see the workflow uses a global state with starting and stopping functions to set the state and clear the state. With Integrant REPL, it does this but with Integrant.