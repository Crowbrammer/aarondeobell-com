---
title: "Working example of Clojure's new `iteration` function"
date: 2022-03-30T12:19:41-04:00
author: "Aaron Bell"
draft: false
---

My example:

```clojure
(def example-data {:a {:next-token :b
                       :val  "A"}
                   :b {:next-token :c
                       :val  "B"}
                   :c {:next-token :d
                       :val "C"}
                   :d {:val "D"}}) ; No token, so `iteration` will end.

(vec (iteration
       (fn [token]
         (get example-data token))
       :initk :a
       :kf :next-token
       :vf :val))
```

I find this simpler and self-evident in how it's used vs. [the AWS example](https://www.juxt.pro/blog/new-clojure-iteration) and [the `.readLine` test example](https://github.com/clojure/clojure/blob/e45e47882597359aa2adce9f244ecdba730e6c76/test/clojure/test_clojure/sequences.clj#L1428) given so far. Even though there's other examples in the tests for `iteration`, the `.readLine` example is the simplest. 