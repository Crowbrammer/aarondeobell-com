---
title: "The Clojure community needs clearer and more documentation (Deps reference, JavaScript interop)"
date: 2022-05-27T16:09:06-04:00
author: "Aaron Bell"
draft: true
---

I love the Clojure group, so I fear offending them with this, but I need to get more information to the Clojure community. There are many difficult tasks that don't need to be difficult simply from a dirth of or weirdness of communication. 

My first example is when using the Clojure CLI tools, there are two documents with the exact same (anchor link) name "Deps and CLI" that differs only when you get to the actual page. These two pages, the Reference and the Guide, contain different essential information. 

My second example is JavaScript interop with ClojureScript. Unlike how Clojure has an official article on Java interop, on the ClojureScript official site, I don't see JavaScript interop explicity taught and discussed. This is a core feature of the language, and it is not taught. I had to learn about dot syntax, double-dot syntax, dot-dash property accessing, when to use `js/window`, `js/document`, or that I could do `js/localStorage` all from other sources, [like this one](https://lwhorton.github.io/2018/10/20/clojurescript-interop-with-javascript.html), besides the official source. 

What if we could get all the info we need from the official source?