---
title: "How I understand Clojure's `apply`"
date: 2021-04-06T22:42:53-04:00
author: "Aaron Bell"
draft: false
---

I wrote a big ole' post on [`apply`](https://clojuredocs.org/clojure.core/apply), but to save you time I'll shrink it:

To start:

 - Clojure functions work with [the `AFn` class](https://github.com/clojure/clojure/blob/b1b88dd25373a86e41310a525a21b497799dbbf2/src/jvm/clojure/lang/AFn.java)...
 - which implements [the `IFn` interface](https://clojure.github.io/clojure/javadoc/clojure/lang/IFn.html)...
 - which specificies twenty-two overloads of  `invoke`
 - and a method called `applyTo`

Then:

 - `applyTo` takes a sequence
 - and passes it to `applyToHelper` 
 - this counts the sequence and 
 - it passes it into a twenty-two branch switch statement 
 - which decides how much `ISeq.first` and `ISeq.next`ing to do
 - to pull out every argument into an `invoke`

[`apply` uses `applyTo`](https://github.com/clojure/clojure/blob/clojure-1.10.1/src/clj/clojure/core.clj#L660) with Java interop. 

`(f '(1 2 3))` will run the single-arity `f.invoke('(1 2 3))`.  `(apply f '(1 2 3))` will run the triple-arity `f.invoke(1 2 3)` which is useful for things like `max`, `min`, `distinct?`.



```
(max [1 2 3])               ;;=> [1 2 3]
(apply max [1 2 3])         ;;=> 3
(apply max 4 [1 2 3])       ;;=> 4  a.k.a. the "chips"
(min [1 2 3])               ;;=> [1 2 3]
(apply min [1 2 3])         ;;=> 1
(apply min 0 [1 2 3])       ;;=> 0
(distinct? [1 1 3])         ;;=> true
(apply distinct? [1 1 3])   ;;=> false
(apply distinct? 1 [1 2 3]) ;;=> false
```

That's it. 🙂