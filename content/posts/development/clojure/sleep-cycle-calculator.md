---
title: "I used java.time.LocalTime with Clojure to make a \"sleep cycle wake time\" calculator"
date: 2022-05-10T00:56:06-04:00
author: "Aaron Bell"
draft: false
---

The rule for a sleep session length in minutes is fifteen minutes plus the sleep cycle length (understood to be ninety minutes) x the desired number of sleep cycles, or T = 15 + 90*n. I often used a [bloated, dissatisfying, too many steps site](https://online-calculator.org/time-from-now-calculator.aspx) to get a simple list of when I'd wake up given a number of sleep cycles. I initially wanted another website for this, but the info's so simple that a simple Clojure expression'd suffice. I looped on a range from 1 to 6, plugging the index into the equation to get the number of minutes required for each sleep session length. Then, relearning the Java time libraries and how to perform Java interop in Clojure, I built a list of times I'd wake up, given the number of sleep cycles I took. 

```clojure
user=> (doseq [x (rest (range 6))] (println x "sleep cycles:" (.toString (.plusMinutes (java.time.LocalTime/now) (+ 15 (* 90 x))))))
1 sleep cycles: 02:23:43.707375100
2 sleep cycles: 03:53:43.707375100
3 sleep cycles: 05:23:43.707375100
4 sleep cycles: 06:53:43.708374300
5 sleep cycles: 08:23:43.708374300
```
TODO: Thread that expression. 