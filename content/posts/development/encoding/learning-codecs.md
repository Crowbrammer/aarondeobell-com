---
title: "Use HEVC (a.k.a. \"MPEG-H Part-2\") over MPEG-4 to save 25-50% the space for the same quality."
date: 2022-03-29T09:23:46-04:00
author: "Aaron Bell"
draft: false
---

One of the best codecs is the [HEVC](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding#Joint_Collaborative_Team_on_Video_Coding) a.k.a. H.265 a.k.a. MPEG-H Part-2, the successor to [MPEG-4](https://en.wikipedia.org/wiki/MPEG-4) Part 10. It offers 25-50% better compression/less space used with the same quality as MPEG-4. But features and selling points aside, I need some technical know-how. I want to know how they work so that I can pull them to my site or server (usually from YT or something), or make them available from a server. In nearly every case the video'll be distributed over HTTP. I could learn the tools to do so, and hopefully the tools'll be in [Clojure](http://clojure.org/).