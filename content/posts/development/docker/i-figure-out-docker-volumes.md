---
title: "I figured out Docker volumes"
date: 2021-06-09T01:14:18-04:00
author: "Aaron Bell"
draft: false
---

I handled a bottleneck that preventing me from using Docker as a portable Clojure dev environment: Persisting data between the creation and destruction of Docker containers. The (probably obvious) key is to use volumes. The existence of both volumes and bind-mounts confused and slowed progress to this insight at first, but Michal (accent needed) showed me how volumes'd work for my purpose. 

Knowing this, I tried to create an Ubuntu container with volumes, but got errors. I'd soon learn that you need to use absolute paths when specifying volumes (afaict). I didn't know either the host nor the container file system that well, but I explored and got a container w/ a volume going. I'd later learn the $PWD variable which essentially lets me use relative paths where absolute paths are needed by prepending whatever local directories I type with the present working directory ("$PWD/my-dir:/root/my-dir"), but for the moment I typed a hard path. 

With the knowledge that volumes'll work and that I need absolute paths to map host directories to the container directories,  I: 1) ran a container with a volume, 2) edited some data, 3) stopped the container, 4) examined the data from the host, and 5) felt a rush of light and lightness, an elation, as I saw the container edit persisted on the host. 