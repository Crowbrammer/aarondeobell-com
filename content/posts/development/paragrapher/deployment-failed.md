---
title: "I haven't gotten Netlify to properly deploy the Paragrapher yet"
date: 2021-08-17T05:27:13-04:00
author: "Aaron Bell"
draft: false
---

My deployment for the Paragrapher failed. First it said, "Cannot convert ECMASCRIPT_2018 feature "RegExp Lookbehind". I checked for an option to target later ECMAScript releases. I set :language-in and :language-out to :es-next each. I don't understand these options, so it's not surprising this failed. I then tried turning :optimizations from :advanced to :none. It deployed, the basic html showed, but the js of the app didn't load. I turned it to :simple and pushed it. Did it work? Let me see: No. It gave me an error though: "(index):77 ClojureScript could not load :main, did you forget to specify :asset-path?"

(I used the Paragrapher to make this. Here's what it looked like:)

![Paragrapher used for this post](https://dl.dropboxusercontent.com/s/8ku0jzfe2ovhh86/2021-08-17_05-27-47.png)