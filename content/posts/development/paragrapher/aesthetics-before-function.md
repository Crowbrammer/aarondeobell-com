---
title: "I (rightly) chose aesthetics before function"
date: 2021-08-16T10:49:22-04:00
author: "Aaron Bell"
draft: false
---

I've improved the aesthetics of my "paragrapher" app before finishing the main course. The app shows the words remaining and disables completed parts onload. My next aesthetic is to have it resize the textarea as one types, and I'll have all this done before I enable it to save or publish what I've written. 

![current paragrapher app](https://dl.dropboxusercontent.com/s/iqmmfon37n7ldnm/2021-08-16_10-50-17.png)