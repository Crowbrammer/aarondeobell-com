---
title: "My version control workflow"
date: 2021-08-23T19:08:14-04:00
author: "Aaron Bell"
draft: false
---

I can make development easy by following the version control workflow for each feature. First glance at the release notes (i.e. to see what the current focus is), then start the following commands with `git `:

### Start

    checkout main
    status -s
    (if fork, at GH fetch and merge upstream) 
    pull origin main
    checkout [-b] my-branch-name-w-semver
    status -s

### During

    (Tests)
    (Code)

### Feature complete

    status -s
    add [name files here]
    commit -S -m '[message]'
    push
    (paste suggested command)

### After push
    (at GH)
    (pull request)

### After accepted PR
    checkout main
    branch -d [branch-name]