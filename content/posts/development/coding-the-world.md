---
title: "Coding the world"
date: 2021-05-21T05:48:37-04:00
author: "Aaron Bell"
draft: true
---

I'd like to understand the blockchain concepts behind the Dogecoin code, which I'd then use to understand the Dogecoin code itself. This shows me how much of code isn't actually the code itself but the ideas that the code encodes. When you code, you should ask, "What to encode?". This means you have some external, real-world thing or idea that you then encode into your program by fitting it into a data structure or function. While I have other things involving technical analysis that I need to understand, I know that I'll provide utility when I learn things that'll impact the world like learning to code. 