---
title: "I need to understand the MediaRecorder API to record and store audio in the browser"
date: 2021-06-27T19:04:02-04:00
author: "Aaron Bell"
draft: false
---

I require knowledge of [ArrayBuffers](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer) to build my [imagestreaming](http://www.winwenger.com/imstream.htm) app. I need to asynchronously collect a stream of data from the user's microphone and pass it to [the MediaRecorder API](https://developer.mozilla.org/en-US/docs/Web/API/MediaRecorder). Then as it records, it apparently passes chunks of bytes to the [blob](https://developer.mozilla.org/en-US/docs/Web/API/Blob). Once I signal it to stop, I can ostensibly do something with this. 