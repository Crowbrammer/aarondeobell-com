---
title: "GitHub couples releases with tags."
date: 2021-12-01T07:41:32-05:00
author: "Aaron Bell"
draft: false
---

I made a [GitHub Actions workflow](https://github.com/Crowbrammer/gha-test/blob/main/.github/workflows/build-and-release.yml) that builds and releases from a repo whenever I push a tag. The on-tag part came from [Sean Corfield's workflow](https://github.com/seancorfield/next-jdbc/blob/develop/.github/workflows/test-and-release.yml), which I modeled. [Patrick](https://twitter.com/naxels)'d prefer to avoid thinking of tags (As Elon Musk [says](https://www.youtube.com/watch?v=igTPC0yFJgg), "All input is error"), but GitHub appears to couple releases with tags. The current actions required for a release workflow are `git`:

 1. `add [files]`
 2. `commit -S -m "[message]"`
 3. `push`
 4. `tag v[tag number]`
 5. `git push origin tag`

3 pushes files, 5 pushes the tag which triggers the workflow. Keeping 1 manual, mayhaps I could combine 3-5 with a [Babashka](https://babashka.org/) script. Have the major, minor, patch version in an edn file, have Babashka slurp and read it, reset/increment some/all of the three numbers, make a string from the three, then make and push a tag from this string. Would reduce friction of manually incrementing seeing what version we're at and tagging by hand.