---
title: "Learning to use Docker with Clojure"
date: 2021-06-04T17:58:40-04:00
author: "Aaron Bell"
draft: false
---

Current development focus: Learn to use a [Docker](https://www.docker.com/)-based environment for my [Clojure](https://clojure.org/) apps. So far, I know that with Docker you: 

 1. Create a recipe 
 2. Build an "image" from this recipe 
 3. Both eat and have the cake by building a container from the image 
 4. Run the image. 

The Docker recipe(s)/image(s)/container(s) should include [JVM](https://en.wikipedia.org/wiki/Java_virtual_machine), [Leiningen](https://leiningen.org/), and Clojure. 