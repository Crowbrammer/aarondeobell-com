---
title: "I recorded my mic with ClojureScript"
date: 2021-07-02T13:03:16-04:00
author: "Aaron Bell"
draft: false
---

I learned how to work with the user's microphone in ClojureScript. I coded 90% of this code using JavaScript interop. I learned to use JS classes in ClojureScript (prepend "js/" and append "."). To record from the microphone, I needed to understand the Navigator, MediaRecorder, Blob, and URL classes in JavaScript. I created two functions for two handlers: `js/window`'s `onload` event to get the user media, and `js/MediaRecorder`'s  `ondateavailable`, so that I could work with the user's audio after I stopped recording. I then used the URL class to make a blob url for the audio. With all this done, I refreshed the page, recorded finger taps, visited the printed link and listened to what I recorded.
