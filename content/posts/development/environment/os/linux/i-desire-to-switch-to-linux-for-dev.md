---
title: "I desire a Linux system for web development"
date: 2021-09-24T16:05:45-04:00
author: "Aaron Bell"
draft: false
---

(Still pondering this. Likely wrong here.)

I'll have an easier time developing with Linux. On Windows, I'm switching between Bash for Windows and Powershell constantly. Tools like clj, Docker (try execcing into a shell), and Babashka (the repl eventually bugs out) simply don't work fully in Bash for Windows. Bash provides a cleaner experience, offering concise linux commands like `which` to find an installation path vs. Powershell's `(Get-Command cmd).Path`, but it gets second-class support on Windows. Further, on Linux, one can use a shell with simpler commands (which and sudo), have clearer path names (no escape symbols for delimiters), and gain easier access to packages with APT. The friction at each of these steps makes simple tasks take five to ten times longer than it does on Linux.