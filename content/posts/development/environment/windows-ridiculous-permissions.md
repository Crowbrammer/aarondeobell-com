---
title: "To modify Windows Registry keys, the administrator can't modify the keys nor its own permissions... but it can add a user and give full permissons... makes sense ha "
date: 2021-10-06T07:34:35-04:00
author: "Aaron Bell"
draft: false
---

For more streamlined development, I like Powershell to open up to the right directory, so a while back, I set up the context menu for Powershell. To learn to do this, I read [How-To Geek's article](https://www.howtogeek.com/165268/how-to-add-open-powershell-here-to-the-context-menu-in-windows/) and noticed an option to make the option show with I press "shift" with the right click. This involves adding an "Extended" value name (key of a key... Windows registry terms) with blank data. When I satisfied my curiosity and decided to remove it... error. No permissions. Try to add permissions for admin... error (note: I can't manually set the owner of the key back to TrustedInstaller, so I can't show the error atm :/). Google searches would reveal something absurd: an admin [can't modify its own permissions by default :(](https://dl.dropboxusercontent.com/s/9cekf3ilifctilz/2021-10-05_19-38-47.gif)... but can make myself the owner and add a new user (me) and permissions for that user including write access... In the words of [Dusty Slay, "We're having a good time"](https://youtu.be/Z6NUnl37FnA?t=151).