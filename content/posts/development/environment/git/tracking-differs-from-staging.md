---
title: "Git tracking differs from staging"
date: 2021-11-11T03:07:27-05:00
author: "Aaron Bell"
draft: false
---

I learned some Git principles and judo:
 - tracking differs from staging: 
    - i.e.`git commit -a` stages and commits already-tracked files, and [new files won't be committed](https://stackoverflow.com/a/13339514/2596132); 
    - git add . tracks and stages.
    - [by definition](https://stackoverflow.com/a/15653161/2596132), files are tracked _only_ when they are first staged/committed. There is no "track but not stage" for new files
 - `git diff` is "staged - working directory". git diff --staged is "commit - staged".
 - `git status -s` gives a concise description of what's untracked, modified, and whether the modification is staged or not. 
 - the "/[name]" ignores things in the current directory.
 - "[name]" ignores things of a given name in all directories... more powerful than expected
