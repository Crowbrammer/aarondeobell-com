---
title: "Files modified after staging need to be staged again to commit the latest changes in Git"
date: 2021-11-10T04:50:47-05:00
author: "Aaron Bell"
draft: false
---

A mystery of missing GitLab repo files motivated me to read [the Pro Git book](https://git-scm.com/book/en/v2). I relearned the different VCS-es and models that back them: local, centralized, distributed; patch/delta vs. snapshot. I revisited the three areas a file can be in: working area, staging area, and the git repo. Per [Chapter 2](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository), I saw how a file can be both modified in the staging area and the working area at once. Git's _snapshot_ of the file goes in the staging area, not the file itself. So if you modify it in the working directory after staging, only the older modification is ready for commit; the later one will need to be added again to be committed. 