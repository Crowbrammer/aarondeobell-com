---
title: "IntelliJ should change the project contents on file change, not on editor focus"
date: 2022-03-31T17:05:48-04:00
author: "Aaron Bell"
draft: false
---

I've had fun working with IntelliJ... and much not fun. While it boasts excellent Clojure namespace management (avoiding lots of conflicts and bad requires), superior test running (click the green icon next to test itself), better REPL management, it has a few (minor) painful downsides. The project files don't reload on file change but when you focus the editor. Contrast this to VS Code, where if I move a file in Explorer, it reflects immediately in VS Code, edited or not. 

![IntelliJ shows file changes on focus... not on change :/](https://dl.dropboxusercontent.com/s/24o63otossjstto/Code_2022-03-31_17-10-15.gif)

Then there's drag and drop--I can drag folders into other folders all day, but I'm unable to do this simple task in IntelliJ. IntelliJ gets a lot of long-tail use cases right, but in the day-to-day tasks, VS Code wins. The long-tail cases are important though, so I'll likely stick with IntelliJ. 