---
title: "Atom has a poor user experience for keybindings, unlike VS Code"
date: 2021-10-01T12:47:02-04:00
author: "Aaron Bell"
draft: false
---

[Atom](https://atom.io/) doesn't let you disable just one keymap from a package. Not from the settings at least. It has you either keep all or reject all without further keymap manipulation. lisp-paredit strict mode near-gave me an aneurysm when it hijacked my main commands like C-w for closing the window--and Atom didn't let me easily remove it without bombing the rest of the keybindings.

![Atom's keybinding hell.](https://dl.dropboxusercontent.com/s/kmps3nybt2o5hwp/2021-10-01_12-48-36.gif)

Contrast this to [VS Code](https://code.visualstudio.com/) which says fuck that and lets you decide what each function's keybinding should be. You go to a list of keybindings and double click the function--in-built or installed package--and you get a prompt to set it there, with the keybinding itself. You can also right click and delete the binding entirely--something you'd need to "unset!" for in Atom. Nicee.

![VS Code's keybinding experience rocks](https://dl.dropboxusercontent.com/s/ja0e4jitiv3jy1w/2021-10-01_12-50-13.gif)

I'm still using Atom tho. 