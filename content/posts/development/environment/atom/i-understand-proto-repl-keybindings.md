---
title: "Proto REPL with Atom on Windows: Press Ctrl + Alt + comma... let go, and then press y to connect to the nREPL"
date: 2021-10-01T12:39:21-04:00
author: "Aaron Bell"
draft: false
---

[Proto REPL](https://github.com/jasongilman/proto-repl) offers [weird keymaps](https://github.com/jasongilman/proto-repl/blob/master/keymaps/proto-repl.cson) for connecting to the "network REPL" (nREPL): `C-M-,y` I didn't know the comma was part of it, so I pressed C-M-y (Ctrl + Alt + y at the same time), no result. I pressed C-M, let go, and then pressed y. It just typed "y" with no result. I tried C-M-,-y (all at the same time). I felt something was broken until I accidentally let go after pressing the comma and pressed y. 

![Successfully bring the nREPL up with keybindings](https://dl.dropboxusercontent.com/s/idh8lu028vaz5qe/2021-10-01_12-43-02.gif)

The form to decide the port for the repl came up 😁 The satisfaction of that prompt justified the pain to that point ha. 