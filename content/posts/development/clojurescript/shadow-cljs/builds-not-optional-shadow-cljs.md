---
title: "Builds are not optional in the shadow-cljs.edn (for testing)"
date: 2022-04-18T01:28:14-04:00
author: "Aaron Bell"
draft: false
---

shadow-cljs uses a namespace-like config structure similar to deps.edn, relying heavily on "builds" ([with parity to "aliases" in deps.edn](../shadow-cljs-complicated-config/)). There are few root-level, non-build config options in shadow-cljs. Most options are in "builds". Absent a "build", shadow-cljs offers no way to specify a "target" (shadow-cljs can help you write ClojureScript for Node.js, the browser, or Karma--which do you want?). You also need it to specify directories for tests. To run tests with ClojureScript you need to output it to JS and then run the JS. There's no default test directory and no root-level option for test paths for the output JS, so without a build, you can't say where it should go. 