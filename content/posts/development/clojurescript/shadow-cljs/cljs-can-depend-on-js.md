---
title: "ClojureScript libraries can depend on JS libraries"
date: 2022-04-09T12:36:35-04:00
author: "Aaron Bell"
draft: false
---

shadow-cljs lets you depend on ClojureScript (cljs) libraries in shadow-cljs.edn and JS libraries alike in package.json. Some cljs libraries can depend on JS libraries, like how Reagent requires React--meaning you'll need to `npm i` them before using your cljs library. 