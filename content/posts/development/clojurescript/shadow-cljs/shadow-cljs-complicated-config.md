---
title: "You need to go five-levels deep in the shadow-cljs config to specify what function should run at start with shadow-cljs"
date: 2022-04-09T12:38:04-04:00
author: "Aaron Bell"
draft: false
---

(Preface: I like shadow-cljs!)

shadow-cljs's config requires :source-paths, :dependencies, and :build values. The :build value should be a hashmap where you'll specify what should be run and how. The config is more complicated than necessary because it specifies the environment similarly to deps.edn but uses different names for the same things:

 - :deps -> :dependencies
 - :paths -> :source-paths
 - :aliases -> :builds

Also just to say "run this function on load" you need to go five levels in: :builds :<build-name> :modules :main :init-fn. Can a make a fork of shadow-cljs that simplifies this? 