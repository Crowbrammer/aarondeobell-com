---
title: "bidi means \"bidirectional\" (routing)"
date: 2021-12-01T00:45:09-05:00
author: "Aaron Bell"
draft: false
---

[Dave](https://github.com/DaveWM) in [Lobster Writer](https://github.com/DaveWM/lobster-writer) uses [bidi](https://github.com/juxt/bidi) to: 1) Decide what should be done for a given uri?, and 2) Given a thing that should be done, what should the uri be? Bidi's name means "_bidi_-rectional", which means the string uri can give a handler, and a handler can give a string. Both cases use the [pushy](https://github.com/kibu-australia/pushy) library. When the uri matches a handler in the route, pushy runs a given function that generally loads a matching page. When an event decides to change pages, the event gives a keyword to bidi, which gives a string, which pushy uses to update the browser's uri. 