---
title: "I used the JS Fetch API with audio data using Clojure(Script)"
date: 2021-08-09T19:46:12-04:00
author: "Aaron Bell"
draft: false
---

I learned to use the [JS Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#uploading_a_file) with audio data using [ClojureScript](http://clojurescript.org/). To send an audio file to the client, I learned the JS Fetch and [FormData API](https://developer.mozilla.org/en-US/docs/Web/API/FormData) (I'm unsure how else to send the audio data). The creators made Fetch [promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)-based, so this means understanding JS promises and the ClojureScript interop for it. I also learned about http headers. The fetch failed until I made the server (not the client) send an ["Access-Control-Allow-Origin" header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin) set to "localhost:3450" or "*". Fetch failed with other values like "no-cors" and "null". 