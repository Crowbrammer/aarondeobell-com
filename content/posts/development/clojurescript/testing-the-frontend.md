---
title: "I can use Figwheel (maybe), `doo`, JS interop, or raw JS to test against the dom"
date: 2021-09-16T08:07:55-04:00
author: "Aaron Bell"
draft: false
---

It's easier to test the front-end with JavaScript than with ClojureScript at the moment. JS has many tools (Puppeteer, Selenium, PhantomJS) to let me spawn a browser, a page from a browser, interact with the page, and then test for what's on the page based on my interaction. 

I have four leads right now for testing functions on the dom: [Figwheel](https://figwheel.org/docs/testing.html), [doo](https://github.com/bensu/doo), the use of JS libraries via JS interop, and raw JS testing against the ClJS-compiled JS.

I use Figwheel to write front-end code, and I see that it has testing documentation. I hope Figwheel handles the problem of testing the dom, else why would it offer testing when cljs.test exists? 