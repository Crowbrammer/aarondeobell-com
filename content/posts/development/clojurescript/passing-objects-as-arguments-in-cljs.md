---
title: "Having trouble passing JS objects as arguments in ClojureScript/JS interop"
date: 2021-06-16T21:14:37-04:00
author: "Aaron Bell"
draft: false
---

Despite me passing in `(js-obj "audio" true)` or other variants into `getUserMedia`, i.e. `(.. js/window -navigator -mediaDevices getUserMedia  (js-boj {"audio" true}))`, it complains about not having a video or audio constraint: "Failed to execute 'getUserMedia' on 'MediaDevices': At least one of audio and video must be requested". What method of passing in the object would work here?
