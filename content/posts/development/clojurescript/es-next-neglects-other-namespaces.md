---
title: "To make your ClojureScript app work, use underscores not dashes with your files"
date: 2021-08-19T06:34:07-04:00
author: "Aaron Bell"
draft: false
---

(OLD POST: [ClojureScript](http://clojurescript.org/), which uses [Google Closure](https://developers.google.com/closure), neglects non-core namespaces when I set :language-out to :es-next. For my pargraph splitter, I have an ECMAScript2018-only feature that lacks a polyfill: reverse lookbehind. My desire to know why outweighs my need to finish the product, so instead of neglecting the use of that, I've tried to figure out how to output ECMAScrpt2018+ JS with the reverse lookbehind feature. Google Closure defaultly transpiles code to ECMAScript3 \[source needed\], but you can change the output language to different ECMAScript (JavaScript) standards. An :ecmascript-2018 option doesn't exist. :es-next does, but [transpiling it with :es-next](https://dl.dropboxusercontent.com/s/w0w59rcvy0f1420/2021-08-19_06-36-08.png) makes it neglect outputting my modules. Why it does this, I don't know. Maybe I could do a webpack type thing to work around this. If I bunched it into a single file, would it work?)

Update: Google Closure/ClojureScript is fine. I had dashes in my file names. I replaced them with underscores, and the files transpiled (compiled?) correctly. 