---
title: "Reitit course starts with a Hello World app template, not the Cheffy app template."
date: 2021-12-08T07:43:09-05:00
author: "Aaron Bell"
draft: false
---

I handled three misconceptions with [Jacek's Reitit course](https://www.jacekschae.com/learn-reitit-pro): It'd use the [Re-Frame cheffy files](https://github.com/jacekschae/learn-re-frame-course-files) off the bat, showing a proper webpage on Heroku; that the Heroku server'd use the latest Leiningen; and that `heroku create` initialized the git repo. `lein new app cheffy` creates a hello world project from the "app" template. It doesn't pull cheffy as a template. Leiningen defaults to 1.7 which does not use https when pulling from Clojars or Maven causing checksum errors when the servers 501 (https required). Then I had issues with `heroku create` until I `git init`ed. 