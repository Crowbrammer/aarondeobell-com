---
title: "Clojure(Script) requires a certain file structure to build correctly"
date: 2021-08-08T09:12:00-04:00
author: "Aaron Bell"
draft: false
---

 The Clojure/ClojureScript compiler takes directions from the pom.xml file, usually auto-generated with Leingingen, to see where to start and build everything from. It then uses these directions to traverse your file structure and then compile the Java bytecode/JavaScript.