---
title: "Use `set!` and `-property` to modify DOM elements with ClojureScript"
date: 2021-06-16T18:54:41-04:00
author: "Aaron Bell"
draft: false
---

To modify a DOM element, select it and then use `set!`. To select it, try `(def h2 (.querySelector js/document "h2"))`. Then with your element, use `set!`, `..`, and `-` for `(set! (.. h2 -style -color) "red")`. Use this, and you'll see the first h2 element, if there is one, turn red.