---
title: "A Reagent component is a function that returns a \"V(oV)oKaHoC\""
date: 2021-09-23T23:38:11-04:00
author: "Aaron Bell"
draft: false
---

...A vector (of vectors) of keywords, mashmaps, or components. Keywords represent html tags. Hashmaps represent attributes for the html tags. Vars that refer to functions that return the V(oV)oKaHs are components that do these things one level deeper.

Simple:

```
(defn card []
  [:card "Hi"])
```

Less simple:
```
(defn card []
  [:card 
    [:img {:src "img/card-icon.svg" :alt "Tiny logo for the card"}]
    [:h2 "Card title"]
    [:p "Card text"]])
```

More less simple:
```
(defn card []
  [:card 
    [:img {:src "img/card-icon.svg" :alt "Tiny logo for the card"}]
    [card-title] ;; Prolly pass in state
    [card-body]]) 
```

Bibliography:

Schae, Jacek. [2021?]. Learn Reagent pro. 04 first components. https://www.jacekschae.com/courses/learn-reagent-pro/21651-components/57029-04-first-components