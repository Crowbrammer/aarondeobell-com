---
title: "I thought depending on other ClojureScript libraries differed from Clojure until I noticed my typo "
date: 2021-08-08T21:57:15-04:00
author: "Aaron Bell"
draft: false
---

[Leiningen](https://leiningen.org/) uses [Maven repos](https://maven.apache.org/guides/introduction/introduction-to-repositories.html), usually [Clojars](https://clojars.org/), to pull in dependencies. With the right repos specified, lein install updates the pom.xml file, which causes Clojure to pull the files from the web needed to complete the requirement. I thought [ClojureScript](http://clojurescript.org/) dependencies differed from [Clojure](https://clojure.org/), because I used `lambdaisland/fetch`, and it didn't fly. After much research I double-checked the namespace name in the GitHub repo, and I saw it was `lambdaisland.fetch`. Changing it to this, the [Figwheel](https://clojure.org/) compilation error disappeared, and my code worked again. 