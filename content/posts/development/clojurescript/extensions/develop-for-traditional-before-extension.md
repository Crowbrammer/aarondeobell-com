---
title: "Develop 80% of a Chrome extension in a traditional environment then convert it to a proper extension"
date: 2022-03-30T18:22:17-04:00
author: "Aaron Bell"
draft: false
---

I see a slow feedback loop with extension development.  Clojure apps have tests and direct execution of the script. ClojureScript offers both tests and fast visual feedback from the browser. Extensions made with ClojureScript, however, must be compiled, put into the browser extension folder; then I must reload the browser extension and run it. 

That said, I've found a faster way of developing Chrome extensions. Write the code as a normal js file first, using Node.js or a browser's dev tools for feedback; then when it's 80%+ done, code in the Chrome APIs to work as an extension. Working with Node.js or dev tools enables faster feedback than the code, copy and paste to the extension folder, reload the folder, run the extension workflow. 