---
title: "I learn to use Figwheel for Clojurescript using Leiningen"
date: 2021-06-16T18:49:09-04:00
author: "Aaron Bell"
draft: false
---

I learned to use [ClojureScript](http://clojurescript.org/). Setting up a project was way easier than I expected: `(lein new figwheel my-app)` ([Figwheel site](https://figwheel.org/)). Using Figwheel lets me make changes with the REPL so that I can see the changes immediately, and with lein, it's a breeze to set up and use. I haven't figured out how to use Figwheel with [Calva](https://calva.io/) yet, but it's better than the _nothing_ I was able to do before.