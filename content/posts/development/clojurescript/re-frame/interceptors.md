---
title: "Interceptors are hash-map modifiers"
date: 2021-11-30T06:47:05-05:00
author: "Aaron Bell"
draft: false
---

[Interceptors](https://day8.github.io/re-frame/Interceptors/) are hashmaps with an ":id, :before, :after" signature, where the :before and :after keys work with another hashmap of a ":coeffects, :effects, :queue, :stack" signature. Before the event fires, interceptors modify the context (hashmap) with functions mapped to the :before key. The event then fires with the modified context. The context then goes through the :after-bound functions, doing work after the main event has fired. This is useful for dealing with local storage with a Re-Frame db: pull the local storage before firing or push db state to local storage after firing. 