---
title: "To fix two shadow-cljs issues, simply restart it and wait :("
date: 2021-12-01T02:29:55-05:00
author: "Aaron Bell"
draft: false
---

I solved five problems from Jacek's _Learn Re-Frame Pro_ course: 
 - Babel transpilation error (solved by restarting shadow-cljs; not understood yet :())
 - Port already in use despite shutting previous instance down (wait, runs anyway, uses the same nrepl port as others that _are_ running)
 - Things not showing (mis-indent; instead of one fragment containing two elements, one fragrement contained one element, then the other element spilled over--due to ClojureScript returning only the last element, only this spilled element returned; a changed indent pulled it into the fragment due to my paredit extension)
 - Image not showing (I misunderstood the `reg-event-fx` signature; the second paramater is a vector, [effect ...params?]; I replaced `{:keys [img]}` with `[_ {:keys [img]}]`, and the image suddenly existed)
 - h2 under an h2 (looked at Jacek's working code and learned to make a conditional: if the thing's a string, wrap it in the h2 tag; if it's not, pass in a raw component; obvious, but as I typed out what Jacek showed, I missed this.)