---
title: "Use an interceptor to use registered coeffects; use a kv-pair to use registered effects"
date: 2021-11-30T17:17:06-05:00
author: "Aaron Bell"
draft: false
---

[Re-Frame](https://day8.github.io/re-frame/re-frame/) has two rules for working with registered [coffects](https://day8.github.io/re-frame/Coeffects/) (world input) and registered [effects](https://day8.github.io/re-frame/Effects/) (output to world): 1) Pull world input with `(inject-cofx :coeffect-name)` in an interceptor, and 2) Call registered effects by putting a `:effect-name :value` kv-pair in the returned effect map. To follow these rules, you need to know that effects can pull in a vector of [interceptors](https://day8.github.io/re-frame/Interceptors/). These interceptors generally change the hashmap called "coffects" before going to the effect. One type of interceptor is `(inject-cofx :coeffect-name)` which pulls a interceptor-signatured function from a global registry using `:coeffect-name`. This pulled function then modifies the coeffects hashmap. You also need to know that returning a hashmap with the key of a registered effect (e.g. `(reg-fx :effect-name)`) will pull and run the registered function with the single given value. Useful for navigation, screen animations, and local storage updating. 