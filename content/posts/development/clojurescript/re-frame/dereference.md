---
title: "Dereference your damn atoms ha"
date: 2021-12-02T07:17:47-05:00
author: "Aaron Bell"
draft: false
---

I couldn't add an image in my app. After many hours, I saw that I passed in an atom to a component without dereferencing it, which caused an image not to load. The `values` atom had the image. I looked in the from-group, the modal, and the button. I put print statements in the component and the subscription and event it used. The print statement in the subcription showed nil. The print in the `:upsert-image` event showed an atom with the image. (:img values) showed nil. Days later, it struck me that to let the event-fx's destructuring get the image, I needed to dereference the `values` atom before dispatching it to the `:upsert-image` event. One little `@` in the `save` function's argument made the event pass a proper hashmap instead of an atom which let me update the db with the appropriate image. 