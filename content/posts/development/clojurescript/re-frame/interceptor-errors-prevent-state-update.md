---
title: "Re-Frame interceptor errors prevent app state updates"
date: 2021-12-06T01:36:38-05:00
author: "Aaron Bell"
draft: false
---

One error in any interceptor prevents [the second Re-Frame domino](https://day8.github.io/re-frame/a-loop/) from falling. Either I wrote an error or [clojure.spec](https://clojure.org/guides/spec) changed since [Jacek](https://www.jacekschae.com/) recorded [his course](https://www.jacekschae.com/learn-re-frame-pro), but my spec saw the db and threw an error. My spec function was in an interceptor, and this interceptor error prevented Re-Frame from updating the app-db, causing nothing to load in the app. Any error in any interceptor (including after interceptors) of an event handler will prevent app state updates and further effects. (I spent a painful number of days on this. I expected  copying Jacek's code to work, but I messed something up or the ecosystem's changed.)

Interceptor:

```clojure
(def check-spec-interceptor (rf/after (partial check-and-throw ::db)))
```

Interceptor function:

```clojure
(defn check-and-throw
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check faild: " (s/explain-str a-spec db)) {}))))
```

Error-throwing spec:

```clojure
(s/def ::uid (s/nilable string?))
(s/def ::auth (s/map-of keyword? ::uid))
```

Ok spec:

```clojure
(s/def ::auth (s/nilable (s/map-of keyword? (s/nilable string?))))
```