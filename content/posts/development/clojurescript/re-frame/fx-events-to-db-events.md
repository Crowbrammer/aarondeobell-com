---
title: "When an event-fx dispatches to an event-db, pull the event-db's op to the original event-fx and change it to event-db"
date: 2021-12-04T11:40:46-05:00
author: "Aaron Bell"
draft: false
---

In [Re-Frame](https://day8.github.io/re-frame/re-frame/), I removed [effect events](https://day8.github.io/re-frame/re-frame/) for db events. Pure db events represent low cognitive-overhead functional programming: db in, db out. Side effect fx events necessarily complicate the program, but per [Jacek Schae](http://jacekschae.com/)'s instruction, I removed unnecessary cases: I.e. fx events that dispatched to db events. I changed these db-dispatching fx events to a db event and have it modify the db one more time in a db event instead of dispatching. This is apparently more pure and therefore better. 