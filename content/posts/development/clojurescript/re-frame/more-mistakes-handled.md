---
title: "Delete .shadow-cljs/build and rebuild to clear caching errors"
date: 2021-12-03T01:05:05-05:00
author: "Aaron Bell"
draft: false
---

I fucked up then fixed [Jacek](https://github.com/jacekschae)'s [Cheffy](https://github.com/jacekschae/learn-re-frame-course-files) app in four ways:

 - malindented (which causes rewrapping) for the
	 - db hashmap (putting key values outside the original map)
	 - page-nav (nesting content under it)
	 - messages (nesting them within an input container)
 - duplicated a file to model it for another page, but I didn't update
	 - the component name
	 - the modal name
	 - the file name 
 - typed `keys` instead of `key` for `^{:key id}`
 - malcached it somehow?

I unindented, renamed, and corrected the first three errors. After this, I still got one error: app.errors.events was required by app.core but not available. I copied Jacek's code over to ensure everything in the code was fine, and I still got the error. There's a caching element or way Re-Frame works that I don't understand here. It took the steam of a hot shower to "idea" me to delete the .shadow-cljs/build folder and restart. Doing that it, worked. 