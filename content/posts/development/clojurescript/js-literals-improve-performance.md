---
title: "JS literals (`#js`) improve runtime performance over `clj->js`"
date: 2021-11-02T09:52:23-04:00
author: "Aaron Bell"
draft: false
---

I was making a "rapid-scheduling app" that uses the [Google Calendar API](https://developers.google.com/calendar/api/guides/overview) , which required I convert Clojure maps into JS objects. 

This means the use of `#js` or `clj->js`

I saw [Paulus Esterhazy's post](https://gist.github.com/pesterhazy/82494dc329b403288abc2491272ee05c) which says I should do this:


```clojure
(def config #js {:apiKey "..."}
                             :clientId "[...].apps.googleusercontent.com"
                             :discoveryDocs #js ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
                             :scope "https://www.googleapis.com/auth/calendar.readonly"))

(.log js/console (.-discoveryDocs config)) ;; => ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest']
```
This turned my vector to a [GAPI](https://developers.google.com/identity/sign-in/web/reference)-friendly array. 

The #js reader macro (also called a js literal? ) converts only the top level into a js-obj or js-array. Nested things appear to be turned into objects. I first did this:

```clojure
(def config #js {:apiKey "..."}
                 :clientId "[...].apps.googleusercontent.com"
                 :discoveryDocs ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
                 :scope "https://www.googleapis.com/auth/calendar.readonly"})

(.log js/console (.-discoveryDocs config)) ;; => {meta: null, cnt: 1, shift: 5, root: {…}, tail: Array(1), …}
```

This caused the GAPI to not load the Calendar object. 

Albeit at the cost of run-time performance, `clj->js` converted the nested vector to a JS  array.

```clojure
(def config (clj->js {:apiKey "..."}
                 :clientId "[...].apps.googleusercontent.com"
                 :discoveryDocs ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
                 :scope "https://www.googleapis.com/auth/calendar.readonly"))

(.log js/console (.-discoveryDocs config)) ;; => ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest']
```

GAPI used this array to load the calendar, and I can see my events, etc. 

It's a function run at runtime to turn clj maps to js objects instead of #js's directly output JS array, so #js is preferred. 