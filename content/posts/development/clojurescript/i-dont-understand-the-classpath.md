---
title: "I don't understand the classpath?"
date: 2021-09-20T11:29:59-04:00
author: "Aaron Bell"
draft: false
---

I suspect my understanding of the "classpath" and Figwheel/ClojureScript's use of it is inadequate, else the wow.core namespace and all its interns (namespace-bound defined variables) would be there. I used clj -m figwheel.main expecting it to work with my namespace, but it didn't.  After calling `clj -m figwheel.main` from Powershell and then `in-ns` from the REPL into my wow.core namespace then calling each function in the REPL, Figwheel/ClojureScript responded saying that each function "undeclared". 