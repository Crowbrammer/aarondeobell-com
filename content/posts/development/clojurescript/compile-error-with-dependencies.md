---
title: "Figwheel doesn't like the `lein install` approach"
date: 2021-08-07T17:51:50-04:00
author: "Aaron Bell"
draft: false
---

[Figwheel](https://figwheel.org/) errored me out with the `lein install` approach. I want to send http requests from the client, so I chose to use the [JS Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API). [Lambda Island](https://lambdaisland.com/) made a [fetch ClojureScript wrapper](https://github.com/lambdaisland/fetch), so I (thought I) installed it, but on an attempted recompile, [it says](https://dl.dropboxusercontent.com/s/oiec6vhcncsbyrm/2021-08-07_17-53-54.png): "No such namespace: lambdaisland/fetch, could not locate lambdaisland_SLASH_fetch.cljs...". 