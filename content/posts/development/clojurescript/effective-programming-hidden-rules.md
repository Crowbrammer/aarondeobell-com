---
title: "Knowing and following hidden rules = programming success"
date: 2022-04-03T15:34:09-04:00
author: "Aaron Bell"
draft: false
---

Effective computer programming requires that you find all the hidden rules the computer and the language have and to follow them strictly until you get what you want. For instance, I'm coding in [ClojureScript](http://clojurescript.org/), a pleasant language in a hellish development environment which compiles to JavaScript that the browser can read. There's a tool I use to assist in ClojureScript's compilation tasks: [shadow-cljs](https://shadow-cljs.github.io/). I use shadow-cljs's `watch` mode to look at my ClojureScript files and load the changed files right after they're changed. This avoids the previously necessary steps of restarting the browser and the compiler every change, and by avoiding this one makes each development cycle possibly literally a hundred times faster. Tying back to the hidden rules, here are several that I had to learn to develop effectively with shadow-cljs:

 - There's two different consoles for feedback--one for the compiler, and one in your browser for the actually-run JavaScript
 - You can't add a namespace after starting the watcher--it won't be included in the hot reload, so you must restart the watcher.
 - You may learn about the init function to tell shadow-cljs what function should be run when it starts, but you must also know how to create an after-load function so that you don't need to restart your browser every change (speeding up feedback time from watch-and-reload-browser by 10x+)
 - You can't use the shadow-cljs watch tool in WSL--it doesn't detect file changes :/ 

Things like the above are not plainly told, or if they are, they're buried in an avalanche of competing information. This likely means you'll need to learn from experience or someone else's experience (a mentor). Until you know these rules, your two hour coding projects will inevitable shapeshift into a two-week coding monsters which consists mostly of evolving your understanding of your programming environment.