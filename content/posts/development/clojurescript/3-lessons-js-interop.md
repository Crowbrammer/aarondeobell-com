---
title: "Three lessons for JS interop in ClojureScript"
date: 2021-10-29T21:35:26-04:00
author: "Aaron Bell"
draft: false
---

A metaphor: I've turned my regular hand into this metal claw talon. That is, I've ironed out misunderstandings with JavaScript interop: (1) Global variables are in the js namespace (`js/gapi` not `gapi`); (2) If one `.` doesn't work, use two `..` 🙂 (If I'd examine the macro-expansion I'd understand why...); (3) Modeling JS's `obj.prop1.prop2.methodCall().prop3` syntax (i.e. calling all props, method calls, and subsequent props at once) seems out of reach atm-- use nested forms: `(-prop3 (.methodCall (. obj -prop1 -prop2)))`. 