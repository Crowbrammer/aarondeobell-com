---
title: "Coding requires thought, not effort. Thinking is questioning and answering. I'll use question-driven development to create the thinking necessary to code."
date: 2021-07-21T14:01:34-04:00
author: "Aaron Bell"
draft: false
---

When coding, I'm gonna Q&A myself. Coding requires thought not effort. I read in [Awaken the Giant Within](https://www.amazon.com/dp/B00AHE2WVO/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1) [page number needed] that thinking is mostly the process of asking and answering questions. I recall a while back that I tried this, where I didn't expect anything to get done until I first answer a question. Though it's anecdotal, and I just what I recall from 4Clojure problem, the number of which I don't remember, I remember it being effective. I'd ask the question and my mind'd jump to the right response, or look up the info, or the answer would flow on screen. I'm sure some non-verbal-minded, visual thinker could "plop" valuable code from a complex 3D image of thought, but for me, my mind requires some dialog. I wish with great pain that it were different, but this is who I am: a verbal thinker who requires high degrees of verbage to achieve his ends. 