---
title: "Installing and configging Emacs"
date: 2021-08-18T05:26:17-04:00
author: "Aaron Bell"
draft: false
---

Install [the Emacs executable](http://ftp.gnu.org/gnu/emacs/windows/emacs-27/emacs-27.1-i686-installer.exe) or unzip [the zip](http://ftp.gnu.org/gnu/emacs/windows/emacs-27/emacs-27.1-i686.zip) to gain the runemacs.exe file in the bin directory. To config on Windows, place your customizations at C:/Users/\[Username\]/AppData/Roaming/.emacs.d