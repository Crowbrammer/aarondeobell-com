---
title: "Emacs is mostly a set of keybound lisp functions"
date: 2021-06-07T03:41:08-04:00
author: "Aaron Bell"
draft: false
---

[emacs](https://www.gnu.org/software/emacs/) is largely a set of keybinding-mapped [lisp](http://www.paulgraham.com/lisp.html) S-expressions (lisp functions). Each keybinding that I do simply executes an S-expression. For more bindings, I could use new modes. Add packages to emacs and you'll experience them as major modes. Use `smex` with `M-x` + "package-list-packages" to see emacs' has a directory of ready to install packages. Use `M-x` + "package-install" when I'm ready to install one. I intend to use the [CIDER](https://docs.cider.mx/cider/index.html) mode to work with the interactive REPL, though if I didn't need the REPL, I could just use the Clojure mode for Clojure syntax. 
