---
title: "(Clojure) environment setup"
date: 2021-06-07T02:42:43-04:00
author: "Aaron Bell"
draft: false
---

I had fun reinstalling (three times on three different Windows computers): [WSL2](https://docs.microsoft.com/en-us/windows/wsl/compare-versions), [Ubuntu](https://ubuntu.com/), [Docker](https://www.docker.com/) (pulled it off W10 and put it exc. on Ubuntu for WSL), and [emacs](https://www.gnu.org/software/emacs/). Important to emacs, after seeing blank files with emacs and full files with nano, after opening a file, I learned to hit a key to show the contents. The terminal doesn't get the cue to reload the screen with the emacs output. After this I learned to save files with C-x C-s and to find files with C-x C-f, using tab completion's neat, letting you find files faster. After this I learned to configure git and to hold off on using the --S (sign) switch until I re-figure out the GPG key. Don't need to sign/prove that the commits are mine yet, so I'll use the time to figure out how to develop with a Docker instance of [Clojure](http://clojure.org/). 