---
title: "Revisiting emacs"
date: 2021-06-04T01:48:45-04:00
author: "Aaron Bell"
draft: false
---

I (again) learned to install [emacs](https://www.gnu.org/software/emacs/) on Windows and Ubuntu and what its limitations are. Windows is more of a PITA for CLI ops, so I'm sticking with Ubuntu for developing atm. That is, it supports the GUI version of emacs and that's it. I want to be more flexible, more able to work in limited environments, so I'm looking to develop in Ubuntu. This turned out to be easy with my Ubuntu version of WSL 2.0. With Ubuntu, I just `sudo apt install`-ed `emacs`. It took a bit, but I researched other things as it installed, and it appears to work flawlessly. I still need to figure out how to install [CIDER](https://github.com/clojure-emacs/cider) to develop for [Clojure](http://clojure.org/) more easily. I'm working with a CLI-only version of emacs, but I don't expect any problems getting CIDER on there. 