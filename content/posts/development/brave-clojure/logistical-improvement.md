---
title: "I want Brave Clojure meetups to cover a single idea instead of a single chapter"
date: 2021-07-13T17:44:56-04:00
author: "Aaron Bell"
draft: false
---

People feel overwhelmed with the meetup's contents. Each chapter has a dozen-plus new ideas. I think focusing on a chapter gives a false set of cohesion, because there's so much in the chapter. I think focusing on one idea in the chapter and zeroing in on that one idea for the meetup'd be huge. I.e. let people get the surface of Clojure for the Brave and True on their own time and then we'll use the meetup to drill deeper into it, asking questions about it. 