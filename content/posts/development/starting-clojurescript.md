---
title: "Starting ClojureScript"
date: 2021-05-18T05:53:36-04:00
author: "Aaron Bell"
draft: false
---

[Dunning-Kreuger](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect)'s alive and well in spirit. I started a [ClojureScript](https://clojurescript.org/guides/quick-start) project expecting it to be easy, but I hit several roadblocks trying to create it. While trying to make a local app that lets me write paragraphs efficiently, I learned that I don't know Cljs setup, [React](https://reactjs.org/), nor how to make this work with HTML. I had trouble with Leiningen, esp. with ClojureScript. I barely got the QuickStart page up using the standalone jar. I had to change hello-world to second-practice-project and make sure my folder name used underscores instead of parentheses. There's a bit of Java/CLI interaction to get it started with little explanation in the QuickStart, so that took some time to figure out, but I did! At a surface level. This all taught me why getting your hands dirty is so important: So that you may know, you learn what you don't know. This gave me enough friction that I've decided to use Simplenote for my paragraphing for now and to just tag the note and create ids for it. I should devote my time to a more useful project, like improving Dogecoin.