---
title: "Improving the image-streaming app"
date: 2021-08-11T01:54:20-04:00
author: "Aaron Bell"
draft: false
---

Because it gets such bizarre results and answers, I'm a fan of [image-streaming](http://www.winwenger.com/imstream.htm) as taught by the late Win Wenger, Ph.D. In my process of [image-streaming hundreds of times](https://dl.dropboxusercontent.com/s/9ley6rpwk421syd/2021-08-11_01-55-13.png), I found a lot of friction, so I started building an app to streamline the process. My app's still nascent, and I want to improve my image-streaming app. The main ideas to implement are: 

 - Make collecting the info enjoyable. (Give feedback at every step.)
 - Connect the audio, transcription, and explanation together in a retrievable way.

To this end, I have odds and ends to handle. Not expecting complete understanding from you, I should:

 - set the 30s default, 
 - set the max recording to 600s and record that long despite a higher entry
 - Play the recording back as soon as it's done 
 - feedback on "currently recording" and "transcribing" (fade in and out effect), 
 - connect the transcription and explanation to the audio. 