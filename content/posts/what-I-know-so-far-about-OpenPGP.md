---
title: "OpenPGP"
date: 2020-10-31T08:09:48-04:00
author: "Aaron Bell"
draft: false
---

Five people and (people from) three orgs with(?) the IETF specced the OpenPGP message format in [RFC 4880](https://tools.ietf.org/html/rfc4880). PGP means 'Pretty Good Privacy'. They specced OpenPGP as an interface for encrypting and decrypting data packets with separate-but-referenced(?) public(?) key packets. Gnu Privacy Guard (GPG), used by APT and Git, implements against this interface.

