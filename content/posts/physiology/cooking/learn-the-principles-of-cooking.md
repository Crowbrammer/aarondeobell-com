---
title: "I should learn the principles of cooking"
date: 2021-07-13T03:11:29-04:00
author: "Aaron Bell"
draft: false
---

I'm trying to make individual meals instead of learning to cook in general. Not the best idea, for if I learn some principles, I can make many delicious meals instead of just the one. Whenever I do something, I should ask, "Why" and attempt to answer it. This why'll let me reason at a deeper level, letting me cook more things in a better way. If I don't have a great answer, I shouldn't sweat it, but I should try to ask it so that I know my reasons for doing things.