---
title: "A problem-solving algorithm where you write a bunch and close your eyes"
date: 2022-04-19T13:38:21-04:00
author: "Aaron Bell"
draft: false
---

A problem-solving algorithm:

 - Write the problem and all its subproblems down
 - Write what you know and don't know
 - To increase your knowns, read, converse, listen, watch
 - Criticize knowns and existing solutions (not written about here)
 - Cut out external stimuli to stimulate a recall response
 - In general, wait for a solution

There's an intersection of knowns and unknowns, the conscious and subconscious mind that lasers through problems with force and speed. The process of the correct use of the mind necessitates lower expectations of correctness and time. You will be wrong many times, but these will be better informed wrongs and your rights will likely be magnificent or at least satisfying. Your mind will take longer to solve things than you may want, but you will get true solutions later rather than never if you choose not to follow the nature of your brain and mind. 

After your expectations are managed, the process of using this "laser with force and speed" is through a combination of your hands and thought. Write your problem, the problems of the problem (recursively), to get an overview of it. At each level, write everything you know (assumptions, possible facts) and don't know about it (questions, guesses). Aim to increase your knowns through reading, talking to people, listening, watching--finding possibly existing models for your solution. 

Once you're loaded up, perhaps signaled by excess tension or fatigue in your body, let your mind process it in two contexts: waking and sleeping. Recall is esp. important for memory (Oakley), and relaxed recall enables abstraction and connection necessary for sophisticated solutions impossible to conceive by the conscious mind alone. The subconscious is either in collecting mode or generating mode. Recall is when you're in generating mode. Generating mode is best initiated by cutting off external stimuli. You can initiate generating mode (or "recall") by cutting off external stimuli: close your eyes and maybe use earplugs. 

After this, it's just waiting game, waiting for the solution to cook.

### Bibliography

Hickey, Rich. (Oct 2010). "Step away from the computer or hammock-driven development". https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/HammockDrivenDev.md

Oakley, Barbara. (Unknown). Learning how to learn: powerful mental tools to help you master tough subjects. https://www.coursera.org/lecture/learning-how-to-learn/terrence-sejnowski-and-barbara-oakley-introduction-to-the-course-structure-1bYD5

Polya, G. (2014; first published in 1971). How to solve it. https://www.amazon.com/How-Solve-Aspect-Mathematical-Method-ebook-dp-B0073X0IOA/dp/B0073X0IOA [Hickey's talk heavily references this.]
