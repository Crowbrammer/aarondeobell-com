---
title: "How I see Riemann"
date: 2021-04-13T19:41:45-04:00
author: "Aaron Bell"
draft: false
---

[Riemann's](http://riemann.io/) a "server", a listening app, that listens for messages on a [socket](https://en.wikipedia.org/wiki/Network_socket) via [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)/[UPD](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) and (among other things) updates a key-value table (?), an index, with [time-constrained events](http://riemann.io/concepts.html) (using a time-to-live field, TTL). This servant, the server, waiting to take orders, events, from other programs called [clients](http://riemann.io/concepts.html). Clients, other programs, send orders over TCP/UDP, and Riemann reacts to it. When thinking about Riemann, it's important to distinguish between Riemann, the actual server, and something that communicates with it, a Riemann client. Any program that communicates with Riemann is a client, but many people of many languages have created a wrapper called a "[Programming Language] Riemann Client". I don't see the wrapper itself as the client. The wrapper makes it easier for your program to communicate with Riemann and therefore be a client. 