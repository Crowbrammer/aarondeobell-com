---
title: "Thorchain"
date: 2021-05-11T08:40:56-04:00
author: "Aaron Bell"
draft: false
---

[Thorchain](https://thorchain.org/) decentralizes cryptocurrency exchanges. Like Bitcoin took the responsibility of transactions from the bank and spread them to the many nodes world-wide, Thorchain will take the responsibility of intercurrency transactions from the exchanges and spread them to many nodes world wide. Crypto's supposed to be decentralized, and having exchanges like Binance and Coinbase takes away from this, and Thorchain intends to change this. Using the Thorchain network and not a centralized exchange, it quickly trades your coin for RUNE, and trades the RUNE for the actual other coin (not a token like other exchanges [source needed]). This lets you easily use your currency (BTC) to buy other currencies (DOGE) with Thorchain without the middleman.