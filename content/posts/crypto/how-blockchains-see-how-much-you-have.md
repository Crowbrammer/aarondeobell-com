---
title: "Bitcoin/Dogecoin use a UTXO model to see how much you have"
date: 2022-01-03T12:22:16-05:00
author: "Aaron Bell"
draft: false
---

Crypto is weird. With it, you really spend transactions labeled with coin values (UTXOs) and not coins themselves.

The blockchain uses an Unspect Transaction Outputs model a.k.a. [a UTXO model]((https://en.wikipedia.org/wiki/Unspent_transaction_output)) to calculate what a person's able to spend. Assume a brand new network which genesises three transactions--1BTC, 0.5BTC, 2BTC--for 3 UTXOs. Someone else joins the network, and you decide to pay him 0.25BTC. [Like pulling a $5 from your wallet](https://komodoplatform.com/en/academy/whats-utxo/), you pull one UTXOs and give it to the network, probably the 0.5BTC, which decreases the UTXO count to 2. The network then creates two new UTXOs of 0.25BTC for you and the recipient for 4 total UTXOs. The set of UTXOs are a subset of all transactions. I believe the full node traverses through all the blocks to generate a UTXO set and then removes/creates UTXOs from each generated block. This UTXO set [is stored in local memory](https://bitcoin.stackexchange.com/questions/37397/where-is-the-utxo-data-stored) and not in the actual blockchain. 

To find the value of a wallet (a hash :/ ), it looks at all transactions pointing to the wallet and sums 'em up. The UTXO model is a copy of all transactions with transactions you used removed. 