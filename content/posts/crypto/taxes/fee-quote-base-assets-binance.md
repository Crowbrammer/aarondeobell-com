---
title: "With Binance, Fee Assets are the type of currency you use to pay Binance; Base Asset is the type of currency offered; Quote Asset is the type of currency received"
date: 2022-02-10T05:44:58-05:00
author: "Aaron Bell"
draft: false
---

Binance has three accounting categories for assets: "Fee", "Quote", and "Base". 

When I first looked at the table, I got fixated on "Buy" and "Sell" operations and looked at Base and Quote through those lenses. 

It's an overcomplication though, because "buy" and "sell" are the same coin looked at from different angles. In both cases one item is given for another; an exchange is made. 

Regardless of Buy or Sell operation:
 - "Fee Asset" means "the type of currency to pay Binance for the currency exchange"
![Fee Asset demo](https://dl.dropboxusercontent.com/s/zbu8w1c6i6d77v0/EXCEL_2022-02-10_05-48-45.png)

 - "Base Asset" means "The type of currency I'm giving", and
 - "Quote Asset" means "The type of currency I'm receiving"

![Sell-based Quote and Base assets](https://dl.dropboxusercontent.com/s/1gc6wpgyb4fnamh/EXCEL_2022-02-10_05-51-17.png)
![Buy-based Quote and Base assets](https://dl.dropboxusercontent.com/s/yvdosd3sv06bvri/EXCEL_2022-02-10_05-53-55.png) 