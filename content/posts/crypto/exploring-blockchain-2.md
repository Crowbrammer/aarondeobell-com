---
title: "Exploring blockchain #2"
date: 2021-05-11T23:37:09-04:00
author: "Aaron Bell"
draft: false
---

Blockchains are sequences of blocks representing currency and their transactions, secured with special marks called hashes.  Blocks have three key aspects: the hash of the current block (including the previous hash and the current transactions), the previous block's hash, and the transactions for the block. These three chunks build the block on a blockchain. These blocks are connected both by index and that they have a hash of the previous block's hash. This disambiguates the jellyfish tree or the string of cans behind the car image of the blockchain, showing it as a linearly composed thing, with each child node have a hash of the previous node which had a hash of the node before that down to the genesis block. There are pointers from each block to one or more wallets with the amount owned attached to the arrow. The transactions can point from one block to another, with a group of entities, wallets, surrounding it. 