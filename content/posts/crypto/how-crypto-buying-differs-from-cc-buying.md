---
title: "How I think crypto buying differs from CC buying"
date: 2021-04-21T16:29:00-04:00
author: "Aaron Bell"
draft: false
---

I think the crypto payment model differs from the credit card payment model. With credit cards, you give someone the authority to pull money from your account with your credit card number (Stripe, Paypal). I don't see that model here. For example, Bitpay sets up a temporary wallet for the given purchase, and you have to personally send that money to it from your wallet. Once it receives a certain amount, it forwards it to the destination and marks it complete. After this, it lets the vendor know you've paid, and they have no reason not to ship your product. 