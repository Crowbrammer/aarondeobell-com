---
title: "Paper wallet fundamentals"
date: 2021-12-31T16:29:12-05:00
author: "Aaron Bell"
draft: false
---

A wallet is a container for pairs of strings. You can have 2, 4, 6... 2n strings in a valid wallet. The two strings of each pair are a public hash string and a private hash string. Why both and not just private hash strings? Without the public, the software it'll eventually work with won't know which address to unlock coins for ("key" is a bad metaphor; the public key is more like a lockbox, and the private key is more like a key). Without the private key, you can't tell the blockchain that it's okay to delink coins from your address and link them to a new address. Software like Dogecoin Core (and Bitcoin Core) and Multidoge store these pairs in .dat files (.dat wallets). These are just containers--"wallets"--for the pairs of sha strings. Though you can't see the strings in .dat files with a text editor, [the strings are there](https://www.reddit.com/r/dogecoin/comments/1x6xvp/comment/cf8mv6c/?utm_source=share&utm_medium=web2x&context=3) (Create a new receiving address, go to Help > Debug > Console, and then `dumpprivkey <public key>`). With paper wallets, you store the the public hash string and private hash string(s) together on a piece of paper instead of storing in a standard .dat file. 

P.S. Dogecoin Core automates paper wallet printing. 

![Dogecoin Core Paper Wallets](https://dl.dropboxusercontent.com/s/sq96axkhmt6o6du/2021-12-31_15-56-44.gif)