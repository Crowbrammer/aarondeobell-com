---
title: "Exploring blockchain #1"
date: 2021-04-29T11:20:40-04:00
author: "Aaron Bell"
draft: false
---

While reading [the cloned Bitcoin C++ code](https://github.com/dogecoin/dogecoin/commit/5b45bf400e4938fd15540d215b91fddcf4217713) in [the Dogecoin repo](https://github.com/dogecoin/dogecoin), I realized I didn't understand blockchain enough. A while back YT recommended [MIT's blockchain OpenCourse](https://www.youtube.com/watch?v=EH6vE97qIP4), and while seeing the "testnet" and "ipc" in the C++ file, the thought of [that OpenCourse](https://ocw.mit.edu/courses/sloan-school-of-management/15-s12-blockchain-and-money-fall-2018/index.htm) popped in my head. I searched for it, and I pulled up [the course's reading material](https://ocw.mit.edu/courses/sloan-school-of-management/15-s12-blockchain-and-money-fall-2018/readings/). From this I searched for and started reading [the most substantive-looking doc I could find](https://csrc.nist.gov/CSRC/media/Publications/nistir/8202/draft/documents/nistir8202-draft.pdf). While reading, I realized I didn't know as much as I thought I did about what even a "block" is and how "consensus" is achieved, i.e. what is proof of work vs. proof of stake vs. round-robin?