---
title: "How I think Bitpay generally works"
date: 2021-04-21T16:29:35-04:00
author: "Aaron Bell"
draft: false
---

To see the future of businesses accepting crypto, I'm trying to understand Bitpay. I think Bitpay sets up a temporary wallet for each purchase. It then takes the price of the item in USD and uses the current market price to convert it to the crypto of your choice and sets that as the threshold for "having paid". Once it receives this amount, it forwards it to the vendor's wallet and marks it as paid, and some fulfillment software comes in and, boom, your item is shipped. 