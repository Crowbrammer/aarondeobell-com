---
title: "Dogecoin stats"
date: 2021-04-29T06:20:42-04:00
author: "Aaron Bell"
draft: false
---

[The Dogecoin repo](https://github.com/dogecoin/dogecoin)'s stats show 201 Devs over 140 merged pull requests have committed 13,154 changes to the master branch of the Dogecoin repository between [Aug 29, 2009](https://github.com/dogecoin/dogecoin/commits/master?after=f80bfe9068ac1a0619d48dad0d268894d926941e+13525&branch=master) and [Feb 28, 2021](https://dl.dropboxusercontent.com/s/0hblaqaenn7sk84/2021-04-29_06-21-31.png). Many commits seem Bitcoin specific, with the earliest commits from [non-github-bitcoin](https://github.com/non-github-bitcoin) for commits by people without GitHub accounts, so there've like been less than 6,000 Dogecoin-specific commits. However this [only shows the master branch and not the dev branches](https://github.com/dogecoin/dogecoin/branches) where much action takes place. The developers of Dogecoin appear to have cloned [the Bitcoin GitHub repo](https://github.com/dogecoin/dogecoin/branches) and developed it [since around December 3, 2013](https://en.wikipedia.org/wiki/Dogecoin) into the Dogecoin we know today. 
