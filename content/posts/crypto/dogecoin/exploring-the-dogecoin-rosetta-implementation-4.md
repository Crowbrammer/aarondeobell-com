---
title: "Exploring the Dogecoin Rosetta implementation #4"
date: 2021-04-28T04:07:09-04:00
author: "Aaron Bell"
draft: false
---

Go doesn't download the dependencies properly with my current setup, and I don't understand the Docker/Go environment enough to make it do so. [The README for the rosetta-dogecoin project](https://github.com/rosetta-dogecoin/rosetta-dogecoin) says to use `make deps` which runs `[go get](https://golang.org/pkg/cmd/go/internal/get/) ./..` which should download and install dependencies, but it doesn't download properly. I see $GOROOT and $GOPATH and github.com links in the error though, so at least I have a lead.