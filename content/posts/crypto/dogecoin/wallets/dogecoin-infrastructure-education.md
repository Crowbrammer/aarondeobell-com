---
title: "You can't make a paper wallet with Multidoge "
date: 2021-06-11T04:19:29-04:00
author: "Aaron Bell"
draft: false
---

Multidoge is a Dogecoin.com-recommended wallet app. Knowing this, and becoming curious about making a paper wallet, I investigated the Multidoge app and its docs. I'd quickly learn that you can't (easily) do paper wallets. You can only pull the wallet file(s?) onto an external device.
