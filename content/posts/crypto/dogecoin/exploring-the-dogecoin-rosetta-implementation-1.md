---
title: "Exploring the Dogecoin Rosetta implementation #1"
date: 2021-04-27T03:53:16-04:00
author: "Aaron Bell"
draft: false
---

I'm seeing some activity on [the Dogecoin Rosetta API project, rosetta-dogecoin](https://github.com/rosetta-dogecoin/rosetta-dogecoin). I see about [one major update of this every two weeks](https://github.com/rosetta-dogecoin/rosetta-dogecoin/commits/main). If there's discourse, it's mostly not on a public project or kanban but mostly a private one. With what public activity I see, the people I do see [appear to be smart people working to figure it out](https://github.com/rosetta-dogecoin/rosetta-dogecoin/issues/37). I wish there was more work and activity on this though. There's still a lot of work to be done, and we're relying on a few developers giving some of their free time. Chris H. appears to be right about the lack of development power behind it.

More than just the commits and issues, I'm starting to dip my feet in the water to see how it works for myself. I want to understand what's going on, so I'm trying to run what they have so far and see what the rosetta-cli checks on their tests say. I'm running into trouble doing this on Windows 10 though. Coding and working with this requires Linux: `make` files in this project and `sh` to install [rosetta-cli](https://github.com/coinbase/rosetta-cli) to check the implementation. My attempts to install with Windows fell short until I started using WSL. I successfully installed rosetta-cli, but I'm getting errors installing rosetta-dogecoin. 

My main issue is with Go. The rosetta-dogecoin project requires [ed25519](https://golang.org/pkg/crypto/ed25519/). This is apparently supposed to be pre-installed, but my `go build` and `make deps` seem to error out over this. I see that the ed25519 package was copyrighted in 2016, and the Golang team released Go 1.10 in 2018, so this means that it should be there, but if it errors over this, then ed25519 doesn't seem to be installed in my Go installation, Go 1.10. I've seen the issue tracker mention updating it from Go 1.15 to Go 1.16, so maybe this is the issue, even though ed25519 should be installed by now. I'll update my Go version, and see what happens, but an old Go version might not be the case.

