---
title: "Exploring the Dogecoin Rosetta implementation #5"
date: 2021-04-28T04:56:43-04:00
author: "Aaron Bell"
draft: false
---

I studied [the Rosetta API documentation on Docker Deployment](https://www.rosetta-api.org/docs/node_deployment.html#multiple-modes), and I learned that a Docker container should run the Data API in "online mode" and the Construction API in "offline mode". A team should create a single Dockerfile for both modes for less maintenance requirements an overhead.

The Rosetta API limits OSes used for implementing it for security (?). It suggests but doesn't require Ubuntu as the base for the Dockerfile, saying it's common practice to limit OSes to increase speed to security pat

There's something about not using prebuilt Docker images as you go forward in time, but I don't fully get that yet.

I'm spinning my wheels with getting the rosetta integration working for either Dogecoin or Bitcoin. I don't understand why `make deps` nor `go build` (?) refuse to download the badger db and ed25519 cryptography packages with my setup. 