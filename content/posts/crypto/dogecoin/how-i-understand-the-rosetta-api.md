---
title: "How I understand Coinbase's Rosetta API"
date: 2021-04-26T23:21:44-04:00
author: "Aaron Bell"
draft: false
---

To integrate with Coinbase, Dogecoin needs to work with [the Rosetta API](https://www.rosetta-api.org/). The Coinbase team made Rosetta API to enable owners of blockchains to more easily become a part of Coinbase. In order for Coinbase to work with a blockchain, it needs to be able to know things about the blockchain and to make transactions on the blockchain. This is why [the Data API and the Construction API combine into the Rosetta API](https://github.com/coinbase/rosetta-specifications). They're the two key ideas in the Rosetta API that will let Coinbase work a blockchain like Dogecoin.

The Data API and the Construction API form the Rosetta API. Conforming a blockchain's code to the Data API lets Coinbase answer questions about the blockchain: get networks holding the blockchain, blocks, and addresses related to the blocks. Conforming a blockchain to the Construction API lets Coinbase modify the blockchain (like a person would?): Create a transaction, sign it, and broadcast it to the other blockchain holders on the connected networks. This lets Coinbase transfer money between wallets ("accounts"). 

I think someone with competence in the blockchain's language (like C++) and who's willing to read and understand the API can successfully integrate a blockchain into Coinbase. Though the Coinbase team programmed Coinbase in Go, they made the Rosetta API adherable in any language, such as C++ which Dogecoin is written in, which makes it more feasibly. Once the Rosetta code's written, Coinbase has a series of checks to see if you've properly conformed your code to the Rosetta API. Once these checks pass, I think it's only a matter of time before we see it on the exchanges. 