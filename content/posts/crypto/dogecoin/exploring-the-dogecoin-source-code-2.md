---
title: "Exploring the Dogecoin source code #2"
date: 2021-04-30T04:28:45-04:00
author: "Aaron Bell"
draft: false
---

The Dogecoin devs have built on the Litecoin and Bitcoin code [since December 6th, 2013](https://api.github.com/repos/dogecoin/dogecoin). They kept the revolutionary work that secured the blockchain and made it usable and built on that to further make it democratic, less expensive, and more joyful to us.  At [version 1.6](https://github.com/dogecoin/dogecoin/releases/tag/1.6) they made Dogecoin as friendly to mine to the individual miner as the multipool miner by making the block reward consistent. At [version 1.7](https://github.com/dogecoin/dogecoin/releases/tag/v1.7.0) they updated it from [a Litecoin base](https://github.com/litecoin-project/litecoin) to [a Bitcoin base](https://github.com/bitcoin/bitcoin). [The version 1.8 upgrade](https://github.com/dogecoin/dogecoin/releases/tag/v1.8.0) uses AuxPoW which increased the hashrate and number available miners: increasing the hashrate, incentivizing more miners, and letting people mine for Litecoin and Dogecoin instead of having to choose. Because they didn't have to choose, they mined Dogecoin, which processed transactions and increased network security. The devs made these three changes in less than a year. 