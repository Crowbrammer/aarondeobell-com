---
title: "Exploring the Dogecoin Rosetta implementation #7"
date: 2021-04-29T02:42:53-04:00
author: "Aaron Bell"
draft: false
---

I tried testing the Dogecoin Rosetta API, and here's what I've seen so far. Coinbase has a tool for testing the Rosetta implementation, rosetta-cli. With both a Docker and local setup, I see about 40-70% test coverage, with twelve of the tests failing. I saw the tests run in my rosetta-dogecoin instance. There are two modes, "OFFLINE" and "ONLINE". Test tests fail or don't run for OFFLINE, and it's not connecting to the test not for ONLINE, so no tests for that. I see "asserter", "indexer", and 'network/status" in the error logs. The network/status thing's probably due to my environment.