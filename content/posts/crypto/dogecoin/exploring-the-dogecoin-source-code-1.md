---
title: "Exploring the Dogecoin source code #1"
date: 2021-04-29T11:20:02-04:00
author: "Aaron Bell"
draft: false
---

I had a hardish time seeing Dogecoin's true history--where we can see the work of the Dogecoin devs and not the Bitcoin devs. Cloning a GitHub repo also clones the commit, so in [the 13K+ unsearchable, unfilterable, and unsortable repo commits](https://github.com/dogecoin/dogecoin/commits/master) I find it hard to see where the Dogecoin devs started. Of the 159 releases on the repo, [the first official Dogecoin release on the GitHub repo I can find](https://github.com/dogecoin/dogecoin/releases?after=1.4) starts at 1.2 on December 23rd, 2013. I looked at the commits on and after that date. At least in the late 2013/early 2014 area, the commits still seem Bitcoin-related though. I'm not seeing a non-Bitcoin dev (someone who has the Dogecoin repo as one of their commits/forked repos) commit in this timeframe. Is there a way to see Doge Dev commits more easily? 

