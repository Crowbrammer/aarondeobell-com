---
title: "Exploring the Dogecoin Rosetta implementation #2"
date: 2021-04-27T07:43:22-04:00
author: "Aaron Bell"
draft: false
---

You need to understand Docker to work with the Dogecoin Rosetta project. While the developers use [Go](http://golang.org/), [shell script](https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_02_01.html), and [GNU Make](https://www.gnu.org/software/make/), the most important thing to understand is [Docker](https://www.docker.com/) to run it all. You need to know how to build a Docker file (`docker build .`) and run it. This will make the other commands like make deps and so forth work. I was trying to install the new version of Golang, but this should already be available in the Docker container. While editing this, I started Docker, ran `docker build .` in the rosetta-dogecoin directory, and boom there's like thirty steps to build everything. Once I learn how to run the built image, I think I'll be able to run the [rosetta-cli](https://github.com/coinbase/rosetta-cli) on a current edition of rosetta-dogecoin to see what's not ready yet for Coinbase.