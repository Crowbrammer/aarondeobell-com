---
title: "Hash functions"
date: 2021-05-20T10:26:29-04:00
author: "Aaron Bell"
draft: false
---

A [hash function](https://en.wikipedia.org/wiki/Hash_function) is one of the [cryptographic primitives](https://en.wikipedia.org/wiki/Cryptographic_primitive) used to "protect communication in the presence of adversaries" ([Gensler, Fall 2018](https://ocw.mit.edu/courses/sloan-school-of-management/15-s12-blockchain-and-money-fall-2018/video-lectures/session-3-blockchain-basics-cryptography/)). The same input will always provide the same output, a string of 0's and 1's in the case of [SHA256](https://en.wikipedia.org/wiki/SHA-2)(?).  Knowing the hash function's internals in most cases does not make it less secure. Afaik, you can know the SHA256 internals and the outputs and will be none-the-wiser on the what the input was. Maybe some smart guy can figure out the pattern, but for some reason, it's unreversible with current tools, pre-quantum era. The consistency of inputs always producing the same outputs, and with slightly different inputs changing the outputs, blockchain tech uses it to verify the blockchain. 