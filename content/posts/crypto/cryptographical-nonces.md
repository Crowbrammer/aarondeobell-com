---
title: "Cryptographic nonces"
date: 2021-05-07T11:42:28-04:00
author: "Aaron Bell"
draft: false
---

I saw "nonce" in the Bitcoin and Dogecoin code, so I wanted to learn about it. Looking it up, I see that [a cryptographic nonce](https://en.wikipedia.org/wiki/Cryptographic_nonce) is a hash of a random number (?) sent from one computer to another, like a one-time secret password between compadres to get into the tree fort. Nonces make hashing harder for computers and somehow makes it more secure. I know that cryptocurrencies use nonces for "proof-of-work", but I don't know more than that atm. 