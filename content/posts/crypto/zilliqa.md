---
title: "Zilliqa"
date: 2021-05-16T16:12:44-04:00
author: "Aaron Bell"
draft: false
---

[The Zilliqa team](https://www.zilliqa.com/) would like to use [proof-of-stake](https://en.wikipedia.org/wiki/Proof_of_stake), but in [the 2017 whitepaper](https://docs.zilliqa.com/whitepaper.pdf), they haven't found a secure proof-of-stake algorithm, so they use proof-of-work. Zilliqa forms consensus with a [SHA3-256](https://en.wikipedia.org/wiki/SHA-3)-using [proof-of-work](https://en.wikipedia.org/wiki/Proof_of_work) from many sub-networks in the networks. That is, instead of having all 10,000 (for example) nodes working on the same blocks, have some "directory service" nodes divide the 10,000 "member" nodes into ten groups of 1,000 and process ten blocks of transactions at once instead of one. This lets you get roughly ten times the work done minus parallelization overhead. This parallel design lets the Zilliqa network mine much faster than Bitcoin _and_ Ethereum 1.0. 