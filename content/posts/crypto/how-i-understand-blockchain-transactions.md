---
title: "How I understand blockchain transactions"
date: 2021-05-01T06:09:29-04:00
author: "Aaron Bell"
draft: false
---

The blockchain network protects the truth with hash functions. When a computer with a correct private key for an account intends to transfer a block from one account to another, it adds an entry to the transaction history and puts it through a hash function and distributes it to the network. Many computers own the hash history, so you can't easily fake it. Whenever a computer wants to verify the transaction, it sends it to other nodes, other computers with the blockchain daemon running, and it gives a valid or invalid response. If it's valid, then the computer creates and broadcasts transaction to the network of copies of the blockchain. If it's invalid, then it maybe errors and then does nothing. 