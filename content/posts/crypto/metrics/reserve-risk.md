---
title: "Learn what \"Supply-Adjusted Coin-Days Destroyed\" (SACDD), \"Median Value of CDD\" (MVOCDD), and the \"HODL Bank\" are to understand the  \"Reserve Risk\" metric"
date: 2022-02-10T21:12:31-05:00
author: "Aaron Bell"
draft: false
---

[Glassnode guy](https://www.youtube.com/watch?v=Qfm_XB3fhF0) uses three indicators to build a macro-indicator of market strength: [the Reserve Risk](https://academy.glassnode.com/indicators/coin-days-destroyed/reserve-risk). The three metrics to compute the Reserve Risk are:

 - ([Supply Adjusted](https://academy.glassnode.com/indicators/coin-days-destroyed/supply-adjusted-cdd)) [Coin Days Destroyed](https://academy.glassnode.com/indicators/coin-days-destroyed/cdd-coin-days-destroyed): Spent coins x number of days each coin existed (divided by the total supply)
   - Analogy: Letting go of a worker who's been at the company five years vs. releasing a new hire
 - (Median) Value of Coin Days Destroyed: Price x Coin Days Destroyed
    - Does it consider _all_ coin-days destroyed _ever_?
 - HODL bank: Price - Median Value of Coin Days Destroyed

The HODL Bank shows how much fiat hodlers have given up to hold. Useful alon, but this focuses too much on the "stick". To consider the "carrot" to sell, he takes the current price and divides it by the HODL Bank. This is the Reserve Risk, and it considers both the hodlers' ability to endure pain (money given up to hold) and their ability to resist temptation (avoid the current fiat value).