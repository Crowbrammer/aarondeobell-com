---
title: "Dual mining = less profit?"
date: 2021-11-30T01:13:12-05:00
author: "Aaron Bell"
draft: false
---

I think you're required to [mine 30% ETH and 70% another crypto](https://techsocialnetwork.org/t-rex-full-unlock-on-lhr-gpu-dual-mining-awesome-job/) in t-rex with dual-mining. I.e. you can't flip it for 70% ETH and 30% RVN. The other crypto generally isn't as valuable, whereas ETH constantly rises, making this less value then a strict 70% ETH operation. Further, dual-mining attempts to trick a smart hash-rate limiting program with variable shutdowns in LHR. Iiuc, solutions to this required [underclocking the GPU](https://www.reddit.com/r/EtherMining/comments/q5lvrp/comment/hg6uazu/?utm_source=share&utm_medium=web2x&context=3) to avoid hitting LHR limits. Single-mining until I'm convinced of a more profitable dual-mining solution. 

