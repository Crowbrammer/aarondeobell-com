---
title: "Ingratitude ruins relationships."
date: 2023-01-21T01:42:48-05:00
author: "Aaron Bell"
draft: 
---

I've failed eight intimate relationships (four broke up with me, and I broke up with four). The breaking point issues were: my shame, my inadequacy, my ingratitude, feeling unloved (due to being quiet where communication was needed. one on my count, two on others' counts), and blasphemy (I love and respect God). I suspect ingratitude is the worst offender of all because gratitude lets me build an emotional bank balance for people, so that when they fuck up, I still feel inclined to help and care for the other person. In every single case, I did not practice gratitude, and I did not have that bank balance, and then we ended things prematurely :( I shall practice gratitude for any relationships I have going forward. 