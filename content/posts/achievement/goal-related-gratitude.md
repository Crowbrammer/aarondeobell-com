---
title: "Hypothesis: Use goal-related gratitude to achieve goals ASAP"
date: 2022-04-01T19:22:55-04:00
author: "Aaron Bell"
draft: false
---

IDing what one wants and then constantly focusing on what one has related to this want will achieve for this person more than anything else. Today I zoned out and I recalled an idea from [On Intelligence](https://www.amazon.com/Intelligence-Understanding-Creation-Intelligent-Machines/dp/0805078533) (Hawkins, 2005) called the "auto-associative" principle. Incomplete ideas will pull in more ideas until it's more or less complete. Thoughts attract like thoughts. Pay for whole, fresh banana bundles with single or rotten bananas. In other words, one's achievement's limited by the extent of one's goal-related gratitude, and with this thought I built a positive feedback loop for pleasant and fast achievement. 

To practice this: first, ID what you want; second, continuously ID what you have of what you want through questions--embracing any weird phrasing, usually along the lines of "What X do I have?". For me right now, I want fifty paragraphs, so it's "What fifty paragraphs do I have?". Weird phrasing, but my mind gets it. 

I had to wrestle with it feeling dumb or pointless to focus on what I have with the goal--because I wasn't convinced that it brought results (how am I supposed to act on what I already have?), but recalling the auto-associative principle from On Intelligence sells me on this to my core. Then from experience, where I've spun my wheels watching YT all day, I followed through and started writing. 

Things I've achieved with this today alone:
 - Prepared for my job early instead of putting it off till the last minute
 - Wrote a paragraph immediately instead of doing ten other things before I started writing
 - Made sense of a bad memory, feeling relief

Things I've missed using this:
 - I usually sleep well, but using this I didn't sleep and will now go to work a lil sleep deprived


