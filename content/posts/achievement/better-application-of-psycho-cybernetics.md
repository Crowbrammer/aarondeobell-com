---
title: "Better application of Psycho-Cybernetics"
date: 2021-05-10T22:54:48-04:00
author: "Aaron Bell"
draft: false
---

I found a better way to activate the subconscious, to apply Psycho-Cybernetics and let my subconscious mind do the work properly. If the subconscious were a gun, the goal'd be where it's aimed at, physical health'd be the bullets, and emotion'd be pulling the trigger. If it were a sentry, the goal be a programmed target, physical health'd be the bullets, and emotion'd plug it in and make it start firing. So I got that you should keep your goal in mind, but I had a difficult time with emotion (feel enthusiasm, dammit! :D )... until today when I remembered something Tony Robbins said: "Emotion is created by motion" (in both Awaken the Giant Within and in Personal Power II). Motion creates emotion. I can't force myself to feel emotion directly, but I can always move a little bit to cause this emotion. This emotion then seeps into my subconscious (the nervous system) which causes it to move to the goal in my mind. Knowing this, I now focus on the (symbol of the goal) and ask, "What's the motion". I tested doing this on this paragraph, and it let me blast through this paragraph with little effort in about fifteen minutes, where I struggled with it for hours before. 