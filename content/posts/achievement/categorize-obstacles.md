---
title: "Categorize obstacles as \"Inspiration to Action\", \"Know-how\", or \"Activity Knowledge\""
date: 2021-08-23T17:01:29-04:00
author: "Aaron Bell"
draft: false
---

I wanted to know how to follow the [Strategy Circle](https://www.youtube.com/watch?v=xqhjKxSd9vk) better and with more consistency. An aha came as I remembered [W. Clement Stone's Success System that Never Fails](https://www.amazon.com/Clement-Stones-Success-System-Never-ebook/dp/B01KBMTFYS), which consists of: Inspiration to Action (I2A), Know-how (KH), and Activity Knowledge (AK). The aha is to categorize the obstacle in one of these three ways and act accordingly. I2A involves raw desire times the energy to achieve it. If I lack drive, refine my self-image into one who feels pain from not achieving the thing and pleasure from achieving it. Set deadlines. For energy, focus on breath, take a nap, eat, whatever's needed, then get to it. For know-how, ID a role model if possible and seek coaching. Whether or not coaching's available, ID the required actions and rehearse it. For activity knowledge, read and write until I feel strong in my knowledge. Seek counsel from others. Categorizing obstacles into one of these three categories should simplify and facilitate solutions that will land me the goal. 