---
title: "Mr Goal Part Two"
date: 2021-07-25T10:11:38-04:00
author: "Aaron Bell"
draft: true
---

And while this will likely be effective by itself, I use self-image theory to crank things up a magnitude. After I id what the goal wants, I'll see "who achieves this" and imagine myself as this person. Within physical limits, the self-image determines the psychological limits of behavior. What you can do to achieve more rapidly and on auto-pilot is to optimize your self-image toward whatever's needed. The most effective way I've discovered to implement a new self-image is to use the word "after". The most effective way to focus on any goal is to focus on the "after". What is life like after the goal's achieved? In reality, goals exist in memory. They're a feeling. It's a "knowing". So a subtle yet effective mental technique is to see yourself opening the fridge for a snack... with your goal achieved. Then for the better self-image approach, to see yourself turning the faucet... as the new person. 

A true self-image exists when you're not thinking about it. Once you've achieved your goal, the goal's achieved while you're sleeping, talking to someone, opening a door. In every facet of reality, your goal will be achieved. It's done; you've won; and you'll be doing other stuff in an enhanced way. 

 I've discovered an effective way to implement a new self-image. A true self-image exists in the day-to-day, mundane reality. 

If the goal means anything to you, you should keep a constant look out for what Mr. Goal wants and to become the person who gets it for him. In other words, I propose a new goal achievement strategy:

1. ID a goal, "What goal would I not regret setting?"
2. Ask "What does the goal want?" (treat it as a person who needs something from you to give it to you)
3. Upon knowing, ask "Who achieves this [thing for Mr. Goal?]"
4. New self-image in mind, say "After being this..." and see random scenarios where you "just are" this new person. Celebrate (an important word) yourself opening the fridge as this new person, sitting down as this new person, drinking water as this new person (etc.). 
5. Eventually your subconscious' autopilot should kick in, with the new self-image in place, and you'll start achieving the things the goal needs. 