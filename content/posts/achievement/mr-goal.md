---
title: "Mr. Goal"
date: 2021-07-25T09:59:41-04:00
author: "Aaron Bell"
draft: false
---

I posit a potentially better goal-achieving system. [One thing people generally agree on is, "You get/become what you think"](https://www.youtube.com/watch?v=y5x-KjBME_E). I know that questions direct focus, so I repeat questions almost as a mantra. I tried to direct my foucs on the goal with the question, "What do I want", but I've found this ineffective as the original goal fades, and the answer transforms to "Relax, watch Naruto, check Twitter, sleep...", all short-term gratifications that do not progress me to the goal. With these new gratifications desired, the original goal is out of focus, and is not being focused into reality. 

While napping, my subconscious resurrected an idea I had about a year ago: "What does the goal want?". I see the goal is almost like its own person, with a valuable good or service (the state of reality you'd like), and it's waiting for a certain price/conditio to be met. All that matters for goal achievement, for whether or not this "person", Mr. Goal, gives this state of reality is whether you pay the price. It doesn't care whether you're rested or not; whether the necessary tasks feel good or not. It just needs something done. You need a way to focus on these things regardless of your short-term feeling, so that you can get them done. (Note: You should optimize your state. Just because it doesn't care about how you feel doesn't mean you have to feel bad). 

So to do two things at once: Keep the original goal in mind and meet these conditions, I propose asking the question "What does the goal want?". It keeps the original goal in mind and focuses you on the conditions that will meet Mr. Goal's needs to "pay you the new state of reality". Then, with you thinking about these conditions enough, you'll generally get these conditions, and because Mr. Goal is satisfied, you'll get your new, once-desired-now-had state of reality.