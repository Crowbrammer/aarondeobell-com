---
title: "My long-term goals"
date: 2023-01-12T20:02:10-05:00
author: "Aaron Bell"
draft: false
---

I'm going to be someone who cares about people, and who develops assets that works for him until he's powerful enough and free enough. I'll develop enough problem knowledge and technical knowledge and business knowledge to find something worth building, figure out how to build it, then build a self-sustaining worth machine called a business for it. 