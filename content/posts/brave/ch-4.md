---
title: "Ch 4"
date: 2021-01-09T21:07:53-05:00
author: "Aaron Bell"
draft: true
---


Intro
- Vampire Diaries intro
- Same Q's to Cloj core funcs
- (Whole explanation) Will learn to program to abstractions, seq, and lazy seq
- Work with implementations of the seq abstractions with fifteen funcs
- Explanation of the exercise

------

Programming to Abstractions
- elisp implemented on a concrete thing and requires different functions for mapping over lists vs. hashes
- map and reduce work with the seqence abstraction and therefore the first, rest, and cons funcs
- abstractions are named collections of operations. If an object can do it, then it is an instance of the abstraction
- Map only cares that it can perform a seq of ops on it.

Lists, Vectors, Sets, Maps as Sequences
- map is like exchanging the domain for the range in math
- sequence, a set of elements with clear before and after
- lists, vectors, most (all?) concrete data structures are treated as a sequence--doesn't matter what it actually is
- If the core sequence works on the data structure, it implements it
- Example of titlizing C4TBAT w/ four seemingly diff data structures
- Map works with everything; minor caveat with maps (`second` required)

first, rest, cons
- He's gonna build first, rest, cons, and map in JS with 'em. 
- All Clojure asks is, "can I `first`, `rest`, and `cons` it?". 
- X: JS implementation of a linked-list node
- Next and value
- Graphic rep of what looks like cons's
- cons is an operation, not a cons cell... map, reduce, and filter use these ops to work with each of 'em.
- Potentially confusing first, rest, and cons implementation
- X: JS implementation of map
- XoX: recursive implemention of the map
- map's implemented the abstract seq structure, so anything that conforms to it, map can work with.
- ? X: Implementation of the array on the map function. It changes external functions--so that what the map uses differs. 
- XoX: If I implement first, rest, and cons, I get map for free. (deg of freedom).

Abstraction through Indirection
- Clojure uses two forms of indirection; indirection means one name, different meanings.
- Polymorphism, like multiple-arity funcs, means dispatching to different function bodies based on the type of arg supplied
- Can work with polymorphism using Java tools or Clojure tools. 
- Clojure often abstracts data sequents by invoking seq on it, (seq [your-thing])
- X: of invoking seq
- (hi) The seq of a map consists two-element, key-value vectors. 
- Can convert a `seq`uence `into` a map.
- If a data structure can use the seq abstraction, it can use the seq library
- Ends over means.
-----------

Seq Function Examples

Clojure's seq library has many often-used functions

map
- Will show it using multilple collections and then functions as args
- Can give map multiple collections
- X: ^
- An element from each collection will be passed as an argument to the func
- (hey*)unify-diet-data example of mapping two collections
- ! Can pass map a collection of functions. Can apply this to 4Clojure.
- X: Can use `(map [key-word] [map d.s.])

reduce
- Will show non-obvious uses
- X: Can update map values with reduce; `assoc`
- XoX: Reduce treats the map as a sequence of key-value vectors.
- assoc assoc
- assoc Accociate's the given key to the given value
- reduce can filter out keys from a map based on a value
- reduce is more versatile than appears; compine reduce with map for jaw-dropping results; also combine filter with some

take, drop, take-while, and drop-while
- take and drop are inverses of each other; take takes the first n items; drop returns the sequence with the first n items dropped
- take-while, drop-while; takes or drops until the function returns true
- X: food journal example
- `[blah]-while` traverses the sequence, applying the pred function to each element.
- Diff between take and drop is it drops or takes until it is true then returns what's there.
- Can get a range of elements using take-while and drop-while

filter and some
- filter works where take-while and drop-while does; except that filter processes all of the data--less efficient.
- some returns the first truthy value of the predicate function against a collection
- Can use `and` to recognize that it's true and return the value

sort and sort-by
- Basic use, abc/123, use `sort`; else use `sort-by` with a function.
- Two x's and two XoX's of this

concat
- Appends members of one sequence to the end (? always the end) of another.

# Lazy Seqs

Probably using thunks, Clojure doesn't compute until I attempt to access the result--"Realizing the seq"

## Demonstrating Lazy Seq Eff

Needle-haystack, vampire-city example--"McFishwich"
Vampmatic 3000 computifier :) 
X: that takes ssn's, behaviors, and props.
XoX: Accessing a record takes ~1s
Can use the time function to see how long it takes to run stuff.
(Can I change the output of time to a log? Probably)
time outputs the elapsed time and return value
("side-effect function"?)
non-lazy'd take one million seconds or twelve days; lazy can take that much time if it's the last suspect... (plague game)
map didn't apply the vamp-rel-deets fn to the var at assignment.
It only computes when I try to access parts of it.
Once I try to use it, it will incur the one-second/lookup cost.
X: using time
XoX: It takes thirty-one seconds longer than desired...
Because it almost always results in better performance, Clojure gets the next thirty-one things as well
Subsequent accessing of the same lazy-seq will be near-instantaneous
x: ^

## Infinite Sequences

Some functions offer inf sequences, like repeat
x: Batman
XoX: nostalgia
Repeatedly invokes a function and returns its argument
x: Rand num
XoX: repeatedly takes in an ann func that returns a rand int between 0 and 10
If the seq keeps providing elements, it'll keep taking 'em.
x: even-numbers that uses recursion with cons to appends to the list...
XoX: ^
x: consing
lazy uses a function for the rest, instead of a literal rest value

#Collection Abstraction

All Clojure's core data structures take part in both the seq and collection abstractions.
Collections are about the whole, whereas sequences are about the parts. 
x: count, empty?, and every? 
Useful to know, but not consciously brought up.

## into

Many seq functions return a seq; into lets me restore it back into its orig. structure.
x: identity... sunlight-reaction
XoX: map -> seq -> into -> {} -> {bwah}
x: It works with vectors, too
x: Can convert a vector into a set
First arg of into doesn't have to be empty
x: ? Add elements into a map; add list elements into a vector;
into can take two collections of the same type---adds all of the items from the second to the first

## conj

adds elements into a collection in a diff way
x: Where it adds the entire vector 
x: Where it adds the members to into
conj passes it autistically into the collection; usually use many scalars
Can conj as many elements/collections as I want
x: Because of how seq functions break collections down into sequences, use a vector with a key-value pair, a tuple, to add a key-value pair to a map
conj and into are similar
x: conj function in terms of `into`; using a rest parameter.

# Function Functions

Clojure can take functions as arguments and return functions
`apply` and `partial` both accept and return functions.

##apply

apply explodes a seqable data structure to put into a function expecting a rest param
x: max, max with vector, 
returns the vector
`max` with apply works
Use `apply` often to explode a sequence into separate args for otherfuncs
Uses `apply` to define `into` in terms of `conj`

## partial

Takes my last function and supplies its and additional arguments
x: Add10, conj later
x: Personal creation of partial
x: Can assign the function returned from a partial to a value; create a sequence of things that will be added together...
Use partials when I use the same comb of functions and args often.
x: logger ex

## complement

Not vampire joke.
x: Find humans filter; not vampire
Wanting the complement/negation, that there's a boolean.
x: complement of vampire
x: Hand-coded implementation of complement
complement makes it easy to get the opposite--and it's seminal
Lots of little utilities that build up to a complex utility

## Vampire Data Analysis, FWPD

Going to parse a CSV with filters for each suspect's glitter intdex :) 
A(ction): Create a new lein app
A: Create a CSV file with vamp data.
Build up the core.clj file and start a new REPL session
Var to filename
Sanity check; make sure I can slurp the CSV.
(So you can put > and < in your function names)
x: Code for str to integers.
XoX: Will get a sequence of maps; 
 - vamp-keys, a vector of keys to create the map
 - "associates a convesion function w/ each key"
 - identity returns what's passed into it
 - The CSV gives text, so a str->int function's used
 - x: convert takes a vamp key and value and returns the glitter index
x: parse function; claims to turn a CSV into a row of columns
XoX: Takes a string; splits it by the comma; these two actions are performed on the str split by new lines. 
x: Parses the slurped csv. 
x: mapify; takes the seq of vectors and combines it with vamp keys to create maps
XoX: mapify; map transforms each row into a map; uses reduce; Creates a seq of key-value pairs; reduce builds it into a bigger map; results in row-map;
x: first result of mapify
x: glitter-filter function; gets records greater than the minimum
x: Implementation of glitter-filter
I'm a supernatural-creature hunting badass.

# Summary

Clojure emphasizes programming to abstractions
seq abstraction often deals with indy elements
seq functions often convert args to a seq and returns a lazy seq
Lazy eval improves performance by delaying computations until needed
Collection abstractions deal with data structures as a whole
Don't trust someone who sparkles in the sunlight :) 

# Exercises

Make Vamptronic better

 1. Make a list of names
 2. Make it so I can add someone to the list
 3. Validate func; only entries with name and glitter-index should be allowed
 4. Convert my map back to a csv. 

"versatility"

* @hey. Going back to reviewing for each entry. 