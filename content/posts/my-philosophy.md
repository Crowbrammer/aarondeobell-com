---
title: "My Philosophy"
date: 2020-11-08T03:14:47-05:00
author: "Aaron Bell"
draft: false
---

Here're my "why"s:

 0. If right, do the harder thing.
 1. Be amazing. 
 2. Achieve asynchronously. 
 3. Always be questioning. 
 4. Start and continue with greater and greater speed until it is done. 
 5. Learn and teach as much as possible. 
 6. Believe it can and must be done faster and better. 
 7. Appreciate existence. 
 8. Be punctual. 
 9. Be determined in my vision and flexible in my approach. 
 10. Be grateful for discontent. 
 11. Seek every advantage where "right". 
 12. Recover if needed. 
 13. Have fun. 
 14. Go into "blank" territory as much as possible. 
 15. Care deeply about people. 
 16. Be as useful as possible.
 17. Automate.
 18. Transmute sex emotion.
 19. Communicate.

Favorite question: What's a memory of being a 'high effort contributor and executor' of my philosophy.