---
title: "Use tighter exertion and recovery cycles in learning and achievement for faster and easier results"
date: 2023-02-13T18:04:06-05:00
author: "Aaron Bell"
draft: false
---

The cycles of recovery and exertion can be much smaller and become even more effective. 

I had the aha while doing pushups after a three-mile run where one of said miles was a five-minute mile. The workout consisted of about a hundred pushups in various ways (standard, wide, knee, box, Hindu), and after the first two sets of 12 to 16, I could only do two... at a time. Recalling David Goggins' injunction to Jesse Itzler in [Living with a SEAL](https://www.amazon.com/Living-SEAL-Training-Toughest-Planet-ebook/dp/B00U6DNZB2?asin=B00U6DNZB2&revisionId=c4a501c6&format=1&depth=1), "We're going to stay here until you do one hundred \[pullups\]", I said the same to myself about this chest workout. With this in mind, I give myself a minute or two to rest, and eek out another two or three pushups. Another minute goes by, and I try again: Another two pushups, etc. I did this until I completed the hundred-pushup workout.

I've done this mentally, too, when I solved 156 4Clojure programming problems. For this, two people influenced me: Isaac Asimov and Barbara Oakley. I recalled ["The Eureka Phenomenon" by Isaac Asimov](https://www.scribd.com/document/219978225/The-Eureka-Phenomenon-by-Isaac-Asimov) where he says, "It is my belief, you see, that thinking is a double phenomenon like breathing". He describes how, when further concentration yielded no effort, he'd go to a Bob Hope action movie where he'd eventually get the idea. I also recalled [Barbara Oakley's description](https://www.youtube.com/watch?v=1FvYJhpNvHY) of how going into the "diffused mode" of thinking allows the mind to reach farther to the depths of the mind - sometimes connecting a drastically different idea to the current subject. These two ideas gave me a strategy to complete the 156 4Clojure programming problems. I cycled between two minutes of concentration and two minutes of staring at the wall and started solving one problem after another over a period of two and a half months. Many of these problems were difficult and required research in "K-maps, minterms, Gray code, binary relations, bijections, set theory, and more". This strategy works! 

Going forward, I want to spend lots of time in this manner - focusing on a big goal while cycling back and forth between exertion and "doing nothing". Given my lessons, both mental and physical, I feel this is an effective and sustainable strategy to achieve many things. 