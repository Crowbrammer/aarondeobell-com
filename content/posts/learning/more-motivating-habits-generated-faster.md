---
title: "(Idea) From a big goal, recursively set sub goals until action is obvious, then apply the Fogg (Tiny Habits) model to that action."
date: 2022-04-21T01:54:33-04:00
author: "Aaron Bell"
draft: false
---

I've found a potentially superior means of rapidly generating quality (tiny) habits. The original method [I learned from B.J. Fogg](https://www.amazon.com/Tiny-Habits-Changes-Change-Everything/dp/0358003326) suggests two methods called behavior swarming and focus mapping to identify candidate habits to install. You start with an aspiration and then behavior swarm. You state an initial guess for an action then recursively follow that with "Great! What's another action" until you have about fifteen behaviors. You then "focus map" the habits (move it along two axes representing ease and motivation, one axis at a time) to identify the ease and motivation to do the habit. He calls the easiest, most motivating habits on the top right the "golden behaviors" and suggests installing these first. 

Having used this, I think I've found out how to generate more motivating habits than this traditional behavior swarming. To achieve most goals/aspirations requires the achievement of sub-goals. These sub-goals often require the achievement of more sub-goals, recursively for an arbitrary depth until action is obvious. Behaviors are low level and real world and are too detached from the biggest aspiration. There needs to be some low-level outcome the behavior can "jump" to mentally, to make it seem worth it. This gives a great chain of goals that the (thought of the) behavior can "ride up", causing greater motivation, likelihood of action, and therefor increased probability of achieving the aspiration. 

(Still testing this.)