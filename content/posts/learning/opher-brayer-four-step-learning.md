---
title: "Opher Brayer suggests pattern recognition, pattern design, analogy, and real-time composition for the maximum rate of learning"
date: 2022-04-21T01:13:28-04:00
author: "Aaron Bell"
draft: false
---

To achieve the maximum rate of progress, we need the maximum rate of variability in our practice (repeated action) to gain the maximum new information possible in a limited scope of time. [Opher Brayer teaches a "four-step" process to do this](https://www.youtube.com/watch?v=2GTZAg_G430), with most of the variability-maximizing done in step two. The steps:

 1. Pattern Recognition
 2. Pattern Design
 3. Analogy
 4. Real-time composition.

Here's how I interpret the steps:

 1. Id what you want to get good at
 2. Break it down into its parts 
 3. Put the parts in every possible permutation
 4. Practice all these permutations, perhaps multiple times
 5. Say "This is like..." to connect it to things you know, accelerating the rate of learning
 6. Use it

Perhaps use [Tiny Habits](https://www.amazon.com/Tiny-Habits-Changes-Change-Everything/dp/0358003326) to use this system consistently. Maybe use this system to generate more tiny habits 😁