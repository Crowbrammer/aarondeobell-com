---
title: "Subconscious consultation"
date: 2021-05-28T21:35:14-04:00
author: "Aaron Bell"
draft: false
---

Knowledge-acquisition mostly happens for me and likely others when I experience vivid visuals. The vivid visuals for me require both time (a thirty second timer is adequate) and space (gotta be undisturbed) where I can close my eyes and let them flowing in. I did this for this paragraph. When I closed my eyes before typing this paragraph, my imagination showed me a rolling ball, a man facing me closing his eyes in meditation, and a orange-gray mushroom cloud stemming from the top of his head. This image excited me, and I looked for the meaning. My meaning-making faculties interpreted this as: Start. Ask the question. Then set the thirty second timer and close the eyes and let the subconscious inform me. This information from the subconscious will avalanche through my life, causing an abundance of great ideas which I can organize and articulate. In other words, [image-stream](http://www.winwenger.com/imstream.htm) more.