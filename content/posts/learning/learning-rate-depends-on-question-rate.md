---
title: "Learning/writing rate depends on question rate"
date: 2021-09-30T23:19:01-04:00
author: "Aaron Bell"
draft: false
---

I have a rule to take 300 notes per paragraph but often stared at a blank screen. Today, while going number two, I got an insight to hypothesize "if I ask sixty supporting questions to the main question, I will take 300 notes more efficiently and in a more satisfying way." I tried this for this paragraph, and it proved true. To support main questions and topics going forward, I will ask and answer many supporting questions in the notes.  