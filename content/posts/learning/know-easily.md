---
title: "To know the unknown"
date: 2021-04-13T18:22:53-04:00
author: "Aaron Bell"
draft: false
---

Keep the known in focus as you look into the unknown. You need to make the new a part of the old; make the future integrate with the past. You need some cohesion between who you are who you will be. Keep both the known and unknown with you as you venture forward.