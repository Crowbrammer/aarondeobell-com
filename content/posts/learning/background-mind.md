---
title: "Until something shows up from the background mind, feed it more info."
date: 2021-10-20T09:32:30-04:00
author: "Aaron Bell"
draft: false
---

I should observe either the outside or the inside. If there's nothing inside from [the background mind](https://youtu.be/f84n5oFoZBc?t=1416), observe the outside. If there is something inside, write or act on it. Drafting a paragraph isn't a very conscious process. While it requires consciousness, it's giving words to something I see presented from the background mind, crystallizing its idea on the screen. Nothing from it? Instead of feeling frustrated: Feed it more. I should always understand if I should be in background-feeding or receiving mode. 