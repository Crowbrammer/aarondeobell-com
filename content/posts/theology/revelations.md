---
title: "My summary of Revelations"
date: 2021-04-13T17:14:32-04:00
author: "Aaron Bell"
draft: false
---

Jesus gives John a vision in Revelations. In the vision, Jesus chastises the seven churches of Asia. After John astral projects to throne room of Heaven, Jesus opens the seven seals, after which the angels sound the seven trumpets and devastate the land and ascend the repentant. After the vision, Jesus says that anyone may wash their robes of their sins, and they may join him in the Kingdom of Heaven. He encourages John to spread the word, and John concludes by warning you against twisting the words of Revelations, saying you'll experience the hell described within if you do. 