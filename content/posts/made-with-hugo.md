---
title: "I made this site with Hugo. Here's what I learned."
date: 2020-10-30T00:40:59-04:00
author: "Aaron Bell"
draft: false
---

Hugo's a static site ~~server~~ builder. From md files Hugo builds the html pages to serve through Nginx (or Apache). I didn't know this when I started. I read [the quickstart](https://gohugo.io/getting-started/quick-start/) where it says to start a server with `huge server -D`. I figured it just worked this way, so  after starting the server, I Nginx-proxy_passed to it. Doing this got the homepage right, but when using the server, it set the links to `localhost:1313/.../...`  and wouldn't load the pages. It ignored the config file because it created temporary files for local testing. After reading on [its basic usage](https://gohugo.io/getting-started/usage/#the-hugo-command), I realized Hugo's not for serving. It's for building things that serves. It's like TS, Gulp, Webpack, Slim, or Sass, but for markdown files. Learning this, I ran `hugo` at the blog directory, which created a `public` directory with interlinking html files. I pointed Nginx to this, and my site loaded properly, with links loading properly, i.e. to `aarondeobell.com`.



