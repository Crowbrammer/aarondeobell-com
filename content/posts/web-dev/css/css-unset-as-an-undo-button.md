---
title: "I see CSS's `unset` as an undo button (CSS #0)"
date: 2021-04-02T11:00:41-04:00
author: "Aaron Bell"
draft: false
---

Three CSS lessons. One theoretical, two practical.

## Value categories: rough to uniform

There are five different value "categories" that describe values from "roughest form" to "cleanest form, ready to convert to binary". The value types are: "declared", "cascaded", "computed", "used", and "actual". 

 - A ["declared" value](https://www.w3.org/TR/css-cascade-4/#declared) is what you declare in the style sheet; 
 - a ["cascaded" value](https://www.w3.org/TR/css-cascade-4/#cascaded) is the last of all the declared values, overriding the preceding values; 
 - a ["specified" value](https://www.w3.org/TR/css-cascade-4/#specified) is the most specific of the declared values, overriding even cascaded value;
 - a ["computed" value](https://www.w3.org/TR/css-cascade-4/#computed) is the first round of conforming values to one unit; 
 - a ["used" value](https://www.w3.org/TR/css-cascade-4/#used) is the second round of conforming values, taking more of the page context into account; 
 - and then there's the ["actual" value](https://www.w3.org/TR/css-cascade-4/#actual) which is the third round of conforming, taking things like the font into account. 
 
These last three could've been more aptly named to "computed-no-context", "computed-most-context", and "computed-full-context" value. 

## `unset`: the undo button

Of the four inheritance properties... `initial`, `inherit`, `unset`, and `revert`... I see `unset` as an "undo" button. Let's say you didn't do anything to `border-color`. It's default value is `initial`. And lets say you didn't do anything to `font-size`. It's default value is `unset`. Now you have a class called "unset". In this, it applies `unset` to both the `border-color` and `font-size`. You know what it'll do? Change it to the default values. What if set it from `unset` to `initial`? The `font-size` would match the default: 16em, however big that is in pixels. The `border-color`'d be the same. What if you set it to `unset` to `inherit`? The `font-size`'d be the default (pulls from its ancestor), but the `border-color`'d do something different, it'd pull from its ancestor.

![the border matches the color property!](https://dl.dropboxusercontent.com/s/cqcor4unu9b4h3l/2021-04-02_10-11-21.gif)

## `border-color` defaults to matching the `color` property

Fun fact, the default of the `border-color`, `initial`, is to match the `color` property. I found this out when I read this in [an example](https://codepen.io/geoffgraham/pen/BaoevoO) from CSS-Tricks. "The border will become black because, again, that's what the initial value is" [(Omodior, 2020)](https://css-tricks.com/computed-values-more-than-meets-the-eye/). I tested it in Codepen. When I saw this wasn't the case, I replied in [an annotation](https://hyp.is/WCOUIpO7Eeu-VW_0MhZh6A/css-tricks.com/computed-values-more-than-meets-the-eye/): "Not true! The initial value of [the border matches the color property!](https://dl.dropboxusercontent.com/s/cqcor4unu9b4h3l/2021-04-02_10-11-21.gif). I changed the color to green, red, indigo, blue, and the border-color [always matched in MS Edge v. 89, Chrome, and Firefox](https://dl.dropboxusercontent.com/s/di9bv2ewebew3sh/2021-04-02_10-15-45.gif)." Nobody thought to change the color property after messing with a descendant's border-color.

![always matched in MS Edge v. 89, Chrome, and Firefox](https://dl.dropboxusercontent.com/s/di9bv2ewebew3sh/2021-04-02_10-15-45.gif)