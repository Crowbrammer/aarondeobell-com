---
title: "How I understand CSS transform (CSS #1)"
date: 2021-04-03T03:41:34-04:00
author: "Aaron Bell"
draft: true
tags: [CSS]
---

A perfect analogy for the transform declaration in CSS.


----

{{< youtube LrxjYfl05ek >}}

-----

In a single declaration (not separately, where transforms are "cascaded over")`transform` composes, or "multiplies", a series of transform functions. "Multiplication" is a close analogy, but there's a difference between transform and straight math. In math, there's the commutative property: a x b = b x a. This is not so with CSS transforms. You can see this when rotating on different axes. 

![Non-commutative example](https://dl.dropboxusercontent.com/s/28svt6v56po7czz/2021-04-03_03-17-37.gif )

Like a conveyor belt, transform pulls in your functions, puts the element through it. This allows for repeated transform functions. You can have three scale(1.2), and it'll expand your element from 1x to 1.2x to 1.44x to 1.728x the size. `transform: scale(1.2) scale(1.2)` is equivalent to `transform: scale(1.44)`. 

![Two scales vs. one equivalent one](https://dl.dropboxusercontent.com/s/1j52fq3uaooni3n/2021-04-03_03-03-06.gif )


`transform: scale(1.2) scale(1.2) scale(1.2)` is equivalent to `transform: scale(1.728)`. `transform: scale(1.2) scale(1.2) scale(1.2)` is equivalent to `transform: scale(1.728)`. 

![Three scales vs. one equivalent one](https://dl.dropboxusercontent.com/s/1j52fq3uaooni3n/2021-04-03_03-03-06.gif)

`transform` goes something like this for your element.  I'll use JS syntax:

  - E(t1, t2, t3) -> t1(E)
  - t1(E(t2, t3)) -> t2(t1(E(t3)))
  - t2(t1(E(t3))) -> t3(t2(t1(E)))

At each stage, it's as if it's a fresh new element. This lets you build up a cool effect, or, in my case, an abomination.  

![My abomination](https://dl.dropboxusercontent.com/s/0l12h7cywo0nzon/2021-04-03_03-29-16.gif)

You have element #0 with three transforms in a declaration. The CSS renderer appears to apply the first transform to the element. You now have element #1 with two transforms left. Apply the second transform, get element #2 with one transform left. Apply the third transform, and you get the final result on element #3. 