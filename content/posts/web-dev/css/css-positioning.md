---
title: "CSS Positioning (CSS #1)"
date: 2021-04-09T03:51:16-04:00
author: "Aaron Bell"
draft: false
---

Absent styling, the renderer has default rules for where to place items on the page. The flow results from following these default rules. After the flow is established(?), the `position` value helps determine an elements relationship to the flow. CSS's `position` declaration affects something called "flow". 

 - `static` could be read as "with flow". It's the default. If there's no `position` property, it's `static`
 - `relative` means, "preserve flow, but move"
 - `fixed` means "leave flow, work with the viewport (screen)"
 - `sticky` means "stay in flow, turn to fixed when it would leave the screen"
- `absolute` means "leave flow, move based on another element"

CSS has what I call "turtle" declarations. After you decide a non-static position value (because you can't move it and have it remain in flow), you can use `top`, `right`, `down`, and `left` properties, the TRDL or "turtle" properties. 