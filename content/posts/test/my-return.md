---
title: "I have returned :)"
date: 2022-12-28T16:37:51-05:00
author: "Aaron Bell"
draft: true
---

It's been a while, but I have my blogging setup back. I use GitLab and its CI/CD with some Git/SSH magic to work with Hugo to publish new articles. 

Since my last post, the author of the Tale theme that I use for this blog updated it to enable dark mode, which I like :) 