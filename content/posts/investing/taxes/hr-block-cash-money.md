---
title: "H&R Block is ass for crypto, but they got me a $1,390 refund"
date: 2022-02-20T19:53:54-05:00
author: "Aaron Bell"
draft: false
---

I filed with H&R Block for a $1,390 refund. Their crypto reporting is _ass_, but I made it doable by aggregating my 377 RH trades (1099) and the 45 buys/sells from Binance into three big trades for DOGE, BNB, and LTC. This turned out to be the right move, because whatever I missed from HIFO optimization, I got back from a Homestead Credit that I would have missed. It brought me up from owing Michigan $195 to a refund of $695. (I entered twelve months of rent at $675/mo and it lifted my state tax situation from the muck). It also had last year's annual gross income on hand, which the IRS requires for e-filing, so that was a win for convenience, too. Now I wonder--is this too good to be true?