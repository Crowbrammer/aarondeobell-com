---
title: "Dogecoin lol (lay of the land)"
date: 2021-05-03T15:12:31-04:00
author: "Aaron Bell"
draft: false
---

Here's my view of the #Dogecoin world. BTC's strong. Elon's SNLing it up on May 8th. Budweiser, Slim Jim, Snickers, Milky Way tweeted with the Doge logo on their product. Patrick merged a Xanimo pull request for the Dogecoin Rosetta API today. Doge is at $0.41 and trending up.