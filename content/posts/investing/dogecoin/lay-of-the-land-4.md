---
title: "Dogecoin lay of the land #4"
date: 2021-06-06T22:43:32-04:00
author: "Aaron Bell"
draft: false
---

Here's a my glance at the crypto world, the Dogecoin "lay of the land" (lol):

 - Four minor updates to v.1.21 on June 5 and 6. (1)
 - Vitalik Buterin says Ethereum is dog money in its own way.
"Ethereum is itself in spirit, at least a little bit in spirit, itself a dog coin... I bought a bunch of Doge and still hold a bunch of Doge...". He offers his thoughts on optimizing Dogecoin and ways Ethereum can help. (2)
 - To attract and keep money in the U.S., two U.S. officials at Bitcoin 2021 want to support crypto. (3)
 - Coindesk consults Jeffrey Wang of Amber Group on the crypto drop and asks if drop due to Elon. He responds it's largerly regulation driven with Elon's tweet a cherry on top (my paraphrase), a short-term downward vector. (4)
 - Matt Wallace suggests the #WeLoveYouElon trend (5). It trends, and Elon reacts with double-heart emoji (6).
 - Iiuc, the people at the crypto trading platform, Bitmex, bought a payload from space infrastructure company, Astrobotic (7). Billy says the doges already secured a flight in 2017 (8). We'll be going together with Bitcoin 😄 
 - @maxotg_ leaps to the stage and says "Dogecoin to the moon!" at Bitcoin 2021 in Miami. (9) (10)
 - The Twitter account, @BscAnonymous, says that the Elon Anonymous video wasn't theirs. (10) (Not sure if they represent Anonymous, but I sense gravitas from their Twitter account with 13.8K followers) (11).
 - Crypto market broke day-and-a-half-plus resistance (12).
 - Volume's over five-ex on the upswing for Doge. 

Sources:

1.  https://github.com/dogecoin/dogecoin/commits/1.21-dev
2.  https://youtu.be/XW0QZmtbjvs?t=6788
3.  https://bitcoinist.com/warren-davidson-at-bitcoin-2021-the-fed-is-dogecoin-ing-the-us-dollar/
4.  https://www.coindesk.com/video/did-elon-musks-bitcoin-breakup-emoji-spook-the-markets
5.  https://dl.dropboxusercontent.com/s/l10yqwk9i6w11nk/2021-06-06_22-36-12.png
6.  https://twitter.com/elonmusk/status/1401719876515946498
7.  https://blog.bitmex.com/bitcoin-on-the-moon-courtesy-of-bitmex
8.  https://twitter.com/astrobotic/status/1390786901402079232
9.  https://youtu.be/QAiWmBbru6s?t=8
10. https://twitter.com/maxotg_/status/1401282728256016385
11. https://twitter.com/BscAnon/status/1401265020554539010
12. https://dl.dropboxusercontent.com/s/bye2m050m4iibbx/msedge_2021-06-06_22-40-57.png