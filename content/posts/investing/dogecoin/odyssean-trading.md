---
title: "I attempted to \"Odyssean trade\""
date: 2021-05-21T05:56:17-04:00
author: "Aaron Bell"
draft: false
---

I tried setting things up so that I can't trade, so that I can't paperhands. That is, I tried making it so that I couldn't access the Robinhood account, so that I was a hodler whether I wanted to or not. To this end, I needed to make it so that I can't use the password, use the email to reset the password, or use the phone to reset the password. I started changing passwords and emails, and I started getting somewhere, but I didn't want to permanently lock myself out, so I needed a way to give me an in sometime in the future. After a few moments and Google searches, I figured out a way to do it with FutureMe; however, by the time I figured everything out, I got insight on how to trade better, so I won't do this just yet 😮