---
title: "Dogecoin lay of the land #3"
date: 2021-06-02T19:28:14-04:00
author: "Aaron Bell"
draft: false
---

Here's my Dogecoin lay of the land.

New:
 - [Ross Nicoll committed fourteen commits between May 29th and June 2nd](https://github.com/dogecoin/dogecoin/commits/1.21-dev). Iiuc, one commit makes difficulty 5.64 times easier, meaning reduced block times. 
 - [We have official Coinbase Pro trading at noon tomorrow](https://blog.coinbase.com/dogecoin-doge-is-launching-on-coinbase-pro-1d73bf66dd9d). Traditional Coinbase trading'll likely follow. 
 - [Bitcoin Miami conference](https://b.tc/conference/) will star Jack Dorsey, Ron Paul, Tony Hawk, and a senator

Reminders:
 - [DOGE-1 mission](https://twitter.com/elonmusk/status/1391523807148527620). 
 - [RH wallets still to come](https://decrypt.co/71918/robinhood-will-soon-let-you-move-your-dogecoin-says-crypto-coo). 
 - [Business adoption (Tesla)](https://twitter.com/elonmusk/status/1392030108274159619?lang=en). 

In the coming days, I see Doge trading up a bit, correcting, and then ranging right pending said catalysts. 