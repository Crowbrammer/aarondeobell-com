---
title: "Parents' money"
date: 2021-05-10T15:05:03-04:00
author: "Aaron Bell"
draft: false
---

I lost some money. I invested $3k of my parent's money at $0.57. I saw a bearish head-and-shoulders pattern in the chart, so I pulled out at $0.54. It dipped to $0.51 before rising to $0.545 (it's a bear pennant). I felt tired and was about to go to bed, but, wanting security, I put Steven Steele's YT livestream on, and he gave enthusiastic, bullish recommendations about the long-term of Doge. I didn't want to forego the hyper-rise for a small fall, so I put the buy order back in at $0.54. I woke up to the shorters having a payday at $0.44. I fucked up in the short-term, not listening to the signals. We're testing long-term support though, and if the market accepts it, we've got a long-term upswing ahead. 