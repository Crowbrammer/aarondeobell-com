---
title: "Reallocating $"
date: 2021-06-23T16:27:13-04:00
author: "Aaron Bell"
draft: false
---

To protect myself from Dogecoin's +38% and -38% variations, I changed my holdings from ~97% Dogecoin (27k Doge) to ~30% Dogecoin (7k Doge), assets, etc. I successfully told the group that I reallocated, without getting crucified. I told the truth, and now my conscience is cleared. I'm presenting myself authentically, so I'm doing okay. Also, Nick Ballz, a Doge millionaire who I think is cool, remained my follower. That makes me happy 😊
