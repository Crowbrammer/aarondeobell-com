---
title: "Dogecoin lay of the land (lol) 2"
date: 2021-05-29T00:38:02-04:00
author: "Aaron Bell"
draft: false
---

Here's my quick lay of the land:

 - Ross Nicoll [committed four changes yesterday](https://github.com/dogecoin/dogecoin/commits/1.21-dev) on message headers, segwits, checkpoints, and seeds. No fundamental changes to the code but parameter values changed.
 - [Matt](https://twitter.com/MattWallace888), [Nick Ballz](https://twitter.com/NickBalazs), [itsALLrisky](https://twitter.com/MattWallace888), and [Slim Jim](https://twitter.com/SlimJim/status/1398443969239998470) remain strong for Dogecoin.
 - Barry called for a weekend BTC ceasefire.
 - Doge and BTC suspended in their current values at around $0.31 and $36.3k.
 - Memorial Day weekend incoming 😊
 - AMC share count on the 2nd; AMC down ~28% from ATH yesterday .
 - GME's also down ~16.8% from high this morning. 

On the last two, certain buying opportunities are like tomatoes and spiders. Gotta get the tomato before the spiders crawl in. After a certain point, it still looks delicious, but you take a bite and they swarm all over you. 