---
title: "Charting patterns I recognize"
date: 2021-05-03T18:56:43-04:00
author: "Aaron Bell"
draft: false
---

I recognize two of [the five candlestick patterns](https://www.investopedia.com/articles/active-trading/092315/5-most-powerful-candlestick-patterns.asp) and [the four continuation patterns](https://www.investopedia.com/terms/c/continuationpattern.asp) that give indications on impending price changes. I remember two candlestick patterns: the bearish double black gap, the bullish abandoned baby pattern. These are helpful to ID the next three to five sticks of price movement. For the continuation patterns, I don't understand the difference between a symmetrical triangle and a pennant. A pennant differs from a flag in that a pennant is triangular and a flag is rectangular with parallel lines. A rectangle is a sideways trading flag. These help to id entry and exit points, usually based on the pattern's height.