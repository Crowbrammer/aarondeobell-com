---
title: "Needed trading improvements"
date: 2021-05-21T04:05:19-04:00
author: "Aaron Bell"
draft: true
---

For trading, I don't know limit order pruning, patterns, and I lack some math habits/hygiene. I find myself straining for the price to go up when I have a limit sell, instead of looking at volume and seeing what's likely. I should just review the volume and news and see if anything's moving the price. For patterns, there are things that seem like, say, a bull flag, but are not. It's like the difference between lightning and a lightning bug: I need to learn to read it. Outside of this, I should also review Binance orders, volume, Bollinger bands, MA, and all at different timeframes. Bottom line is I need better protocols, habits, and checklists to trade successfully. 