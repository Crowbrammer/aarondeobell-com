---
title: "Better trendlining"
date: 2021-05-03T18:19:51-04:00
author: "Aaron Bell"
draft: false
---

Someone criticized my use of trendlines, saying that the price swung between the two trendlines, so only indicators should be used, like the MACD. Fundamental factors sway the price, so price won't always bounce perfectly between predefined support and resistance, but it gives a field where we're able to predict the price and save our investments and capitalize on upward trends. After studying trendlining though, I saw that the wick does reflect past asset prices, so I realized you're supposed to count the wick, so I will start trendlining against the wick ends. I fear that I'll draw the trendlines incorrectly, but I think I know enough to draw them right enough. I should brace for being wrong and still put what I see out there, as it has served me well enough. Trendlining is an imperfect practice, and I think my trendlining is perfect enough to help me make profitable enough decisions to pay my rent. 