---
title: "How I see support and resistance (technical analysis)"
date: 2021-04-30T12:57:59-04:00
author: "Aaron Bell"
draft: false
---

[Support and resistance](https://www.investopedia.com/trading/support-and-resistance-basics/) are the two prices that the market tends to bounce between, and a breach of these two prices means that something fundamental's changed. For example, things were fine and dandy for Dogecoin (and the crypto market as a whole) with Dogecoin at ~$0.35 until someone leaked Biden's doubled capital gains tax law, and people sold like mad causing a free fall in the crypto price; the price just went down and down. Did the market reject support? Yes! Even if you didn't know about the capital gains tax leak (which helped to know), if you plotted support trendlines and sold when it broke past this, you would've protected yourself against that -30% dip in price and saved a lot of money. I think. Still working out the math with the short-term capital gains tax vs. the long-term. 
