---
title: "Elliot Wave Theory"
date: 2021-05-17T17:39:52-04:00
author: "Aaron Bell"
draft: false
---

To use [EW Theory](https://www.investopedia.com/terms/e/elliottwavetheory.asp), you should know what motivated people to use it, what impulse and corrective waves are, and that different timeframes should be considered. People took notice of Elliot Wave Theory when Ralph Nelson Elliot identified a stock market bottom in 1935. Elliot Wave Theory identifies [impulse waves](https://www.investopedia.com/terms/i/impulsewave.asp) and [corrective waves](https://www.investopedia.com/terms/c/corrective-waves.asp) in 5-3-5-3 patterns at different timeframes. Once you've gone up three impulsive waves with two corrective waves between, you should see the market go down two larger corrective waves with a small impulse wave in between before restarting the cycle. If it continues down before exceeding the previous corrective wave, there is likely a true [reversal](https://www.investopedia.com/terms/r/reversal.asp) instead of a correction. People say that seeing at a higher timeframe can help you identify the current wave directions at a lower time frame.