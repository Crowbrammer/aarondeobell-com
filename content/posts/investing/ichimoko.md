---
title: "Ichimoko"
date: 2021-05-28T01:42:21-04:00
author: "Aaron Bell"
draft: false
---

Five indicators comprise [the Ichimoko Kinko Hyo "one glance cloud chart"](https://medium.com/@tradersmokey/a-comprehensive-guide-to-ichimoku-kinko-hyo-e5ed286c3258). You have the two moving averages: Kijun-Sen for long-term and Tenkan-Sen for the short-term. Like all MA indicators, the short-term crossing the long-term indicates a buy or sell signal.  After this are the two Senkou spans and the cloud the outline. The first Senkou span's based on the KS and TS; the second averages the highest high added to the lowest low. The type of cloud (SSA > SSB or vice versa) predicts the price. 