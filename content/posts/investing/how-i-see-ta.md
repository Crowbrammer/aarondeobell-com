---
title: "How I see technical analysis"
date: 2021-05-03T13:21:47-04:00
author: "Aaron Bell"
draft: false
---

To predict future prices and make profitable buying and selling decisions, traders use chart patterns (including trendlines revealing support and resistance) and chart-plotted mathematical equations called indicators to technically analyze asset prices, to use the past to predict the future. Chart patterns include trendlines ("walls") for "support" (where demand concentrates) and "resistance" (where supply concentrates) and silly-sounding patterns like "cup and handle" (price going up), "head and shoulders" (price going down), and "double bottom" (price going up). Mathematical indicators include the MACD and Bollinger bands. Iiuc, when a MA crosses the MACD (iiuc), that's an indication of a large price change. When the price reaches one of the Bollinger bands, the price'll tend to move opposite the band, esp. in a consolidation phase (no up or down trend). 