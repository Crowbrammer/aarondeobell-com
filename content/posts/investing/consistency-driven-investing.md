---
title: "(Maybe) reduce risk, cognitive load, and increase profit by dollar-cost averaging into index funds"
date: 2022-02-21T22:02:42-05:00
author: "Aaron Bell"
draft: false
---

Maybe I can improve my performance and reduce my agony in market research by reducing variation in multiple ways for when (DCA/VA) and what I buy and sell (indexing). [Dollar-cost averaging](https://www.investopedia.com/terms/d/dollarcostaveraging.asp) (DCA) is investing consistent amounts at consistent periods regardless of the market. This prevents me from timing myself out of the market and gives me consistent buys at hopefully better and better prices. [Value averaging](https://www.investopedia.com/terms/v/value_averaging.asp) (VA) is consistent buys but with higher amounts during lower prices. [Indexing](http://localhost:63342/h/index.html?_ijt=kpun1nmq6ude9fka0dgdc5bk4n&_ij_reload=RELOAD_ON_SAVE) is spreading my funds to many different sources to match the risk and reward of the entire market, usually for a consistent 7% back. Though this sounds boring, the important thing is that I always make and never lose money. Not losing money, esp. to inflation, is a huge achievement. 