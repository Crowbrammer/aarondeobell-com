---
title: "Dogecoin or Tesla"
date: 2021-03-23T06:35:55-04:00
author: "Aaron Bell"
draft: false
---

If I invest in Dogecoin, I could lose it all. There's almost [everything that I don't know about blockchain](https://www.youtube.com/watch?v=EH6vE97qIP4). [SEC regulations could mess things up](https://financefeeds.com/judge-dropped-bombshells-sec-vs-ripple-top-lawyer-argues/) by treating it as a taxable(?) security (with stocks, bonds) instead of a currency. There's tax implications and price implications from tax implications if it's treated as a security and not a currency. Also, there are many cryptocurrencies, and people could drift entirely from Dogecoin. To buy this, I'd pull money from my Tesla shares: I lose Tesla. How high can Tesla go? They're unleashing FSD. What does that mean for them? How will that affect things? More car sales. Money from using it as an automated taxi. Higher revenue from people buying the FSD option. But if these factors are accounted for, and it still looks good, I think Dogecoin has the potential I need to get back in the game. I have $4K to invest right now. [Upshod claims he thinks it'll be at $0.40 by the end of the year](https://youtu.be/CNA0g0qcHLI?t=358). This would turn my $4K into $32K. Eventually, if it goes to $1, it'll turn it to $80K. Elon gives [qualified support for Doge](https://twitter.com/elonmusk/status/1361094185412100096), and a sort of self-fortune-telling process called [image-streaming](http://www.winwenger.com/imstream.htm) I did seems pro-Dogecoin (saw a vision of a Shiba Inu meme with upward-right lines).

Hmmm. 