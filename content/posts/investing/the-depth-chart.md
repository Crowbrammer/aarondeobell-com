---
title: "The depth chart"
date: 2021-04-21T13:55:19-04:00
author: "Aaron Bell"
draft: false
---

The depth chart describes recorded supply and demand. Two kinds of people called makers and takers make up the trading grounds. Makers are those who've made their supply or demand known: they've created limit sells or limit buys. The depth chart describes these limit sells and limit buys. New people called takers can come in and buy or sell at these limits, so what's available is constantly changing. Limit sells and limit buys will change constantly through new maker decisions and through taker decisions to buy or sell at these limits, so the depth chart is somewhat fluid and moving.  

The depth chart doesn't describe the hodlers--the people who will never sell--and the indifferent--the people who will never buy. Hodlers reduce the supply and means there's less to buy, which means people can charge for more it. The indifferent people serve the other end, to drive the price down. To make the price go up, you want to increase buyers and hodlers, and you want to reduce sellers and indifferent. I can hodl rocks all I want, and it won't drive the price up. I need to hodl the rocks and help people realize that there's something special in these rocks--my love--and people will switch from indifferent to wanting it, and the ability to sell will go up.

The trading environment is a consignment store where all the items are the same but with different price tags, and all the customers want the same thing but with different ideas of how much to pay for it. If the two don't agree, the customer's still in the store but nothing's bought. When the two agree, both the item and the customer leave the store. Also new people can come into the store at any time willing to buy or sell at the current best price. There's an unknown amount of people on their way to the store, and part of effective trading is predicting how many people will be at the store today. 
