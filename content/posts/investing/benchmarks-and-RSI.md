---
title: "Benchmarks and RSI"
date: 2021-05-04T21:49:13-04:00
author: "Aaron Bell"
draft: false
---

Groups of people group assets and provide ongoing metrics/statistics about them and call them indices (indexes) which are then reframed as "benchmarks" for other uses. These benchmarks can help you see how an asset's performing relative to everything else.  Like how S&P weights companies based on each's market cap, each index has its own method of measuring the asset class. You can use these benchmarks to measure investment health, create buy or sell signals (automated or manual), and measure investment risk. 

Relative strength indiactors, or RSIs, use benchmarks. To use RSIs properly, long-term trends should be considered. Momentum investors use relative strength indicators which use a benchmark like the S&P 500 index. This lets you create buy and sell signals, for example buy at 40%  RSI and sell at 70%. Do I understand this yet? No, but I do know that people recommend this threshold. At the bottom of a long-term bullish reversal, I intend to consider the 40% RSI signal for buying. At the top of a long-term bearish reversal, I intend to use the 70% RSI signal for selling. 