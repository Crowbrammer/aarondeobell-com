---
title: "Cloud charting"
date: 2021-05-20T11:51:14-04:00
author: "Aaron Bell"
draft: true
---

The [Ichimoko Kinko Hyo](https://medium.com/@tradersmokey/a-comprehensive-guide-to-ichimoku-kinko-hyo-e5ed286c3258) ("one glance cloud chart") has: two moving averages: the Kijun-Sen (long-term) and the Tenkan-Sen (short-term);  two Senkou Spans to form the boundaries of a signaling cloud; the Kumo (cloud) they delineate; and the Chikou which plots the Xth (30th) closing price behind (to keep perspective of past price vs. current price). The main signals are: Kijun-Sen and Tenkan-Sen crosses at the priceline away from the cloud; Senkou Span A higher than Senkou Span B and vice versa; a double top called a Kijun bounce; the Kumo or Cloud twist; I don't understand the C-Clamp yet; and there's more signals to come. I feel excited about this, and this will be my go-to tool for analysis in the future. 