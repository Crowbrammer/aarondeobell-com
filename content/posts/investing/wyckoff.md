---
title: "Wyckoff"
date: 2021-05-18T15:12:38-04:00
author: "Aaron Bell"
draft: false
---

[The Wyckoff method and schematics](https://school.stockcharts.com/doku.php?id=market_analysis:the_wyckoff_method) detail the process by which large institutions pull money from the masses. Large institutions buy and sell amongst themselves to create the appearance of a booming (bull) or reversing (bear) market to attract retail investors to buy at nearly just the wrong time, letting them pull in lots of money on a grand scale: Getting a little from each person--but also a little from a lot of people. The metaphor of the whale is appropriate here. The whale feeds on and survives off krill.

