---
title: "Implications of the 2019 Bitfinex Tether scandal on crypto prices"
date: 2021-05-04T21:34:18-04:00
author: "Aaron Bell"
draft: false
---

A member of a Discord I'm on, BrizzyVol, linked [a tweet by Jacob Kinge](https://twitter.com/JacobKinge/status/1389605324223565828) which referenced [a NY Supreme Court document on the Bitfinex Tether fraud](https://iapps.courts.state.ny.us/fbem/DocumentDisplayServlet?documentId=vIexA1b0spKOnK_PLUS_ZUGTJ3A==&system=prod) where the Bitfinex made a statement confirming the link between BTC and Tether. The 4/25/2019 SC document shows possibly corrupted areas that could undermine trust in the crypto system; someone thinks that Bitfinex has more Tether than the money they have, i.e. they have 75%(?) of the money they represent in Tether. Glitches in Tether affected BTC's price, so this has big implications if true across the crypto market.