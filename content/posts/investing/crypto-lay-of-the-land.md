---
title: "Crypto lay of the land (lol)"
date: 2021-04-26T21:15:43-04:00
author: "Aaron Bell"
draft: false
---

The capital gains tax and the Biden administration's potential new law affect the crypto market. Some people thought, "Oh, I just made a lot of money" and cashed out when their position got high. This usually would cause more taxes than if they would've held. The new CG law appears to double the taxes on gains, so it's possible that this was the right move and saved them a lot of money. 

The SEC affects the crypto market, too. [They're suing Ripple again](https://www.courtlistener.com/recap/gov.uscourts.nysd.551082/gov.uscourts.nysd.551082.132.0.pdf) (for the second, third, fourth time?), attempting to establish XRP as a security and not a currency. It's technically a currency, but awareness of how people treat it will make its status as a security obvious, giving the SEC a basis in common sense. I'd rather not see the SEC win, because I think that will flatten many crypto prices and affect me tax-wise, but they do have a leg to stand on. I'm crossing my fingers on this. 

The BTC price with 100% certainty affects the crypto market, too. The price of BTC nearly directly matches the price of whatever security you're following--assuming no other contradicting news. With it reliably trending upward, it's lifted everything else with it.

Then finally, some other important things are:

 - May 1st is Doge Dev Day
 - May 8th is Elon Hosts SNL Day
 - The Doge community appears to be rebuilding after the hit from pump and dumpers on 4/20