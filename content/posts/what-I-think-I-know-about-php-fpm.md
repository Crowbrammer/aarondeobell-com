---
title: "PHP FPM"
date: 2020-10-31T09:58:08-04:00
author: "Aaron Bell"
draft: false
---

PHP FPM\* has PHP running(?), waiting for requests at a Unix socket or at the localhost's port 9000\*\*. When the number of requests per moment gets past a certain amount, it creates(?) more PHP processes to delegate them . FPM conforms to the IETF-specced Common Gateway Interface, which parses the script and path info from the url and sends it to the PHP instance. PHP then creates the environment (`$_SERVER`) variables like SCRIPT_NAME(?) and PATH_INFO. It'll execute the script name. The script'll likely use the PATH_INFO. Nginx's fastcgi_pass sends the request with specific variables from the URI to PHP.

\* Run at `/usr/sbin/php-fpm<version_number>`, if installed. 

\*\* Set the listen location at `/etc/php/<version_num>/fpm/pool.d/www.conf`.