---
title: "My new job is to help people trade money for products (gas station clerk)"
date: 2021-07-12T23:31:17-04:00
author: "Aaron Bell"
draft: false
---

I got a job manning the gas station at midnight, usually from midnight to six. My job is to enable customers to switch items for money. The bottleneck to getting customers to exchange money for Meijer's items is my ability to ring items up. Everything else will be more or less handled, because stocking requires physical effort and following a checklist. Not worried about that, because elbow grease and raw human suffering'll make that work. The changing of money for items requires some practice, i.e. changing dollars for the right dollars and coins. I've been playing a game where I can better learn to do that.