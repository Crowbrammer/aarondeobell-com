---
title: "Annoyed at cat."
date: 2023-01-14T21:56:17-05:00
author: "Aaron Bell"
draft: 
---

I want my cat to have zero capability to annoy me if I choose for her not to. The cat meowed while I wrote notes earlier. Minutes later, she's pawing on my side. 

I haven't done yoga today, and yoga's my preventative maintenance for anger because my muscles get tense easily and tend to not relax, so I'm more irritable than normal. I push her away and tell her to stop.

When she paws on me again, I grab my earmuffs and chuck them at her. Because I had nothing soft to throw left, I had to further stop what I Was doing, get up, grab the earmuffs, and resume work - but mad.

Instead of the relaxed flow state I felt earlier, I feel mad as I write the notes for this. I also see a towel, and my brain's identified this as a non-lethal weapon to whack her with if she tries to paw her way up here again. 

That said, she's alone, and I appear to be here only source of love and entertainment here, so maybe I could get her a second cat to distract her, or maybe some toys.