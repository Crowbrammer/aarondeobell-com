---
title: "An unseen current you throw delicious ingredients to which returns delicous food"
date: 2022-03-30T16:14:43-04:00
author: "Aaron Bell"
draft: false
---

Cooking's like an unseen current that you throw delicious ingredients to, and the current curves it back to you like a magnet, pulling the ingredients back to you in a more delicious form. Ingredients flow from a magic black box called a store, to your home, to be mixed in various ways, to be put in hot places for a time, to be returned to a more presentable place, until you decide to eat, and it arrives into your tummy. 