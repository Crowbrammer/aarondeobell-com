---
title: "\"Cooking\" is shopping, prepping, waiting."
date: 2022-03-30T16:14:12-04:00
author: "Aaron Bell"
draft: false
---

 You could call some aspect of prep "cooking", but "cooking" is really putting things in the right place and letting nature do the work, so I put "cooking" into prep. The rest (the hard part) is waiting.