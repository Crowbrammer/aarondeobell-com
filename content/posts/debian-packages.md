---
title: "Debian Packages"
date: 2020-11-01T06:39:21-05:00
author: "Aaron Bell"
draft: false
---

Debian's(?) [APT requires from packages a structure of six documents plus the app tarball](https://wiki.debian.org/Packaging/Intro?action=show&redirect=IntroDebianPackaging#Step_3:_Add_the_Debian_packaging_files). APT looks for six files in the debian directory with the packages(?): changelog, compat, control, copyright, rules, source/format. The rules file is a [Makefile](https://opensource.com/article/18/8/what-how-makefile) (because makefiles are composed of rules that are essentially CL-runnable functions). It describes how to build the app(?).
