---
title: "Debian Repos"
date: 2020-11-01T10:12:08-05:00
author: "Aaron Bell"
draft: false
---

Debian repos are file directories with a `dists` folder containing directories representing suites. These suites have Release files. Sometimes `Release` + `Release.gpg`, else just an `InRelease`. `InRelease` contains the gpg key. `Release` doesn't, so `Release.gpg` must accompany it. Release files with the gpg keys confirm the trustworthiness of the distro holder. 

Package managers like APT require Release files (`Release` and `Release.gpg` OR `InRelease`) for each(?) distro. Checksums have the same output based on the input. Repo owners put checksums in the release files with the checksum functions used, like MD5Sum used with Ubuntu distributions. This lets my package manager put the files through the same checksum function and compare it to what the Release file says. If the value the package manager gets from the checksum of the file matches what the Release file says, it accepts it as safe and proceeds to use the rules(?) makefile with the distro to build the app(?). All this ensures data integrity and that you get what the repo owner intended you to get. [Here's an example of a release file](http://ch.archive.ubuntu.com/ubuntu/dists/bionic/Release) that uses the MD5Sum program to create and check 128-bit (i.e. long, hard-to-fake) checksums. Package managers (APT, APK(?)) will error if the checksum of a file mismatches the Release file's checksum.

In the suite directories, there are (can be?) several directories for different branches of given release which contain different binaries if it's a binary, or just the source code(?) if it's a source. APT refers to source packages with `deb-src` in `sources.list`. Source packages must be compiled before use. Binary packages are precompiled. Different CPUs can use different architectures. There's about a dozen Linux-supported architectures. Each has different requirements of a binary. i386 (32-bit) and amd64 (64-bit) are common architectures, so distros often have at two separate directories in suite of a distro containing binaries 'em.
