---
title: "Running a startup is about learning, and learning is the process of setting, measuring, guessing, and testing."
date: 2023-01-15T19:26:05-05:00
author: "Aaron Bell"
draft: false
---

Outcomes and metrics must be set. Without outcomes, where are you going? Without metrics, how will you know you've gotten there? Some outcomes are qualitative--based on the senses and oftentimes feeling, but with business, business success means quantifiable money success (at some point), so profitability, cost, lifetime value is important. 

Then, assumptions guide actions, and certain actions cause better effects on the metrics than others. What we want to do is weed out all the bad assumptions and replace them with super good assumptions that cause super good actions that cause super good things to the metrics. Identifying assumptions is called guessing or "setting a hypothesis". 

With these guesses, we can form tests--questions about whether the hypothesis is true--and by acting to answer these tests/questions, we get two things: 1) we know whether something improves the metrics or not, and 2) we maybe get an improved metric. The guessing, testing, and answering/acting gives two benefits over blind action, which _sometimes_ gets one. 

Bibliography

Ries, Eric. (2011.) The lean startup. Chapter 7: measure. https://www.amazon.com/dp/B09Y5GDMMR
