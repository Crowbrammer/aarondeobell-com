---
title: "Successful online businesses securely accept payment and fairly deliver a great service"
date: 2021-08-19T10:44:20-04:00
author: "Aaron Bell"
draft: false
---

Securely accepting payment and ensuring the app delivers a great service is the crux to a successful online business. I have business ideas, but I need to solve this problem for all of 'em. I'll process payments through [Stripe](https://stripe.com/) and [CoinPayments](https://www.coinpayments.net/home) to leverage their security. I'll then use the "payment successful" responses from their services to update the db and provide different tools based on the payment. But how to process refunds? Customer service? I'd like to automate that. (Just learned: Can offer refunds, [but the fee is not returned... ](https://dl.dropboxusercontent.com/s/va7eh1wx8y9l7wm/2021-08-19_10-48-12.png), as seen [from here](https://stripe.com/pricing)) I want clear policies for getting people their money back, and to have our income based on a sincere sense of value delivered, not strong-arming people with "sorry, we don't do refunds". 