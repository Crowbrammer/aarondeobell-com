---
title: "Success and implications"
date: 2021-05-11T23:32:17-04:00
author: "Aaron Bell"
draft: false
---

Success increases positive emotion and motivation making the habit easier to do, so successes, and esp. their implications on my life, should be dwelled on. Esp. when stuck, ask "What's a success?". Further, I've found that dwelling on all the good things that'll come from it multiplies the feeling of this focus. Once you've IDed the success, then ask "What are the implications?". This has filled me with a lot of positive emotion and helped me finish some hard tasks lately, so I wanted to share it. 

Life is full of us missing the mark. It's a consistently dark place, and at some point the mind and body'll shut down. If everything's an error, the only logical conclusion is to stop. So you need to feed it positive feedback so that it goes in the right direction. Once you have this positive feedback, you can amplify it by studying its consequences: "I succeeded at this, which will cause this good result and this good result and...!".
