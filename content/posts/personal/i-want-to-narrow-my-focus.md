---
title: "I want to narrow my focus"
date: 2021-07-13T12:43:47-04:00
author: "Aaron Bell"
draft: false
---

I want to drill deeper instead of drilling many shallow holes. With my personality ("enneagram seven"), I find this near impossible, as partway through drilling one hole, I see that another hole seems more promising. After all, how could I know everything about this hole before I started digging it? Now that I dug this hole several feet deep, I now know that a different hole seems more promising. But if the treasure's always at ten feet or deeper, and I keep on changing holes three feet in, I will never get the treasure. So even if the current treasure sucks, at least I'll get some treasure instead of searching for the next, better treasure. But how do I convince myself to do that? More concretely, my foci have ranged from crypto, programming (JavaScript and ClojureScript), fishing, crypto, gas station clerk duties. I'd like to narrow this (forget others) and focus on just programming and cooking.