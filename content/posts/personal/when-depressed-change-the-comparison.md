---
title: "When comparing myself to others, if I feel bad I should change the metric."
date: 2021-08-16T01:46:50-04:00
author: "Aaron Bell"
draft: false
---

When I feel pain from comparison, I should change the metric. Recently I've compared my intellect to my sister's somewhat blinding intellect and caused myself lots of grief. "Why bother?" came up often as she'd bring up her short learning curve while I recall how hard things came to me. Like a Naruto character, I felt consternation at my lack of intrinsic skill/ability. "Comparison is the thief of joy". This is almost true. What's truer is "[Certain comparisons are thieves] of joy". If I compared patience, care, and work ethic to some of these people, I'd feel a bit better about myself. I may not be a genius, but maybe I can be a genius at hard work and commitment. 