---
title: "Relax in tiny ways to relax bigly (ha)"
date: 2021-10-13T20:48:29-04:00
author: "Aaron Bell"
draft: false
---

While I've learned from [Psycho-Cybernetics](https://www.amazon.com/dp/B00SI02BW4) that relaxation is key years ago, it's only with B.J. Fogg's [Tiny Habits](https://www.amazon.com/Tiny-Habits-Changes-Change-Everything-ebook/dp/B07LC9KDP5) model ([video here](https://www.youtube.com/watch?v=AdKUJxjn-R8)) that I'm consciously relaxing more consistently. "After I lie down, I will see my head as cement, sinking so far through the mattress that it touches the floor". Just this, and not all the body parts, is my primary commitment. However, I felt inspired to continue after this initial commitment to relaxation. If it's anything like the "just the dish glove" thing, which gets me to do all the dishes after donning them, it should get me to relax my whole body more consistently. 