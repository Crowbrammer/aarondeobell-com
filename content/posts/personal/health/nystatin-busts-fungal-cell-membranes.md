---
title: "Nystatin busts fungal cell membranes"
date: 2022-04-11T17:19:23-04:00
author: "Aaron Bell"
draft: false
---

I have oral thrush (mouth fungus... gross). A nurse practitioner saw it and prescribed me a Nystatin "suspension" (liquid), which I've been taking four times daily. Learning about it, I see that Nystatin binds to the ergosterol in the fungi's cell membrane, causing holes which leads to potassium loss then acidification and fungal cell death. Ergosterol doesn't exist in animals or plants, so the Nystatin is effectively harmless. It can bind a bit to cholesterol though. 