---
title: "The next five years"
date: 2021-05-29T02:49:03-04:00
author: "Aaron Bell"
draft: false
---

I inquired of my subconscious what my plan is for the next five years. It gave me three images.

1) A silhoette of a forest; snapped small trunk; silhoutte of a man standing, looking right, silhoutte of an old woman looking toward me. 

2) A rocket ship on the left. Two blue lines extend from the top and the bottom of it to meet at a point to the right. I should learn math? 

3) Stalactites. 

From these images, I think I want to accomplish three things by 2026: Live the quiet life, learn math, and explore caves. 