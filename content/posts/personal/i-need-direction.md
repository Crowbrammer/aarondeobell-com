---
title: "I need direction"
date: 2021-05-04T17:04:37-04:00
author: "Aaron Bell"
draft: false
---

I have no long-term direction. Whenever I set a long-term goal, I feel fear that I'm going in the wrong direction, and thus I don't make the groundwork toward any direction. The past objectives I've gone after are: ForeVR, Dogecoin, and prosthetics. I have an apartment where my neighbors' fate and I's are intertwined in an uncomfortable way.