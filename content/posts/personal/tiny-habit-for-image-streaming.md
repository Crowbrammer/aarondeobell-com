---
title: "Tiny habit for image streaming leads to faster writing"
date: 2021-06-01T22:17:09-04:00
author: "Aaron Bell"
draft: false
---

I got lucky with a recent habit change.  Today, after feeling bad about my lack of progress with my writing, and after mild desperation caused me to reread the checklist for creating a new tiny habit in the appendix of [Tiny Habits](https://www.amazon.com/Tiny-Habits-Changes-Change-Everything/dp/0358003326), I decided to integrate a tiny [image-streaming](http://www.winwenger.com/imstream.htm) habit in my writing workflow: "After no inspiration for five seconds, I will 'search the darkness' for three seconds". 

What does search the darkness mean? When I close my eyes, I see mostly darkness, a sort of [aphantasia](https://en.wikipedia.org/wiki/Aphantasia)... until I look hard enough. Like a TV that sparks images every few seconds, I get meaningful images. They're not crystal clear and HD, but I get more meaningful low-def images in my subconscious than I do in my high-def reality.

I'm not an "open-minded" person, and I'm certainly not visual, so more than this will exhaust me. But because I understand the tiny habits process, at least I can capitalize on _some_ of the benefits that a visual, open-minded person would receive, i.e. the benefits of creativity and imagination. 

After rehearsing and celebrating this, I started doing it more. The paragraph flowed without inner resistance. I started getting through the draft, spit, and reorder in sub-forty-five instead of an hour-plus. I started to understand what I read faster. The quality of the writing went up. The speed at which I wrote improved. We'll see how this benefits me going forward, but I'm a huge fan of tiny habit change after this. 
